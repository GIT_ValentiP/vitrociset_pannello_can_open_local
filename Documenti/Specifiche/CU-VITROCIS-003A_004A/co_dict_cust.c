
/*********************************************************************
 * Be sure to include all headers here that relate to the symbols
 * below.
 ********************************************************************/

#include	"CO_Dict.H"		// Standard device information
#include	"CO_DEV.H"		// Standard device information
#include	"CO_SDO1.H"		// SDO1, the default SDO
#include	"CO_pDO1.H"		// 
#include	"CO_pDO2.H"		// 
#include	"CO_pDO3.H"		// 
#include	"CO_emcy.H"		// 




//			<index_l>,<index_h>,<sub index>,<type>,<count>,<ptr>


extern DWORD test32bit;


/*********************************************************************
 * This memory region defines the data types. All the indices in this
 * definition must range from 0x0001 to 0x009F. According to the 
 * CANOpen specification these are not required to be handled. If 
 * they are implemented then they should be read only and return the 
 * length of the object.
 ********************************************************************/
/* Dictionary database built into ROM */								
const rom DICT_OBJECT_TEMPLATE _db_objects[] = {
			{0x0001,0x00,WO | ROM_BIT | MAP_BIT,1,(rom unsigned char *)&__dummy[0]},
			{0x0002,0x00,WO | ROM_BIT | MAP_BIT,1,(rom unsigned char *)&__dummy[0]},
			{0x0003,0x00,WO | ROM_BIT | MAP_BIT,2,(rom unsigned char *)&__dummy[0]},
			{0x0004,0x00,WO | ROM_BIT | MAP_BIT,4,(rom unsigned char *)&__dummy[0]},
			{0x0005,0x00,WO | ROM_BIT | MAP_BIT,1,(rom unsigned char *)&__dummy[0]},
			{0x0006,0x00,WO | ROM_BIT | MAP_BIT,2,(rom unsigned char *)&__dummy[0]},
			{0x0007,0x00,WO | ROM_BIT | MAP_BIT,4,(rom unsigned char *)&__dummy[0]} };


const rom DICT_OBJECT_TEMPLATE _db_device[] = {
/*********************************************************************
 * This memory region defines the device. All the indices in this
 * definition must range from 0x1000 to 0x10FF. 
 ********************************************************************/
			{0x1000,0x00,CONST,4,(rom unsigned char *)&rCO_DevType},
			{0x1001,0x00,RO,1,(rom unsigned char *)&uCO_DevErrReg2},
			{0x1002,0x00,RO,4,(rom unsigned char *)&uCO_DevManufacturerStatReg},
			{0x1003,0x00,FUNC | RW,1,(rom unsigned char *)&CO_COMM_AccessErrorsFIFOCount},
			{0x1003,0x01,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},		// la cagata di accesso diretto alla fifo, v. uCO_DevErrReg2
			{0x1003,0x02,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x03,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x04,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x05,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x06,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x07,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
			{0x1003,0x08,FUNC | RO,4,(rom unsigned char *)&CO_COMM_AccessErrorsFIFO},
// 1004= num PDO supported, 00000001 = 0 receive 1 transmit; poi pdo after sync e pdo after RTR
			{0x1005,0x00,FUNC | RO,4,(rom unsigned char *)&CO_COMM_SYNC_COBIDAccessEvent},		// erano r/w ma non si fa! tutti...
// 1006 e 1007 comm cycle period, sync; sync window length
			{0x1008,0x00,CONST,sizeof(rCO_DevName)-1,(rom unsigned char *)&rCO_DevName},
			{0x1009,0x00,CONST,4,(rom unsigned char *)&rCO_DevHardwareVer},
			{0x100A,0x00,CONST,4,(rom unsigned char *)&rCO_DevSoftwareVer},
			{0x100B,0x00,RO,1,(rom unsigned char *)&configParms.nodeID},
			{0x100C,0x00,FUNC | RW,2,(rom unsigned char *)&CO_COMM_NMTE_GuardTimeAccessEvent},
			{0x100D,0x00,FUNC | RW,1,(rom unsigned char *)&CO_COMM_NMTE_LifeFactorAccessEvent},
// 100f= num SDO supported, mettere 0 client 1 server 0x00000001
			{0x1010,0x00,FUNC | RW,1,(rom unsigned char *)&rCO_parmIndx4},
//http://epec.planeetta.com/Public/Manuals/EPEC_Programming_And_Libraries/projecttopics/topic100329.htm
			{0x1010,0x01,FUNC | RW,4,(rom unsigned char *)&CO_SaveParameters},
			{0x1010,0x02,FUNC | RW,4,(rom unsigned char *)&CO_SaveParameters},
			{0x1010,0x03,FUNC | RW,4,(rom unsigned char *)&CO_SaveParameters},
			{0x1010,0x04,FUNC | RW,4,(rom unsigned char *)&CO_SaveParameters},
			{0x1011,0x00,FUNC | RW,1,(rom unsigned char *)&rCO_parmIndx4},
			{0x1011,0x01,FUNC | RW,4,(rom unsigned char *)&CO_RestoreDefault},
			{0x1011,0x02,FUNC | RW,4,(rom unsigned char *)&CO_RestoreDefault},
			{0x1011,0x03,FUNC | RW,4,(rom unsigned char *)&CO_RestoreDefault},
			{0x1011,0x04,FUNC | RW,4,(rom unsigned char *)&CO_RestoreDefault},
// 1012 e 1013 sono timestamp
			{0x1014,0x00,FUNC | RO,4,(rom unsigned char *)&CO_COMM_EMCY_COBIDAccessEvent},
			{0x1016,0x00,RW,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1016,0x01,RW,4,(rom unsigned char *)&HeartBeatConsumers}, // boh, fare?
//http://www.byteme.org.uk/canopenparent/canopen/guard-protocol-canopen/
			{0x1017,0x00,FUNC | RW,2,(rom unsigned char *)&CO_COMM_NMTE_HeartBeatAccessEvent},
			{0x1018,0x00,CONST,1,(rom unsigned char *)&rCO_DevIdentityIndx},
			{0x1018,0x01,CONST,4,(rom unsigned char *)&rCO_DevVendorID},
			{0x1018,0x02,CONST,4,(rom unsigned char *)&rCO_DevProductCode},
			{0x1018,0x03,CONST,4,(rom unsigned char *)&rCO_DevRevNo},
			{0x1018,0x04,CONST,4,(rom unsigned char *)&configParms.serialNumber} };




const rom DICT_OBJECT_TEMPLATE _db_sdo[] = {
/*********************************************************************
 * This memory region defines the SDOs. All the indices in this
 * definition must range from 0x1200 to 0x12FF. SDO servers range
 * from 0x1200 to 0x127F. SDO clients range from 0x1280 to 0x13FF - ERRORE! ovviamente intendeva 12FF .
 ********************************************************************/
			{0x1200,0x00,CONST,1,(rom unsigned char *)&_uSDO1COMMIndx},
			{0x1200,0x01,FUNC | RO,4,(rom unsigned char *)&CO_COMM_SDO1_CS_COBIDAccessEvent},
			{0x1200,0x02,FUNC | RO,4,(rom unsigned char *)&CO_COMM_SDO1_SC_COBIDAccessEvent} };
			
//1280 sarebbe in effetti la descrizione del comportamento client to server... NOI NON LO FACCIAMO!
//in teoria non dovremme nemmeno esistere la CS qua sopra




/*********************************************************************
 * This memory region defines receive PDOs. All the indices in this
 * definition must range from 0x1400 to 0x15FF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_pdo1_rx_comm[] = {
			{0x1400,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1400,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_RPDO1_COBIDAccessEvent},
			{0x1400,0x02,RW,1,(rom unsigned char *)&configParms.uSyncSet[0]} };

#ifdef USE_PDO_2
const rom DICT_OBJECT_TEMPLATE _db_pdo2_rx_comm[] =	{
			{0x1401,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1401,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_RPDO2_COBIDAccessEvent},
			{0x1401,0x02,RW,1,(rom unsigned char *)&configParms.uSyncSet[1]} };
#endif

#ifdef USE_PDO_3
const rom DICT_OBJECT_TEMPLATE _db_pdo3_rx_comm[] = {
			{0x1402,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1402,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_RPDO3_COBIDAccessEvent}	};
#endif

#ifdef USE_PDO_4
const rom DICT_OBJECT_TEMPLATE _db_pdo4_rx_comm[] = {
			{0x1403,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1403,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_RPDO4_COBIDAccessEvent}	};
#endif

#ifdef USE_PDO_5
const rom DICT_OBJECT_TEMPLATE _db_pdo5_rx_comm[] = {
			{0x1404,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif
#ifdef USE_PDO_6
const rom DICT_OBJECT_TEMPLATE _db_pdo6_rx_comm[] = {
			{0x1405,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif

const rom DICT_OBJECT_TEMPLATE _db_pdo1_rx_map[] = {
/*********************************************************************
 * This memory region defines receive PDOs mapping. All the indices 
 * in this definition must range from 0x1600 to 0x17FF. 
 ********************************************************************/
			{0x1600,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1600,0x01,CONST,4,(rom unsigned char *)&uRPDO1Map[0]},
			{0x1600,0x02,CONST,4,(rom unsigned char *)&uRPDO1Map[1]}
			};

#ifdef USE_PDO_2
const rom DICT_OBJECT_TEMPLATE _db_pdo2_rx_map[] = {
			{0x1601,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx8},
			{0x1601,0x01,CONST,4,(rom unsigned char *)&uRPDO2Map[0]},
			{0x1601,0x02,CONST,4,(rom unsigned char *)&uRPDO2Map[1]},
			{0x1601,0x03,CONST,4,(rom unsigned char *)&uRPDO2Map[2]},
			{0x1601,0x04,CONST,4,(rom unsigned char *)&uRPDO2Map[3]},
			{0x1601,0x05,CONST,4,(rom unsigned char *)&uRPDO2Map[4]},
			{0x1601,0x06,CONST,4,(rom unsigned char *)&uRPDO2Map[5]},
			{0x1601,0x07,CONST,4,(rom unsigned char *)&uRPDO2Map[6]},
			{0x1601,0x08,CONST,4,(rom unsigned char *)&uRPDO2Map[7]}
			};
#endif

#ifdef USE_PDO_3
const rom DICT_OBJECT_TEMPLATE _db_pdo3_rx_map[] = {
			{0x1602,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1602,0x01,CONST,4,(rom unsigned char *)&uRPDO3Map[0]},
			{0x1602,0x02,CONST,4,(rom unsigned char *)&uRPDO3Map[1]}
			};
#endif

#ifdef USE_PDO_4
const rom DICT_OBJECT_TEMPLATE _db_pdo4_rx_map[] = {
			{0x1603,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1603,0x01,CONST,4,(rom unsigned char *)&uRPDO4Map}
			};
#endif

#ifdef USE_PDO_5
const rom DICT_OBJECT_TEMPLATE _db_pdo5_rx_map[] = {
			{0x1604,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif

#ifdef USE_PDO_6
const rom DICT_OBJECT_TEMPLATE _db_pdo6_rx_map[] = {
			{0x1605,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif


/*********************************************************************
 * This memory region defines transmit PDOs. All the indices in this
 * definition must range from 0x1800 to 0x19FF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_pdo1_tx_comm[] = {
			{0x1800,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1800,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_TPDO1_COBIDAccessEvent},
			{0x1800,0x02,RW,1,(rom unsigned char *)&configParms.uSyncSet[0]}	};

#ifdef USE_PDO_2
const rom DICT_OBJECT_TEMPLATE _db_pdo2_tx_comm[] = {
			{0x1801,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1801,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_TPDO2_COBIDAccessEvent},
			{0x1801,0x02,RW,1,(rom unsigned char *)&configParms.uSyncSet[1]} };
#endif
#ifdef USE_PDO_3
const rom DICT_OBJECT_TEMPLATE _db_pdo3_tx_comm[] = {
			{0x1802,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1802,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_TPDO3_COBIDAccessEvent} };
#endif
#ifdef USE_PDO_4
const rom DICT_OBJECT_TEMPLATE _db_pdo4_tx_comm[] = {
			{0x1803,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1803,0x01,RO | FUNC,4,(rom unsigned char *)&CO_COMM_TPDO4_COBIDAccessEvent} };
#endif

#ifdef USE_PDO_5
const rom DICT_OBJECT_TEMPLATE _db_pdo5_tx_comm[] = {
			{0x1804,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif
#ifdef USE_PDO_6
const rom DICT_OBJECT_TEMPLATE _db_pdo6_tx_comm[] = {
			{0x1805,0x00,RW,1,(rom unsigned char *)&__dummy} };
#endif



/*********************************************************************
 * This memory region defines receive PDOs mapping. All the indices 
 * in this definition must range from 0x1A00 to 0x1BFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_pdo1_tx_map[] = 		{
			{0x1A00,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1A00,0x01,CONST,4,(rom unsigned char *)&uTPDO1Map[0]},
			{0x1A00,0x02,CONST,4,(rom unsigned char *)&uTPDO1Map[1]} };

#ifdef USE_PDO_2
const rom DICT_OBJECT_TEMPLATE _db_pdo2_tx_map[] = {
			{0x1A01,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx4},
			{0x1A01,0x01,CONST,4,(rom unsigned char *)&uTPDO2Map[0]},
			{0x1A01,0x02,CONST,4,(rom unsigned char *)&uTPDO2Map[1]},
			{0x1A01,0x03,CONST,4,(rom unsigned char *)&uTPDO2Map[2]},
			{0x1A01,0x04,CONST,4,(rom unsigned char *)&uTPDO2Map[3]} };
#endif
#ifdef USE_PDO_3
const rom DICT_OBJECT_TEMPLATE _db_pdo3_tx_map[] = {
			{0x1A02,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x1A02,0x01,CONST,1,(rom unsigned char *)&uTPDO3Map[0]},
			{0x1A02,0x02,CONST,1,(rom unsigned char *)&uTPDO3Map[1]} };
#endif
#ifdef USE_PDO_4
const rom DICT_OBJECT_TEMPLATE _db_pdo4_tx_map[] = {
			{0x1A03,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1A03,0x01,CONST,1,(rom unsigned char *)&uTPDO4Map} };
#endif

#ifdef USE_PDO_5
const rom DICT_OBJECT_TEMPLATE _db_pdo5_tx_map[] = {
			{0x1A04,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1A04,0x01,RW,1,(rom unsigned char *)&__dummy}
#endif
#ifdef USE_PDO_6
const rom DICT_OBJECT_TEMPLATE _db_pdo6_tx_map[] = {
			{0x1A05,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x1A05,0x01,RW,1,(rom unsigned char *)&__dummy} };
#endif



/*********************************************************************
 * This memory region defines manufacturer specific objects. All the 
 * indices in this definition must range from 0x2000 to 0x2FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g1[] = {
			{0x2000,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2000,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinPolarity},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2000,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinPolarity},
#if NUM_BYTE_TASTI_I2C>2
			{0x2000,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinPolarity},
#if NUM_BYTE_TASTI_I2C>4
			{0x2000,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinPolarity},
#if NUM_BYTE_TASTI_I2C>6
			{0x2000,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinPolarity},
#endif
#endif
#endif
#endif
			{0x2001,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2001,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinFilter},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2001,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinFilter},
#if NUM_BYTE_TASTI_I2C>2
			{0x2001,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinFilter},
#if NUM_BYTE_TASTI_I2C>4
			{0x2001,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinFilter},
#if NUM_BYTE_TASTI_I2C>6
			{0x2001,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinFilter},
#endif
#endif
#endif
#endif
			{0x2002,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2002,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinIntEnable},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2002,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinIntEnable},
#if NUM_BYTE_TASTI_I2C>2
			{0x2002,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinIntEnable},
#if NUM_BYTE_TASTI_I2C>4
			{0x2002,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinIntEnable},
#if NUM_BYTE_TASTI_I2C>6
			{0x2002,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinIntEnable},
#endif
#endif
#endif
#endif
			{0x2003,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2003,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinIntChange},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2003,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinIntChange},
#if NUM_BYTE_TASTI_I2C>2
			{0x2003,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinIntChange},
#if NUM_BYTE_TASTI_I2C>4
			{0x2003,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinIntChange},
#if NUM_BYTE_TASTI_I2C>6
			{0x2003,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinIntChange},
#endif
#endif
#endif
#endif
			{0x2004,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2004,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinIntRise},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2004,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinIntRise},
#if NUM_BYTE_TASTI_I2C>2
			{0x2004,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinIntRise},
#if NUM_BYTE_TASTI_I2C>4
			{0x2004,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinIntRise},
#if NUM_BYTE_TASTI_I2C>6
			{0x2004,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinIntRise},
#endif
#endif
#endif
#endif
			{0x2005,0x00,CONST,1,(rom unsigned char *)&rMaxIndexPuls},
			{0x2005,0x01,RW,2,(rom unsigned char *)&configParms.inputFeat[0].uIOinIntFall},
#ifdef NUM_BYTE_TASTI_I2C
			{0x2005,0x02,RW,2,(rom unsigned char *)&configParms.inputFeat[1].uIOinIntFall},
#if NUM_BYTE_TASTI_I2C>2
			{0x2005,0x03,RW,2,(rom unsigned char *)&configParms.inputFeat[2].uIOinIntFall},
#if NUM_BYTE_TASTI_I2C>4
			{0x2005,0x04,RW,2,(rom unsigned char *)&configParms.inputFeat[3].uIOinIntFall},
#if NUM_BYTE_TASTI_I2C>6
			{0x2005,0x05,RW,2,(rom unsigned char *)&configParms.inputFeat[4].uIOinIntFall},
#endif
#endif
#endif
#endif

#ifdef NUM_ANA 
			{0x2100,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},			// PARAMETRIZZARE SU NUM_ANA
			{0x2100,0x01,RW,2,(rom unsigned char *)&configParms.anaRange[0]},
#if NUM_ANA>(1)
			{0x2100,0x02,RW,2,(rom unsigned char *)&configParms.anaRange[1]},
#if NUM_ANA>(2)
			{0x2100,0x03,RW,2,(rom unsigned char *)&configParms.anaRange[2]},
#if NUM_ANA>(3)
			{0x2100,0x04,RW,2,(rom unsigned char *)&configParms.anaRange[3]},
#endif
#endif
#endif
#endif

			{0x2200,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx3},
			{0x2200,0x01,RW,2,(rom unsigned char *)&configParms.outputFeat[0].bootState},
			{0x2200,0x02,RW,2,(rom unsigned char *)&configParms.outputFeat[1].bootState},		// NUM_BYTE_LED_I2C
			{0x2200,0x03,RW,2,(rom unsigned char *)&configParms.outputFeat[2].bootState},		

#ifdef USA_PWM
			{0x2300,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},			// PARAMETRIZZARE SU NUM_PWM
			{0x2300,0x01,RW,2,(rom unsigned char *)&configParms.PWMFreq[0]},
			{0x2300,0x02,RW,2,(rom unsigned char *)&configParms.PWMFreq[1]},
			{0x2301,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},			//
			{0x2301,0x01,RW,2,(rom unsigned char *)&configParms.PWMDuty[0]},
			{0x2301,0x02,RW,2,(rom unsigned char *)&configParms.PWMDuty[1]}
#endif

			};

// METTERE da qualche parte configParms.outputFeat[0].bootState e l'altro
				
/*********************************************************************
 * This memory region defines manufacturer specific objects. All the 
 * indices in this definition must range from 0x3000 to 0x3FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g2[] = {
			{0x3200,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x3200,0x01,FUNC | RW,1,(rom unsigned char *)&CO_SetNodeId},
			{0x3200,0x02,FUNC | RW,1,(rom unsigned char *)&CO_SetBaudRate},
			{0x3202,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x3202,0x01,FUNC | RW,1,(rom unsigned char *)&_setTerminatorsMode} };
				
/*********************************************************************
 * This memory region defines manufacturer specific objects. All the 
 * indices in this definition must range from 0x4000 to 0x4FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g3[] = {
			{0x4000,0x00,RW,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x4000,0x01,RW,4,(rom unsigned char *)&test32bit} };
				
/*********************************************************************
 * This memory region defines manufacturer specific objects. All the 
 * indices in this definition must range from 0x5000 to 0x5FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g4[] = {
#ifdef USA_BOOTLOADER 
// a 5000, 5001 mettiamo le voci usate per il bootloader
			{0x5002,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x5002,0x01,RW | FUNC,4,(rom unsigned char *)&CO_COMM_SDO1_BootloaderKey},
#endif
//5003 e' bootloader version
			{0x5004,0x00,CONST,1,(rom unsigned char *)&rCO_parmIndx2},
			{0x5004,0x01,WO | FUNC,4,(rom unsigned char *)&CO_COMM_SerialNumberKey},			//*tK*
			{0x5004,0x02,RW | FUNC,4,(rom unsigned char *)&CO_COMM_SerialNumber}
			};

//http://en.canopen-lift.org/wiki/CAN_Statistic_Counter
// contatori/errori a 5800h

/*********************************************************************
 * This memory region defines standard objects. All the 
 * indices in this definition must range from 0x6000 to 0x6FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_standard_g1[] = {
			{0x6000,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx3},		// 
			{0x6000,0x01,RO,1,(rom unsigned char *)&uLocalXmtBuffer[0]},
			{0x6000,0x02,RO,1,(rom unsigned char *)&uLocalXmtBuffer[1]},
			{0x6000,0x02,RO,1,(rom unsigned char *)&uLocalXmtBuffer[2]},

			{0x6200,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx2},

// credo che questi debbano essere read-only. o, se si scrive, lo scritto deve andare in statoled ecc di configparms
			{0x6200,0x01,RW,1,(rom unsigned char *)&uLocalRcvBuffer[0]},		// o statoled? o configParms.statoled?
			{0x6200,0x02,RW,1,(rom unsigned char *)&uLocalRcvBuffer[1]},

// e anche savedState...

			{0x6500,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx5},
			{0x6500,0x01,FUNC | WO,4,(rom unsigned char *)&CO_SetLCD1},
			{0x6500,0x02,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD1_1},
			{0x6500,0x03,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD1_2},
			{0x6500,0x04,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD1_3},
			{0x6500,0x05,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD1_4},
			{0x6501,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx5},
			{0x6501,0x01,FUNC | WO,4,(rom unsigned char *)&CO_SetLCD2},
			{0x6501,0x02,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD2_1},
			{0x6501,0x03,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD2_2},
			{0x6501,0x04,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD2_3},
			{0x6501,0x05,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD2_4},
			{0x6502,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx5},
			{0x6502,0x01,FUNC | WO,4,(rom unsigned char *)&CO_SetLCD3},
			{0x6502,0x02,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD3_1},
			{0x6502,0x03,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD3_2},
			{0x6502,0x04,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD3_3},
			{0x6502,0x05,FUNC | RW,20,(rom unsigned char *)&CO_SetLCD3_4},

			{0x6600,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx7},
			{0x6600,0x01,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay1},
			{0x6600,0x02,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay2},
			{0x6600,0x03,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay3},
			{0x6600,0x04,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay4},
			{0x6600,0x05,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay5},
			{0x6600,0x06,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay6},
			{0x6600,0x07,FUNC | RW,4,(rom unsigned char *)&CO_SetDisplay7},

			{0x6700,0x00,RO,1,(rom unsigned char *)&rCO_parmIndx1},
			{0x6700,0x01,FUNC | RO,2,(rom unsigned char *)&CO_ReadTemperature},

			};

// v. anche variabile statoled, salvare in eeprom?
// uLocalXmtBuffer2 


/*********************************************************************
 * This memory region defines standard objects. All the 
 * indices in this definition must range from 0x7000 to 0x7FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_standard_g2[] = {
			{0x7000,0x00,0,1,(rom unsigned char *)&__dummy},
			{0x7000,0x00,0,1,(rom unsigned char *)&__dummy} };

/*********************************************************************
 * This memory region defines standard objects. All the 
 * indices in this definition must range from 0x8000 to 0x8FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_standard_g3[] = {
			{0x8000,0x00,0,1,(rom unsigned char *)&__dummy},
			{0x8000,0x00,0,1,(rom unsigned char *)&__dummy} };

/*********************************************************************
 * This memory region defines standard objects. All the 
 * indices in this definition must range from 0x9000 to 0x9FFF. 
 ********************************************************************/
const rom DICT_OBJECT_TEMPLATE _db_standard_g4[] = {
			{0x9000,0x00,0,1,(rom unsigned char *)&__dummy},
			{0x9000,0x00,0,1,(rom unsigned char *)&__dummy} };
				

								
const unsigned int sizeof_db_objects=sizeof(_db_objects)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_manufacturer_g1=sizeof(_db_manufacturer_g1)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_manufacturer_g2=sizeof(_db_manufacturer_g2)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_manufacturer_g3=sizeof(_db_manufacturer_g3)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_manufacturer_g4=sizeof(_db_manufacturer_g4)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_standard_g1=sizeof(_db_standard_g1)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_standard_g2=sizeof(_db_standard_g2)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_standard_g3=sizeof(_db_standard_g3)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_standard_g4=sizeof(_db_standard_g4)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_device=sizeof(_db_device)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_sdo=sizeof(_db_sdo)/sizeof(DICT_OBJECT_TEMPLATE);

const unsigned int sizeof_db_pdo1_rx_comm=sizeof(_db_pdo1_rx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo1_tx_comm=sizeof(_db_pdo1_tx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo1_rx_map=sizeof(_db_pdo1_rx_map)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo1_tx_map=sizeof(_db_pdo1_tx_map)/sizeof(DICT_OBJECT_TEMPLATE);
#ifdef USE_PDO_2
const unsigned int sizeof_db_pdo2_rx_comm=sizeof(_db_pdo2_rx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo2_rx_map=sizeof(_db_pdo2_rx_map)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo2_tx_comm=sizeof(_db_pdo2_tx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo2_tx_map=sizeof(_db_pdo2_tx_map)/sizeof(DICT_OBJECT_TEMPLATE);
#endif
#ifdef USE_PDO_3
const unsigned int sizeof_db_pdo3_rx_comm=sizeof(_db_pdo3_rx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo3_rx_map=sizeof(_db_pdo3_rx_map)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo3_tx_comm=sizeof(_db_pdo3_tx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo3_tx_map=sizeof(_db_pdo3_tx_map)/sizeof(DICT_OBJECT_TEMPLATE);
#endif
#ifdef USE_PDO_4
const unsigned int sizeof_db_pdo4_rx_comm=sizeof(_db_pdo4_rx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo4_tx_comm=sizeof(_db_pdo4_tx_comm)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo4_rx_map=sizeof(_db_pdo4_rx_map)/sizeof(DICT_OBJECT_TEMPLATE);
const unsigned int sizeof_db_pdo4_tx_map=sizeof(_db_pdo4_tx_map)/sizeof(DICT_OBJECT_TEMPLATE);
#endif
					
				
