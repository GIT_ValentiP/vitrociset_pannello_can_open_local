//=============================================================
//          WINSTAR Display Co.,Ltd
//    LCM     	 :WEH2004
//    Contraller :
//    Author     :Brian lin 2010/12/13
//    history     :
//==============================================================
#include	<reg51.h>
#include 	<stdio.h>          // define I/O functions
#include 	<INTRINS.H>        // KEIL FUNCTION

#define  	Cword	0x14 //20
#define	one		0x80 // DD RAM Address 第一行之起始位置0x00
						// 所以設定DD RAM位址為0x80+0x00=0x80	
#define	two		0xc0 // DD RAM Address 第二行之起始位置0x40
						// 所以設定DD RAM位址為0x80+0x40=0xc0
#define	three		0x94// DD RAM Address 第二行之起始位置0x14
#define	four		0xd4// DD RAM Address 第二行之起始位置0x54
#define	Data_BUS	P1

sbit		busy    	=P1^7;
sbit		RS      		=P3^0;
sbit	 	RW      		=P3^7;
sbit		Enable  	=P3^4;

sbit	STP     = P2^0;
sbit	S_S     = P2^1;

char bdata  flag;
sbit busy_f  = flag^0;

unsigned char code MSG1[Cword]  ="   Winstar display  ";
unsigned char code MSG2[Cword]  ="      WEH2004       ";
unsigned char code MSG3[Cword]  ="                    ";

void CheckBusy(void);
void WriteIns(unsigned char);
void WriteData(unsigned char);
void WriteString(unsigned char,unsigned char *);
void Initial_ks0066();
void delay(unsigned char);
									  


void WriteIns(unsigned char instruction)
{
	 RS = 0;	       
	 RW = 0;
	 Data_BUS = instruction;
	 Enable = 1;            //1us
	 _nop_();		//1us
	 Enable = 0;		//1us
}

void CheckBusy(void)
{
	Data_BUS = 0xff; //訊號由high變為low比較容易,所以全部設為high.
	RS = 0;
	RW = 1;  
	do
	{
		  Enable = 1;
		  busy_f = busy;
		  Enable = 0;
	 }while(busy_f);
}
//=================================
void WriteCmd(unsigned char instruction)
{
	 RS = 0;	       
	 RW = 0;
	 Data_BUS = instruction;
	 Enable = 1;            //1us
	 _nop_();		//1us
	 Enable = 0;		//1us
	
	CheckBusy();
}
//=================================
//=================================
void WriteData(unsigned char data1)
{
	 RS = 1;	       
	 RW = 0;
	 Data_BUS = data1;
	 Enable = 1;
	 _nop_();
	 Enable = 0;

	CheckBusy(); 
}
//=================================
//=================================
void WriteString(unsigned char count,unsigned char * MSG)
{
	unsigned char i;
	
	for(i = 0; i<count;i++)
		WriteData(MSG[i]);
}

//=================================
void Initial_OLED(void)
{
	WriteIns(0x38);//function set
	WriteIns(0x38);//function set
	WriteIns(0x38);//function set
	WriteIns(0x38);//function set
	WriteCmd(0x08);//display off
	WriteCmd(0x06);//entry mode set
	WriteCmd(0x17);//Character mode
	WriteCmd(0x01);//clear display
	WriteCmd(0x02);
	WriteCmd(0x0c);//display on
}
//============================================
//==================================
void delay(unsigned char m)
{
	unsigned char i,j,k;
	 for(j = 0;j<m;j++)
 	{
	 	for(k = 0; k<200;k++)
	 	{
			for(i = 0; i<100;i++)
			{
			}
		}
 	}	
}
//===================================
// stp_sc();
//==================================
void stp_sc(void)
{
	 while(S_S==0)
	 {
		 if(S_S==1)
	 	break;
	 	if(STP==1)
	 	break;
 	}
}
//==================================
void main(void)
{
	unsigned char i,j=0;

	while(1)
	{
		Initial_OLED();

		WriteIns(one);
		for(i = 0; i<Cword;i++)
		{
			WriteData(0xff);
		}
		WriteIns(two);
		for(i = 0; i<Cword;i++)
		{
			WriteData(0xff);
		}
		WriteIns(three);
		for(i = 0; i<Cword;i++)
		{
			WriteData(0xff);
		}
		WriteIns(four);
		for(i = 0; i<Cword;i++)
		{
			WriteData(0xff);
		}
		delay(10);
		stp_sc();

		WriteIns(one);
		for(i = 0; i<Cword;i++)
		{
			WriteData(MSG1[i]);
		}
		WriteIns(two);
		for(i = 0; i<Cword;i++)
		{
			WriteData(MSG2[i]);
		}
		WriteIns(three);
		for(i = 0; i<Cword;i++)
		{
			WriteData(MSG1[i]);
		}
		WriteIns(four);
		for(i = 0; i<Cword;i++)
		{
			WriteData(MSG2[i]);
		}
		
		delay(10);
		stp_sc();
	}
}


