/*****************************************************************************
 *
 * Microchip CANopen Stack (Dictionary Services)
 *
 *****************************************************************************
 * FileName:        CO_DICT.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.20.00 or higher
 * Linker:          MPLINK 03.40.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		27.3.17		PIC24E version
 * 
 *****************************************************************************/


#ifndef _CO_DICT_DEFINED_H
#define _CO_DICT_DEFINED_H


#include	<p24exxxx.H>

#if defined(__18CXX)
#include	"CO_DEFS.DEF"			// Global definitions
#include	"CO_TYPES.H"
#include	"CO_CANDRV.H"			// Driver services
#else
#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include  "co_main24.h"
#endif
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services
#ifndef IS_BOOTLOADER
#include  "demoobj.h"
#endif
#include	<libpic30.H>
#endif



// This is the scalar equivalent of a portion of the DICT_OBJ
typedef struct _DICTIONARY_OBJECT_TEMPLATE {
	unsigned int index;
	unsigned char subindex;
	unsigned char ctl;
	unsigned int len;
	rom unsigned char * pROM;
	} __attribute__ ((packed)) DICT_OBJECT_TEMPLATE;




typedef struct _DICTIONARY_EXTENDED_OBJECT {
	//enum _DICT_OBJECT_REQUEST		/* Command required for function */
	//{
	//	DICT_OBJ_INFO = 0,
	//	DICT_OBJ_READ,
	//	DICT_OBJ_WRITE
	//}cmd;

	unsigned char *pReqBuf;		// Pointer to the requestors buffer 

	unsigned int reqLen;			// Number of bytes requested 
	unsigned int reqOffst;			// Starting point for the request 

	unsigned int index;				// CANOpen Index 
	unsigned char subindex;		// CANOpen Sub-index 

	enum DICT_CTL {					// Memory access type 
		ACCESS_BITS	= 0b00000111,
				
		NA			= 0b00000000,	/* Default, non-existant */
		CONST		= 0b00000101,	/* Default, read only from ROM */
		RW			= 0b00000011,	/* Default, read/write from RAM */
		RO			= 0b00000001,	/* Default, read only from RAM */
		WO			= 0b00000010,	/* Default, write only to RAM */
		RW_EE		= 0b00001011,	/* Default, read/write from EEDATA */
		RO_EE		= 0b00001001,	/* Default, read only from EEDATA */
		WO_EE		= 0b00001010,	/* Default, write only to EEDATA */
		FUNC		= 0b00010000,	/* Default, function specific */
		
		RD_BIT		= 0b00000001,	/* Read Access */
		RD			= 0b01111111,	
		N_RD		= 0b01111110,	
		
		WR_BIT		= 0b00000010,	/* Write Access */
		WR			= 0b01111111,	
		N_WR		= 0b01111101,
		
		ROM_BIT		= 0b00000100,	/* ROM based object */
		ROM			= 0b01111111,	
		N_ROM		= 0b01111011,
		
		EE_BIT		= 0b00001000,	/* EEDATA based object */
		EE			= 0b01111111,
		N_EE		= 0b01110111,
		
		FDEF_BIT	= 0b00010000,	/* Functionally defined access */
		FDEF		= 0b01111111,	
		N_FDEF		= 0b01101111,
		
		MAP_BIT		= 0b00100000,	/* PDO Mappability*/
		MAP			= 0b01111111,
		N_MAP		= 0b01011111,	
		
		FSUB_BIT	= 0b01000000,	/* Functionally defined sub-index */
		FSUB		= 0b01111111,
		N_FSUB		= 0b00111111	
		} __attribute__ ((packed)) ctl;
	
	unsigned int len;				// Size of the object in bytes 
	
	union DICT_PTRS	{				// Pointers to objects 
		void (* pFunc)(void);
		unsigned char * pRAM;
#if defined(__18CXX)
		rom unsigned char *pROM;
#endif
		unsigned int pEEDATA;
		}	__attribute__ ((packed))p;
	
	} __attribute__ ((packed)) DICT_OBJ;



typedef struct _MULTIPLEXOR {
	UNSIGNED16 index;
	UNSIGNED8 sindex;
	} MULTIPLEXOR;


typedef struct _DICTIONARY_DATA {
	DICT_OBJ *obj;					/* Pointer to the local object */
	enum _DICT_OBJECT_REQUEST	{	/* Command required for function */
		DICT_OBJ_INFO = 0,
		DICT_OBJ_READ,
		DICT_OBJ_WRITE
		} cmd;
	unsigned char ret;				/* Return status */
	} __attribute__ ((packed)) DICT_PARAM;


extern DICT_PARAM uDict;



BYTE CO_DictObjectRead(DICT_OBJ *);
BYTE CO_DictObjectWrite(DICT_OBJ *);
BYTE CO_DictObjectDecode(DICT_OBJ *);
void CO_DictTest(void);
void CO_TestObjectAccessEvent(void);


#define mCO_DictGetCmd()			uDict.cmd
#define mCO_DictGetRet()			uDict.ret
#define mCO_DictSetRet(retVal)		uDict.ret = retVal

extern const rom unsigned char	__dummy[4];

/* Dictionary database built into ROM */								
extern const rom DICT_OBJECT_TEMPLATE _db_objects[];
extern const rom DICT_OBJECT_TEMPLATE _db_device[];
extern const rom DICT_OBJECT_TEMPLATE _db_sdo[];

extern const rom DICT_OBJECT_TEMPLATE _db_pdo1_rx_comm[];
#ifdef USE_PDO_2
extern const rom DICT_OBJECT_TEMPLATE _db_pdo2_rx_comm[];
#endif
#ifdef USE_PDO_3
extern const rom DICT_OBJECT_TEMPLATE _db_pdo3_rx_comm[];
#endif
#ifdef USE_PDO_4
extern const rom DICT_OBJECT_TEMPLATE _db_pdo4_rx_comm[];
#endif

extern const rom DICT_OBJECT_TEMPLATE _db_pdo1_rx_map[];
#ifdef USE_PDO_2
extern const rom DICT_OBJECT_TEMPLATE _db_pdo2_rx_map[];
#endif
#ifdef USE_PDO_3
extern const rom DICT_OBJECT_TEMPLATE _db_pdo3_rx_map[];
#endif
#ifdef USE_PDO_4
extern const rom DICT_OBJECT_TEMPLATE _db_pdo4_rx_map[];
#endif

extern const rom DICT_OBJECT_TEMPLATE _db_pdo1_tx_comm[];
#ifdef USE_PDO_2
extern const rom DICT_OBJECT_TEMPLATE _db_pdo2_tx_comm[];
#endif
#ifdef USE_PDO_3
extern const rom DICT_OBJECT_TEMPLATE _db_pdo3_tx_comm[];
#endif
#ifdef USE_PDO_4
extern const rom DICT_OBJECT_TEMPLATE _db_pdo4_tx_comm[];
#endif

extern const rom DICT_OBJECT_TEMPLATE _db_pdo1_tx_map[];
#ifdef USE_PDO_2
extern const rom DICT_OBJECT_TEMPLATE _db_pdo2_tx_map[];
#endif
#ifdef USE_PDO_3
extern const rom DICT_OBJECT_TEMPLATE _db_pdo3_tx_map[];
#endif
#ifdef USE_PDO_4
extern const rom DICT_OBJECT_TEMPLATE _db_pdo4_tx_map[];
#endif

extern const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g1[];
extern const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g2[];
extern const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g3[];
extern const rom DICT_OBJECT_TEMPLATE _db_manufacturer_g4[];
extern const rom DICT_OBJECT_TEMPLATE _db_standard_g1[];
extern const rom DICT_OBJECT_TEMPLATE _db_standard_g2[];
extern const rom DICT_OBJECT_TEMPLATE _db_standard_g3[];
extern const rom DICT_OBJECT_TEMPLATE _db_standard_g4[];


								
extern const unsigned int sizeof_db_objects;
extern const unsigned int sizeof_db_manufacturer_g1;
extern const unsigned int sizeof_db_manufacturer_g2;
extern const unsigned int sizeof_db_manufacturer_g3;
extern const unsigned int sizeof_db_manufacturer_g4;
extern const unsigned int sizeof_db_standard_g1;
extern const unsigned int sizeof_db_standard_g2;
extern const unsigned int sizeof_db_standard_g3;
extern const unsigned int sizeof_db_standard_g4;
extern const unsigned int sizeof_db_device;
extern const unsigned int sizeof_db_sdo;

extern const unsigned int sizeof_db_pdo1_rx_comm;
extern const unsigned int sizeof_db_pdo1_tx_comm;
extern const unsigned int sizeof_db_pdo1_rx_map;
extern const unsigned int sizeof_db_pdo1_tx_map;
#ifdef USE_PDO_2
extern const unsigned int sizeof_db_pdo2_rx_comm;
extern const unsigned int sizeof_db_pdo2_rx_map;
extern const unsigned int sizeof_db_pdo2_tx_comm;
extern const unsigned int sizeof_db_pdo2_tx_map;
#endif
#ifdef USE_PDO_3
extern const unsigned int sizeof_db_pdo3_rx_comm;
extern const unsigned int sizeof_db_pdo3_rx_map;
extern const unsigned int sizeof_db_pdo3_tx_comm;
extern const unsigned int sizeof_db_pdo3_tx_map;
#endif
#ifdef USE_PDO_4
extern const unsigned int sizeof_db_pdo4_rx_comm;
extern const unsigned int sizeof_db_pdo4_tx_comm;
extern const unsigned int sizeof_db_pdo4_rx_map;
extern const unsigned int sizeof_db_pdo4_tx_map;
#endif

#endif
