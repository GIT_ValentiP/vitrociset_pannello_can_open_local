
/*****************************************************************************
 *
 * Microchip CANopen Stack (Device Info)
 *
 *****************************************************************************
 * FileName:        CO_DEV.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * This file contains many of the standard objects defined by CANopen.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		2017
 * 
 *****************************************************************************/


#ifndef _CO_DEV_DEFINED_H
#define _CO_DEV_DEFINED_H

#include	<p24exxxx.H>

#if !defined(__18CXX)
#define rom const
#endif

extern rom unsigned long rCO_DevType; 
#ifdef IS_BOOTLOADER			// per sizeof...
extern const rom char rCO_DevName[28];
#elif IS_CARIBONI
extern const rom char rCO_DevName[26];
#else
extern const rom char rCO_DevName[24];
#endif
extern rom unsigned char rCO_DevHardwareVer[];
extern rom unsigned char rCO_DevSoftwareVer[]; 

extern rom unsigned char rCO_DevIdentityIndx;
extern rom unsigned long rCO_DevVendorID; 
extern rom unsigned long rCO_DevProductCode;
extern rom unsigned long rCO_DevRevNo;
//extern rom unsigned long rCO_DevSerialNo;

extern unsigned char uCO_DevErrReg;
extern unsigned long uCO_DevErrReg2[8];		//array/FIFO
extern unsigned long uCO_DevManufacturerStatReg;
extern unsigned long uCO_DevPredefinedErrField;
void CO_RestoreDefault(void);
void CO_SaveParameters(void);
void CO_SetBaudRate(void);
void CO_SetNodeId(void);

void CO_SetLCD1(void);
void CO_SetLCD1_1(void);
void CO_SetLCD1_2(void);
void CO_SetLCD1_3(void);
void CO_SetLCD1_4(void);
void CO_SetLCD2(void);
void CO_SetLCD2_1(void);
void CO_SetLCD2_2(void);
void CO_SetLCD2_3(void);
void CO_SetLCD2_4(void);
void CO_SetLCD3(void);
void CO_SetLCD3_1(void);
void CO_SetLCD3_2(void);
void CO_SetLCD3_3(void);
void CO_SetLCD3_4(void);

void CO_SetDisplay1(void);
void CO_SetDisplay2(void);
void CO_SetDisplay3(void);
void CO_SetDisplay4(void);
void CO_SetDisplay5(void);
void CO_SetDisplay6(void);
void CO_SetDisplay7(void);

void CO_ReadTemperature(void);

extern unsigned char rCO_parmIndx1,rCO_parmIndx2,rCO_parmIndx3,rCO_parmIndx4,rCO_parmIndx5,
	rCO_parmIndx7,rCO_parmIndx8;

#endif

