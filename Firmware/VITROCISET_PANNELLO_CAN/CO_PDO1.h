#include "co_pdo.h"

extern rom unsigned long uRPDOMap1;
extern rom unsigned long uRPDOMap2;
extern rom unsigned long uRPDOMap3;
extern rom unsigned long uRPDOMap4;


void CO_COMM_RPDO1_COBIDAccessEvent(void);
void CO_COMM_RPDO1_TypeAccessEvent(void);

void CO_COMM_TPDO1_COBIDAccessEvent(void);
void CO_COMM_TPDO1_TypeAccessEvent(void);
void CO_COMM_TPDO1_ITimeAccessEvent(void);
void CO_COMM_TPDO1_ETimeAccessEvent(void);


void CO_PDO1LSTimerEvent(void);
void CO_RPDO1TXFinEvent(void);
void CO_TPDO1TXFinEvent(void);

