

// The communications conducted in this object incorporates 
// one transmit and one receive endpoint


// Global definitions
#if defined(__18CXX)
#include	"CO_TYPES.H"			// Standard types
#else
#include  "co_main24.h"
#include	"CO_TYPES24.H"
#endif
#include	"CO_DICT.H"				
#include	"CO_COMM.H"				// Communications
#include	"CO_PDO.H"				// PDO functions
#include	"CO_PDO1.H"				// PDO functions


rom unsigned long uRPDOMap1 = 0x02292312L;



UNSIGNED32		uRPDO1Comm;		// RPD1 communication setting
UNSIGNED32		uTPDO1Comm;		// TPD1 communication setting
_PDOBUF 		_uPDO1;
unsigned char 	_uRPDO1Handles,_uTPDO1Handles;




void CO_COMM_PDO1_Create(WORD tSID,WORD rSID) {
	// Convert to MCHP
//	mTOOLS_CO2MCHP(mCOMM_GetNodeID().byte + 0xC0000180L);
//	mTOOLS_CO2MCHP(0x180 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x180L | _uCO_nodeID.byte)

	// Store the COB
	mTPDOSetCOB(1,Canopen2Microchip((tSID << 7) | configParms.nodeID));


	// Convert to MCHP
//	mTOOLS_CO2MCHP(mCOMM_GetNodeID().byte + 0xC0000200L);
//	mTOOLS_CO2MCHP(0x200 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x200L | _uCO_nodeID.byte)
	
	// Store the COB
	mRPDOSetCOB(1,Canopen2Microchip((rSID << 7) | configParms.nodeID));
	}

void CO_COMM_PDO1_Open(BYTE enableRX, BYTE enableTX) {

	/*uRPDOComm1.word = 0x180L + _uCO_nodeID.byte;
	mTOOLS_CO2MCHP(uRPDOComm1.word);
//USARE _CO_COB_CANopen2MCHP_SID(0x180L | _uCO_nodeID.byte)
	uRPDOComm1.word = mTOOLS_GetCOBID();*/

// [was: INVERSIONE! RPDO1 riceve su 202 e TPDO1 lo mettiamo a 182... boh (v. anche demoobjc)]
	// Call the driver and request to open a receive endpoint
	_uRPDO1Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_RPDO_1, uRPDO1Comm.word);
	if(_uRPDO1Handles) {
		if(enableRX)
			COMM_RPDO_1_EN = 1;	
		}

	_uTPDO1Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_TPDO_1, uTPDO1Comm.word);
	if(_uTPDO1Handles) {
		if(enableTX)
			COMM_TPDO_1_EN=1;		// [was: perch� dio porco non li alza l'init? boh]
		}

	}

void CO_COMM_PDO1_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_uRPDO1Handles);
	CANCloseMessage(_uTPDO1Handles);
	COMM_RPDO_1_EN=0;	
	COMM_TPDO_1_EN=0;	
	}

void CO_COMM_RPDO1_RXEvent(unsigned char b) {

	mRPDORead(1);

//LATDbits.LATD8^=1;
// 182 len=5 payload=00 00 00 00 00		// mandiamo questo come cambio di stato (unsolicited) 
// 182 len=0 RTR=1	//	questa � la richiesta del master
// 182 len=5 payload=00 00 00 00 00		// risposta nostra (solicited)
//	*((_DATA8 *)(_uPDO1.RPDO.buf)) = *((__eds__ _DATA8 *)(mCANGetPtrRxData(b))); // con eds ptr
	__eds__ unsigned char *pds=mCANGetPtrRxData(b);
	CO_MEMIO_CopyFromEds(pds,&_uPDO1.RPDO.buf[0],8);
#warning [PORCODIO] ev. usare CO_MEMIO_CopyFromEds(), per problema in EDS/bug compilatore

//	_uPDO1.RPDO.buf[0]=mCANGetDataByte0(b);
//	_uPDO1.RPDO.buf[1]=mCANGetDataByte1(b);
	_uPDO1.RPDO.len = mCANGetDataLen(b);


#ifdef NUM_BYTE_LED_I2C		// non � perfetto ma ok
	if(_uPDO1.RPDO.len == NUM_BYTE_LED_I2C) {
		if(!GestLedI2C(NUM_BYTE_LED_I2C,&uLocalRcvBuffer[0]))
			ErroreI2C=1;
		}
#endif

		
	}

void CO_COMM_TPDO1_RXEvent(unsigned char b) {
//unsigned int n;
	
//	LATGbits.LATG9^=1;

	mTPDORead(1);

// NO!	_uPDO1.TPDO.len = mCANGetDataLen(b);


	if(mCANIsGetRTR(b))	{

		// Tell the stack data is loaded for transmit
		mTPDOWritten(1);
		}

/*	n=GestTasti();
	uTPDOComm1.bytes.B0.byte=LOBYTE(n);
	uTPDOComm1.bytes.B1.byte=HIBYTE(n);

	mCANIsPutReady(0);		// 

	// Set the COB
	// Call the driver, load data to transmit
	*((__eds__ _DATA8 *)(mCANGetPtrTxData(0))) = *((_DATA8 *)(_uPDO1.TPDO.buf));

	mCANPutDataByte0(0,LOBYTE(n));
	mCANPutDataByte1(0,HIBYTE(n));
	_uPDO1.TPDO.len=2;	
	mCANPutDataLen(0,_uPDO1.TPDO.len);

	mTOOLS_CO2MCHP(0x180 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x180L | _uCO_nodeID.byte)

	mTPDOSetCOB(1,mTOOLS_GetCOBID());
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(0)) = uTPDOComm1.word;
	#warning OCCHIO ai buffer TX!
*/

	}

void CO_COMM_RPDO1_TXEvent(BYTE b) {

	}

void CO_COMM_TPDO1_TXEvent(BYTE b) {

//	int i;

//		mCANPutDataByte0(0,LOBYTE(n));
//		mCANPutDataByte1(0,HIBYTE(n));
	// Call the driver, load data to transmit
	*((__eds__ _DATA8 *)(mCANGetPtrTxData(b))) = *((_DATA8 *)(_uPDO1.TPDO.buf));		// qua funziona anche con eds, ma x sicurezza...
//	for(i=0; i<8; i++)
//		*(mCANGetPtrTxData(0)+i)=*(_uPDO1.TPDO.buf+i);

	mCANPutDataLen(b,_uPDO1.TPDO.len);
//	mCANPutDataByte0(0,_uPDO1.TPDO.buf[0]);
//	mCANPutDataByte1(0,_uPDO1.TPDO.buf[1]);
//mCANPutDataByte2(0,_uPDO1.TPDO.buf[2]);
//mCANPutDataByte3(0,_uPDO1.TPDO.buf[3]);


//	mTOOLS_CO2MCHP(0x180 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x180L | _uCO_nodeID.byte)
//	mTPDOSetCOB(1,Canopen2Microchip(0x180 | _uCO_nodeID.byte));
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(b)) = uTPDO1Comm.word;

	}



// Process access events to the COB ID

void CO_COMM_RPDO1_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(mRPDOGetCOB(1));

// USARE _CO_COB_MCHP2CANopen_SID(mRPDOGetCOB(1))
			myCob=Microchip2Canopen(mRPDOGetCOB(1));
			
			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object NON VOGLIAMO SCRIVERE!
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS) {
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mRPDOGetCOB(1)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mRPDOIsOpen(1)) {
						mRPDOClose(1);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(1))).PDO_DIS = 1;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mRPDOGetCOB(1)) & 0xFFFFEFFFL))	{
					// but only open if the PDO endpoint was closed
					if(!mRPDOIsOpen(1)) {
						mRPDOOpen(1);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(1))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		}	
	}



// Process access events to the transmission type
void CO_COMM_TPDO1_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//	mTOOLS_MCHP2CO(mTPDOGetCOB(1));
//USARE _CO_COB_MCHP2CANopen_SID(mTPDOGetCOB(1))
			myCob=Microchip2Canopen(mTPDOGetCOB(1));
			
			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))
			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS)	{
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mTPDOGetCOB(1)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mTPDOIsOpen(1)) {
						mTPDOClose(1);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(1))).PDO_DIS = 1;
				}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mTPDOGetCOB(1)) & 0xFFFFEFFFL)) {
					// but only open if the PDO endpoint was closed
					if(!mTPDOIsOpen(1)) {
						mTPDOOpen(1);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(1))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
			}
		break;
	case DICT_OBJ_INFO: 	// Read the object
		break;
		}	
	}



// Process access events to the transmission type
void CO_COMM_TPDO1_TypeAccessEvent(void) {
	unsigned char tempType;
	
	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the 
			// structure with legth, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// Write the Type to the buffer
			*(uDict.obj->pReqBuf) = configParms.uSyncSet[0];
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			tempType = *(uDict.obj->pReqBuf);
			if((tempType >= 0) && (tempType <= 240)) {
				// Set the new type and resync
				uDemoSyncCount[0] = configParms.uSyncSet[0] = tempType;
				}
			else {
				if((tempType == 254) || (tempType == 255))	{
					configParms.uSyncSet[0] = tempType;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			
			break;
		}	
	}


// Process access events to the inhibit time
void CO_COMM_TPDO1_ITimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}


// Process access events to the event timer
void CO_COMM_TPDO1_ETimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}



void CO_PDO1LSTimerEvent(void) {
	
	}


void CO_RPDO1TXFinEvent(void) {
	
	}

void CO_TPDO1TXFinEvent(void) {
	
	}


