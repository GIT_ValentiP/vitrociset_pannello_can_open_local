/*****************************************************************************
 *
 * Microchip CANopen Stack (Main Managing Routines)
 *
 *****************************************************************************
 * FileName:        CO_MAIN.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.01.17	PIC24 version
 * 
 *****************************************************************************/


#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services

#include	"CO_COMM.H"				// Object
#include	"CO_NMTE.H"				// Error protocols: heartbeat, node-guard


void CO_ProcessAllEvents(void) {

	// Process driver events
	CANEventManager();

	// Process receive events
	CO_COMMRXEventManager();

	// Process transmit events
	CO_COMMTXRdyEventManager();

	// Process transmit events
	CO_COMMTXFinEventManager();
	}



