#include	"CO_MAIN24.H"
#include	"libpic30.H"


// ---------------------------------------------------------------------------------------
void _ISR __attribute__((__no_auto_psv__)) _AddressError(void) {
	Nop();
	Nop();
	}

void _ISR __attribute__((__no_auto_psv__)) _StackError(void) {
	Nop();
	Nop();
	}
	


/******************************************************************************
 * Function:        void _T2Interrupt(void)
 * PreCondition:    None
 * Input:
 * Output:
 * Side Effects:
 * Overview:
 *****************************************************************************/
#ifdef USA_SOFT_PWM_DIMMER 
BYTE LedValue[12];
BYTE Timer64RGB=0;
#endif
BYTE TimerBlink=0;

void __attribute__ (( interrupt, shadow,  no_auto_psv )) _T2Interrupt(void) {
// Dura circa ?uS con ? PWM attivi 
	BYTE n;

#ifdef USA_SOFT_PWM_DIMMER 
	Timer64RGB++;
	Timer64RGB &= 15;

//	if(Timer64RGB>=TriacValue[0])
//		m_TriacO1Bit=0;
//	else
//		m_TriacO1Bit=1;
#endif



	IFS0bits.T2IF = 0; 			//Clear the Timer2 interrupt status flag STRONZI

	}


void __attribute__ (( interrupt, /*shadow, solo UNO! */ no_auto_psv )) _CNInterrupt(void) {
	int i;

	i=PORTF;



	IFS1bits.CNIF = 0;

	}



