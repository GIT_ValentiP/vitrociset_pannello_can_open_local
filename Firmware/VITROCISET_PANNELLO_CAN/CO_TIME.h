/*****************************************************************************
 *
 * Microchip CANopen Stack (SYNC Object)
 *
 *****************************************************************************
 * FileName:        CO_TIME.H
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * CinziaG 					23.10.2017		PIC24E 
 *****************************************************************************/



#ifndef	__CO_TIME_H
#define	__CO_TIME_H


// Object 1009h, this is also defined functionally
// This is also initialized by the app at startup since it could be 
// initialized from non-volitile memory.
extern UNSIGNED32 _uTIME_COBID;


void CO_COMM_TIME_Open(void);

void CO_COMM_TIME_Close(void);

void CO_COMM_TIME_RXEvent(unsigned char);

void CO_COMM_TIME_COBIDAccessEvent(void);

#define	mTIME_SetCOBID(TIME_COB)	_uTIME_COBID.word = TIME_COB;

#define mTIME_GetCOBID()			TIME_COB


#endif	//__CO_TIME_H


