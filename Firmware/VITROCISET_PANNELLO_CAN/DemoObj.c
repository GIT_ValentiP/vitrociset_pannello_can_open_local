/*****************************************************************************
 *
 * Microchip CANopen Stack (Demonstration Object)
 *
 *****************************************************************************
 * FileName:        DemoObj.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		2.2.17	PIC24GU
 * 
 *****************************************************************************/



#include	"CO_MAIN24.H"
#if defined(__PIC24EP512GU810__)		// 
#include "ppsnew.h"
#else
#include "pps.h"
#endif

#include <timer.h>
#include "swi2c.h"
#include "lcd.h"

#include "co_pdo1.h"
#include "co_pdo2.h"
#include "co_pdo3.h"



// These are mapping constants for TPDO1 
// 	starting at 0x1A00 in the dictionary, etc
rom unsigned long uTPDO1Map[2] = {0x60000108,0x60000208};
rom unsigned long uRPDO1Map[2] = {0x62000108,0x62000208};
#ifdef USE_PDO_2
//#ifdef NUM_BYTE_TASTI_I2C		// non � perfetto ma ok
rom unsigned long uTPDO2Map[1] = {		// togliere?
	0x60010108,0x60010208,0x60010308,0x60010408};
//#endif
//#ifdef NUM_BYTE_LED_I2C		// non � perfetto ma ok
rom unsigned long uRPDO2Map[NUM_BYTE_LED_I2C] = {
	0x62010108, 0x62010208, 0x62010308, 0x62010408, 0x62010508, 0x62010608, 0x62010708, 0x62010808};
#endif
rom unsigned long uPDO1Dummy = 0x00000008;
#ifdef USE_PDO_3
#ifdef NUM_ANA
rom unsigned long uTPDO3Map[NUM_ANA] = {0x64000110,0x64000210,0x64000310,0x64000410};
#else
rom unsigned long uTPDO3Map[NUM_PWM] = {0x64000110,0x64000210};
#endif
#ifdef NUM_ANA
rom unsigned long uRPDO3Map[NUM_ANA] = {0x64110110,0x64110210,0x64000310,0x64000410};
#else
rom unsigned long uRPDO3Map[NUM_PWM] = {0x64110110,0x64110210};
#endif
#ifdef USE_PDO_4
rom unsigned long uTPDO4Map = 0x66000108;
rom unsigned long uRPDO4Map = 0x67000108;
#endif
#endif

//unsigned int uIOinFilter[1+(NUM_BYTE_TASTI_I2C/2)];					// 0x6003 filter
//unsigned int uIOinPolarity[1+(NUM_BYTE_TASTI_I2C/2)];				// 0x6002 polarity
//unsigned int uIOinIntChange[1+(NUM_BYTE_TASTI_I2C/2)];				// 0x6006 interrupt on change
//unsigned int uIOinIntRise[1+(NUM_BYTE_TASTI_I2C/2)];					// 0x6007 interrupt on positive edge
//unsigned int uIOinIntFall[1+(NUM_BYTE_TASTI_I2C/2)];					// 0x6008 interrupt on negative edge
//unsigned int uIOinIntEnable[1+(NUM_BYTE_TASTI_I2C/2)];				// 0x6005 enable interrupts

unsigned int uIOinDigiInOld[2];				// 24 tasti //era 22

// Static data referred to by the dictionary
rom unsigned char uDemoTPDO1Len = 2;
rom unsigned char rMaxIndexPuls=2;

#ifdef USA_PWM 
rom unsigned char uDemoTPDO3Len = 2;
#endif


unsigned char uLocalXmtBuffer[8];			// Local buffer for TPDO1
unsigned char uLocalRcvBuffer[8];			// local buffer fot RPDO1
#ifdef USE_PDO_2
unsigned char uLocalXmtBuffer2[8];			// Local buffer for TPDO2
unsigned char uLocalRcvBuffer2[8];			// local buffer fot RPDO2
#endif
#ifdef USE_PDO_3
unsigned char uLocalXmtBuffer3[8];			// Local buffer for TPDO3
unsigned char uLocalRcvBuffer3[8];			// local buffer fot RPDO3
#endif

UNSIGNED8 uDemoState[2]; 					// Bits used to control various states; bit 0..2 
unsigned char uDemoSyncCount[2];			// Counter for synchronous types
//unsigned char uDemoSyncSet[2];				// Internal TPDO type control messi in configParms

unsigned char ErroreI2C;

unsigned int statoLed;
unsigned char statoLedI2C[4];
unsigned char statoDisplay[28];
unsigned char statoLCD[3][4*20];
#ifdef USA_PWM 
unsigned int statoPWM[2];
#endif



void DemoInitHW(void) {
	WORD n;

	// 
	LATA = 0b0000000000000000;		// 
	LATB = 0b0000000000000000;
	LATC = 0b0000000000000000;
	LATD = 0b0000000000000000;
	LATE = 0b0000000000000000;
	LATF = 0b0000000000000000;
	LATG = 0b0000000000000000;
	TRISA= 0b1100011011110000;		// 8 tasti; 2 led; 2 i2c; 
	TRISB= 0b0011110000111111;		// 10 tasti; 4 spare
	TRISC= 0b0011000000011110;		// 4 tasti; 2 switch;
	TRISD= 0b0000000000000011;	    // 2 tasti 6 spare 0b0000000000000000;	//  8 spare; 2 i2c; 2 terminator can; oled (4) 
	TRISE= 0b0000000000000000;		// oled (8); 2 led; pwm;  
	TRISF= 0b0011000100110100;		// RF12 can rx=INPUT; RF13 can tx=INPUT; 4 tasti; rs232 + cts/rts
	TRISG= 0b0010000000000011;		// 2 switch; pwm; 4 spare; I2C sense/reset

	ANSELA=0x0000;
	ANSELB=0x0000;
	ANSELC=0x0000;
	ANSELD=0x0000;
	ANSELE=0x0000;
	ANSELG=0x0000;

	TRISGbits.TRISG15=0;		//buzzer OCCHIO A PWM!



// http://www.microchip.com/forums/m801504.aspx  MBedder !


//	PPSOut(_U1TX, _RP126);      // TXD 
//	PPSIn(_U1RX, _RP125);      // RXD 


#ifdef USA_PWM 
	PPSOut(_OC1, _RP127);      // buzzer 4KHz , qua � rimappabile o PWM
	PPSOut(_OC2, _RP126);      // 
#endif

	PPSIn(_C1RX, _RP108);      // 
	PPSOut(_C1TX, _RP109);      // 

//	TRISFbits.TRISF12=1;		//





//	_PLLDIV=278 /*205*/;						// M = _PLLDIV+2, 2..513
//	_PLLPRE=6;						// N1 = _PLLPRE+2, 2..33
//	_PLLPOST=0;						// N2 = _PLLPOST, 2 (0), 4 (1), 8 (3)
// [Lavoro a 100MHz per ora (v .H anche, v. anche il blocco per dsPIC sopra) ]
// [(215*7.37)/(2*8*2) = 100~]
// (280*8)/(8*2) = 140
// (304*8)/(8*2) = 140

/*	
  Fosc= Fin*M/(N1*N2)
	0.8MHz<(Fin/N1)<8MHz
	100MHz<(Fin*M/N1)<340MHz
	M=2,3,4,...513
	N1=2...33
	N2=2,4,8
	
//	PLLFBD    = M -2;
//	CLKDIVbits.PLLPRE = N1 -2;
//	CLKDIVbits.PLLPOST = 0b00; //N2={2,4,R,8} */

//OSCCONbits.CLKLOCK=1;OSCCONbits.NOSC=1;OSCCONbits.OSWEN=1;
//  while (OSCCONbits.COSC != 0x7)LATB ^= 1;; 

// forse meglio cos�?
  CLKDIVbits.PLLPRE = 0; // N1 = 2		// in quest'ordine, dice http://www.microchip.com/forums/FindPost/1011737 (ma non � vero...))
  CLKDIVbits.PLLPOST = 0; // N2 = 2
  PLLFBD = 68; // M = PLLFBD + 2 = 70


//  __builtin_write_OSCCONH(0x01);
//  __builtin_write_OSCCONL(OSCCON | 0x01);
//  while(OSCCONbits.COSC != 3)				OCCHIO a che OSC si usa!
//	ClrWdt();

  while(OSCCONbits.LOCK != 1)
		ClrWdt();			// boh?



// test clock
#if 0
	TRISD=0;
	while(1) {	/*LED1_IO ^= 1;*/ ClrWdt(); 		// test clock 810
		__builtin_btg((unsigned int *)&LATD,0); }		// 22/1/15: dovrebbe andare a ? MHz (sono 8 cicli v. asm ossia 4 + 4);
#endif



	OpenTimer2(T2_ON & T2_IDLE_CON & T2_GATE_OFF & T2_32BIT_MODE_OFF & T2_PS_1_64 & T2_SOURCE_INT,
		TMR2BASE);		//100Hz per PWM soft e lampeggio led
	ConfigIntTimer2(T2_INT_PRIOR_3 & T2_INT_ON);


#ifdef USA_PWM 

  /* Reset PWM */
  OC1CON1 = 0x0000;
  OC1CON2 = 0x0000;
  
	if(configParms.PWMFreq[0])
		n=70000000L/configParms.PWMFreq[0];
	else
		n=0;
	// siamo a circa 2100Hz (70000000/32768) ago 2017
  /* set PWM duty cycle to 50% */
  OC1R    = n/2 /* basato su SysClock => 64KHz (16MHz / 256) */; //PWM_PERIOD >> 1; /* set the duty cycle tp 50% */
  OC1RS   = n /* se uso Timer come Src SEMBRA NON FARE NULLA... qua boh! */;  //PWM_PERIOD - 1;  /* set the period */
  
  /* configure PWM */
  OC1CON2 = 0x001f;   /* 0x001F = Sync with This OC module                               */
  OC1CON1 = 0x1c00 /* 0x0400 => src=Timer3 */;  /* 0x1C08 = Clock source Fcyc, trigger mode 1, Mode 0 (disable OC1) */
  
  /* enable the PWM */
  OC1CON1 |= 0x0006;   /* Mode 6, Edge-aligned PWM Mode */ 

  OC2CON1 = 0x0000;
  OC2CON2 = 0x0000;
  
	if(configParms.PWMFreq[1])
		n=70000000L/configParms.PWMFreq[1];
	else
		n=0;
  OC2R    = n/2 /* basato su SysClock => 64KHz (16MHz / 256) */; //PWM_PERIOD >> 1; /* set the duty cycle tp 50% */
  OC2RS   = n; //PWM_PERIOD - 1;  /* set the period */
  
  OC2CON2 = 0x001f;   /* 0x001F = Sync with This OC module                               */
  OC2CON1 = 0x1c00 /* 0x0400 => src=Timer3 */;  /* 0x1C08 = Clock source Fcyc, trigger mode 1, Mode 0 (disable OC1) */
  
  OC2CON1 |= 0x0006;   /* Mode 6, Edge-aligned PWM Mode */ 

	GestPWM(configParms.PWMDuty[0],configParms.PWMDuty[1]);

#endif

	setTerminators(0);		// partiamo con 0, poi a fine init vengono messi al valore da eeprom

	}

	
void DemoInit(void) {
	int i;

//	configParms.uSyncSet[0]=configParms.uSyncSet[1] = 255;		// messo in EEprom

/*
	uIOinFilter[0] = 0;
	uIOinPolarity[0] = 0;
	uIOinIntChange[0] = 0x0fff;			// qua fisso 12 
	uIOinIntRise[0] = 0;
	uIOinIntFall[0] = 0;
	uIOinIntEnable[0] = 0x0fff;
	for(i=1; i<(1+NUM_BYTE_TASTI_I2C/2); i++) { 
		uIOinFilter[i] = 0;
		uIOinPolarity[i] = 0;
		uIOinIntChange[i] = 0xffff;			// VEDERE quanti tasti ci sono e mettere maschera apposita!
		uIOinIntRise[i] = 0;
		uIOinIntFall[i] = 0;
		uIOinIntEnable[i] = 0xffff;
		}*/

	uIOinDigiInOld[0] = uLocalXmtBuffer[0] = 0;
	uLocalRcvBuffer[1] = uLocalXmtBuffer[1] = 0;
	uLocalRcvBuffer[2] = uLocalXmtBuffer[2] = 0;
	uLocalRcvBuffer[3] = uLocalXmtBuffer[3] = 0;
	uLocalRcvBuffer[4] = uLocalXmtBuffer[4] = 0;
	uLocalRcvBuffer[5] = uLocalXmtBuffer[5] = 0;
	uLocalRcvBuffer[6] = uLocalXmtBuffer[6] = 0;
	uLocalRcvBuffer[7] = uLocalXmtBuffer[7] = 0;

#ifdef USE_PDO_2
//duplicare anche filtri ecc...?
	uIOinDigiInOld[1] = uLocalXmtBuffer2[0] = 0;
	uLocalRcvBuffer2[1] = uLocalXmtBuffer2[1] = 0;
	uLocalRcvBuffer2[2] = uLocalXmtBuffer2[2] = 0;
	uLocalRcvBuffer2[3] = uLocalXmtBuffer2[3] = 0;
	uLocalRcvBuffer2[4] = uLocalXmtBuffer2[4] = 0;
	uLocalRcvBuffer2[5] = uLocalXmtBuffer2[5] = 0;
	uLocalRcvBuffer2[6] = uLocalXmtBuffer2[6] = 0;
	uLocalRcvBuffer2[7] = uLocalXmtBuffer2[7] = 0;
#endif


	// Set the pointer to the buffers
	mTPDOSetTxPtr(1,(unsigned char *)(&uLocalXmtBuffer[0]));
	
	// Set the pointer to the buffers
	mRPDOSetRxPtr(1,(unsigned char *)(&uLocalRcvBuffer[0]));

	// Set the length
	mTPDOSetLen(1,4);			// 24 tasti era 22 tasti

#ifdef USE_PDO_2

	// Set the pointer to the buffers
	mTPDOSetTxPtr(2,(unsigned char *)(&uLocalXmtBuffer2[0]));

	// Set the pointer to the buffers
	mRPDOSetRxPtr(2,(unsigned char *)(&uLocalRcvBuffer2[0]));		// v. buffer usato in co_pdo.c, _CO_COMM_PDO3_RXEvent()

	// Set the length
	mTPDOSetLen(2,1);
#endif

#ifdef USE_PDO_3
	
	// Set the pointer to the buffers
	mTPDOSetTxPtr(3,(unsigned char *)(&uLocalXmtBuffer3[0]));

	// Set the pointer to the buffers
	mRPDOSetRxPtr(3,(unsigned char *)(&uLocalRcvBuffer3[0]));		// v. buffer usato in co_pdo.c, _CO_COMM_PDO5_RXEvent()

#ifdef NUM_ANA
	// Set the length
	mTPDOSetLen(3,NUM_ANA*2 /* v. numAna sopra... */);
#else
	mTPDOSetLen(3,NUM_PWM*2);
#endif

#endif

	}


void CO_COMMSyncEvent(void) {

	// Process only if in a synchronous mode
	if((configParms.uSyncSet[0] == 0) && (uDemoState[0].bits.b2))	{
		// Reset the synchronous transmit and transfer to async
		uDemoState[0].bits.b2 = 0;
		uDemoState[0].bits.b0 = 1;
		}
	else {
		if((configParms.uSyncSet[0] >= 1) && (configParms.uSyncSet[0] <= 240))	{
			// Adjust the sync counter
			uDemoSyncCount[0]--;
			
			// If time to generate sync
			if(uDemoSyncCount[0] == 0) {
				// Reset the sync counter
				uDemoSyncCount[0] = configParms.uSyncSet[0];
				
				// Start the PDO transmission
				uDemoState[0].bits.b0 = 1;
				}
			}
		}

	// questo dovrebbe essere per i tasti i2c
	if((configParms.uSyncSet[1] == 0) && (uDemoState[1].bits.b2))	{
		uDemoState[1].bits.b2 = 0;
		uDemoState[1].bits.b0 = 1;
		}
	else {
		if((configParms.uSyncSet[1] >= 1) && (configParms.uSyncSet[0] <= 240))	{
			uDemoSyncCount[1]--;
			if(uDemoSyncCount[1] == 0) {
				uDemoSyncCount[1] = configParms.uSyncSet[0];
				uDemoState[1].bits.b0 = 1;
				}
			}
		}

	// potremmo volerne una anche per gli analogici...

	}


void CO_COMMTmstpEvent(void) {
	}	


void DemoProcessEvents(void) {
	unsigned long change;
	unsigned long rise;
	unsigned long fall;
	unsigned int n,n21;
	unsigned long n1,n2,n3;
	unsigned char n1valid,n2valid;
	static unsigned char divider1,divider2,divider3,divider4;

	// Read the input port

//	uLocalXmtBuffer[0] = PORTAbits.RA1;		// puls pres. uomo!
	divider1++;
	if(divider1 > 24) {			//10mSec direi che � ok!  // era 22
		divider1=0;
		n2=GestTasti();
		n=LOWORD(n2);
	  n=n ^ configParms.inputFeat[0].uIOinPolarity;		// 
		uLocalXmtBuffer[0] = LOBYTE(n);
		uLocalXmtBuffer[1] = HIBYTE(n);
		n1=n;
		n=HIWORD(n2);
	  n=n ^ configParms.inputFeat[1].uIOinPolarity;		// 
		uLocalXmtBuffer[2] = LOBYTE(n);
		uLocalXmtBuffer[3] = HIBYTE(n);
		n1 |= ((unsigned long)n) << 16;
		n1valid=TRUE;
		}
	else {
		n1=0;
		n1valid=FALSE;
		}


#ifdef USE_PDO_2			// unire con sopra... basta 1 credo...
	divider2++;
	if(divider2 > 20) {			//20mSec direi che � ok!
		divider2=0;
		divider3++;


		if(divider3 > 100) {			//2Sec 
			divider3=0;
			if(!I2CCheck()) {			// v. define
				mResetI2C2=0;
				__delay_ms(10);
				ErroreI2C |= 1;
				mResetI2C2=1;
				InitI2C();
				GestLedI2C(NUM_BYTE_LED_I2C,statoLedI2C);
				}
			}

		}
	else {
		n2valid=FALSE;
		}
#endif


/* TEST ECHO LED I2C
	GestLedI2C(2,n2);		// TEST i2c ECHO
*/



	

	if(n1valid) {
		// Determine the change if any
		change = MAKELONG(uIOinDigiInOld[0],uIOinDigiInOld[1]);
		change ^= n1;
	
	// passa di qua ogni 1 millisecondi, 4.7.17
	
	
		// Determine if there were any rise events
		rise = MAKELONG(configParms.inputFeat[0].uIOinIntRise,configParms.inputFeat[1].uIOinIntRise);
		rise = (rise & change) & n1;
			
		// Determine if there were any fall events
		fall = MAKELONG(configParms.inputFeat[0].uIOinIntFall,configParms.inputFeat[1].uIOinIntFall);
		fall = (fall & change) & ~n1;
	
		// Determine if there were any change events
		change = MAKELONG(configParms.inputFeat[0].uIOinIntChange,configParms.inputFeat[1].uIOinIntChange) & change;
	
		// Cycle the current value to the old
		uIOinDigiInOld[0] = LOWORD(n1);
		uIOinDigiInOld[1] = HIWORD(n1);
	
		// If any of these are true then indicate an interrupt condition
		if(MAKELONG(configParms.inputFeat[0].uIOinIntEnable,configParms.inputFeat[1].uIOinIntEnable) &
			(change | rise | fall)) 
			uDemoState[0].bits.b1 = 1;
	
		if(uDemoState[0].bits.b1)	{
			switch(configParms.uSyncSet[0])	{
				case 0:				// Asyclic synchronous transmit
					// Set a synchronous transmit flag
					uDemoState[0].bits.b2 = 1;
					break;
	
				case 254:			// Asynchronous transmit
				case 255:						
					// Reset the asynchronous transmit flag
					uDemoState[0].bits.b0 = 1;
					break;
				}
			}
		}


#ifdef USE_PDO_2
	if(n2valid) {
		n2=MAKEWORD(uLocalXmtBuffer2[0],uLocalXmtBuffer2[1]);
		change = uIOinDigiInOld[1] ^ n2;
		rise = (configParms.inputFeat[1].uIOinIntRise & change) & n2;
		fall = (configParms.inputFeat[1].uIOinIntFall & change) & ~n2;
		change = (configParms.inputFeat[1].uIOinIntChange & change);
		uIOinDigiInOld[1] = n2;
		// If any of these are true then indicate an interrupt condition
		if(configParms.inputFeat[1].uIOinIntEnable & (change | rise | fall)) 
			uDemoState[1].bits.b1 = 1;

		n2=MAKEWORD(uLocalXmtBuffer2[2],uLocalXmtBuffer2[3]);
		change = uIOinDigiInOld[2] ^ n2;
		rise = (configParms.inputFeat[2].uIOinIntRise & change) & n2;
		fall = (configParms.inputFeat[2].uIOinIntFall & change) & ~n2;
		change = (configParms.inputFeat[2].uIOinIntChange & change);
		uIOinDigiInOld[2] = n2;
		if(configParms.inputFeat[2].uIOinIntEnable & (change | rise | fall)) 
			uDemoState[1].bits.b1 = 1;


		if(uDemoState[1].bits.b1)	{
			switch(configParms.uSyncSet[1])	{
				case 0:				// Asyclic synchronous transmit
					// Set a synchronous transmit flag
					uDemoState[1].bits.b2 = 1;
					break;
	
				case 254:			// Asynchronous transmit
				case 255:						
					// Reset the asynchronous transmit flag
					uDemoState[1].bits.b0 = 1;
					break;
				}
			}
		}
#endif

#ifdef USE_PDO_3		// era Analogici
#endif


	if(uDemoState[0].bits.b0)	{
	// If ready to send 
		if(mTPDOIsPutRdy(1))	{

			mTPDOWritten(1);
		
//	LATDbits.LATD9^=1;

		// Reset any synchronous or asynchronous flags
			uDemoState[0].bits.b0 = 0;
			uDemoState[0].bits.b1 = 0;
			}
		else
			uDemoState[0].byte = 0;
		}

#ifdef USE_PDO_2
	if(uDemoState[1].bits.b0)	{
	// If ready to send 
		if(mTPDOIsPutRdy(2))	{

			mTPDOWritten(2);
	
			// Reset any synchronous or asynchronous flags
			uDemoState[1].bits.b0 = 0;
			uDemoState[1].bits.b1 = 0;
			}
		else
			uDemoState[1].byte = 0;
		}
#endif

#ifdef USE_PDO_3
	//pwm
#endif


	}


#define INVALID_READOUT 9999		//meglio di ffff !!

signed int leggi_tempMCP(BYTE q) {
	WORD n;
	int n1;


/*    StartI2C1(); 
    IdleI2C1();
    MasterWriteI2C1(WRITE_ADD); //Send command
    IdleI2C1();
    MasterWriteI2C1(MEAS_X); //Read X command
    IdleI2C1();
    StopI2C1();
    IdleI2C1();
    delay100();   //100 microsecond delay
    StartI2C1();
    IdleI2C1();
    MasterWriteI2C1(READ_ADD);  //Read values
    IdleI2C1();
    bhi = MasterReadI2C1();  //High 8 bits
    AckI2C1();
    IdleI2C1();      
    blow = MasterReadI2C1();  //Low 4 bits
    NotAckI2C1();
    IdleI2C1();
    StopI2C1();
    IdleI2C1();
*/

	I2CSTART2();
	if(I2CTXByte2(0x90 | (q << 1))) {		// mando address (FORZO WR)
    I2CTXByte2(0);
    I2CSTART2();
    I2CTXByte2(0x91 | (q << 1));		// mando address (FORZO RD)
    n=I2CRXByte2();
		I2CBITOUT2(0);						// to continue transmission
    n <<= 8;
    n |= I2CRX1Byte2();
//    if(!n || n==0xffff)		// segnalo errore se chip guasto TOLTO con check su ACK
//      return INVALID_READOUT;

		n1=n;
    n1 >>= 7;		// @ 9 bit
    n1 *= 10;
    n1 /= 2;
    }
  else
    n1=INVALID_READOUT;
	I2CSTOP2();

	return n1;
  }


void CO_SetLCD1(void) {			// x comandi "evoluti"

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			switch(uDict.obj->pReqBuf[0]) {
				case 1:
					LCDCls(0);
					break;
				}	
			break;
		}	
	}

void CO_SetLCD1_1(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[0][0],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[0][0],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=0;LCDXY_(0);
				LCDWriteN(0,&statoLCD[0][0],20);
				}
			break;
		}	
	}

void CO_SetLCD1_2(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[0][20],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[0][20],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=1;LCDXY_(0);
				LCDWriteN(0,&statoLCD[0][20],20);
				}
			break;
		}	
	}

void CO_SetLCD1_3(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[0][40],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[0][40],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=2;LCDXY_(0);
				LCDWriteN(0,&statoLCD[0][40],20);
				}
			break;
		}	
	}

void CO_SetLCD1_4(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[0][60],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[0][60],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=3;LCDXY_(0);
				LCDWriteN(0,&statoLCD[0][60],20);
				}
			break;
		}	
	}

void CO_SetLCD2(void) {			// x comandi "evoluti"

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			switch(uDict.obj->pReqBuf[0]) {
				case 1:
					LCDCls(1);
					break;
				}	
			break;
		}	
	}

void CO_SetLCD2_1(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[1][0],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[1][0],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=0;LCDXY_(1);
				LCDWriteN(1,&statoLCD[1][0],20);
				}
			break;
		}	
	}

void CO_SetLCD2_2(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[1][20],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[1][20],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=1;LCDXY_(1);
				LCDWriteN(1,&statoLCD[1][20],20);
				}
			break;
		}	
	}

void CO_SetLCD2_3(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[1][40],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[1][40],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=2;LCDXY_(1);
				LCDWriteN(1,&statoLCD[1][40],20);
				}
			break;
		}	
	}

void CO_SetLCD2_4(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[1][60],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[1][60],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=3;LCDXY_(1);
				LCDWriteN(1,&statoLCD[1][60],20);
				}
			break;
		}	
	}

void CO_SetLCD3(void) {			// x comandi "evoluti"

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			switch(uDict.obj->pReqBuf[0]) {
				case 1:
					LCDCls(2);
					break;
				}	
			break;
		}	
	}

void CO_SetLCD3_1(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[2][0],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[2][0],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=0;LCDXY_(2);
				LCDWriteN(2,&statoLCD[2][0],20);
				}
			break;
		}	
	}

void CO_SetLCD3_2(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[2][20],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[2][20],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=1;LCDXY_(2);
				LCDWriteN(2,&statoLCD[2][20],20);
				}	
			break;
		}	
	}

void CO_SetLCD3_3(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[2][40],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[2][40],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=2;LCDXY_(2);
				LCDWriteN(2,&statoLCD[2][40],20);
				}
			break;
		}	
	}

void CO_SetLCD3_4(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoLCD[2][60],uDict.obj->pReqBuf,20);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoLCD[2][60],20);
			if(mNMT_StateIsOperational()) {
				LCDX=0;LCDY=3;LCDXY_(2);
				LCDWriteN(2,&statoLCD[2][60],20);
				}
			break;
		}	
	}


void CO_SetDisplay1(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[0],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[0],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(0,&statoDisplay[0]);
				}
			break;
		}	
	}

void CO_SetDisplay2(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[4],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[4],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(1,&statoDisplay[4]);
				}
			break;
		}	
	}

void CO_SetDisplay3(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[8],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[8],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(2,&statoDisplay[8]);
				}
			break;
		}	
	}

void CO_SetDisplay4(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[12],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[12],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(3,&statoDisplay[12]);
				}
			break;
		}	
	}

void CO_SetDisplay5(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[16],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[16],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(4,&statoDisplay[16]);
				}
			break;
		}	
	}

void CO_SetDisplay6(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[20],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[20],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(5,&statoDisplay[20]);
				}
			break;
		}	
	}

void CO_SetDisplay7(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			CO_MEMIO_CopySram(&statoDisplay[24],uDict.obj->pReqBuf,4);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			CO_MEMIO_CopySram(uDict.obj->pReqBuf,&statoDisplay[24],4);
			if(mNMT_StateIsOperational()) {
				GestDisplay(6,&statoDisplay[24]);
				}
			break;
		}	
	}

void CO_ReadTemperature(void) {
	WORD n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
//			if(mNMT_StateIsOperational()) {
			n=leggi_tempMCP(0);			// operational??
			CO_MEMIO_CopySram(&n,uDict.obj->pReqBuf,2);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			break;
		}	
	}


