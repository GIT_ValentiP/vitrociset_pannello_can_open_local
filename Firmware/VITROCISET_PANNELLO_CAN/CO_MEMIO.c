/*****************************************************************************
 *
 * Microchip CANopen Stack (Memory APIs)
 *
 *****************************************************************************
 * FileName:        CO_MEMIO.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * This file contains some basic memory copy APIs. They are designed to 
 * accelerate data movement above typical looped index coppies.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * GC								17.3.17				PIC24 e poi pointer eds
 * 
 *****************************************************************************/



#include	"CO_DEFS.DEF"			// Global definitions
#include	"CO_MEMIO.H"


// Private structures used to set block copy sizes
typedef struct _DATA_BLOCK_16 {
	unsigned char bytes[16];
	} _DATA16;

typedef struct _DATA_BLOCK_8 {
	unsigned char bytes[8];
	} _DATA8;

typedef struct _DATA_BLOCK_4 {
	unsigned char bytes[4];
	} _DATA4;




void CO_MEMIO_CopySram(unsigned char *pIn, unsigned char *pOut, unsigned int len) {	
// questa funziona per ptr normali; le versioni EDS potevano andare per copia RAM-RAM ma NON ROM-RAM...

	while(len--) {
		*pOut++ = *pIn++;
		}
	}

void CO_MEMIO_CopyToEds(unsigned char *pIn, __eds__ unsigned char *pOut, unsigned int len) {	

	while(len--) {
		*pOut++ = *pIn++;
		}
	}

void CO_MEMIO_CopyFromEds(__eds__ unsigned char *pIn, unsigned char *pOut, unsigned int len) {	

	while(len--) {
		*pOut++ = *pIn++;
		}
	}

