/*****************************************************************************
 *
 * Microchip CANopen Stack (Process Data Objects)
 *
 *****************************************************************************
 * FileName:        CO_PDO.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		29.3.17		PIC24E/GU
 * 									20.7.17		rimesso un po' tutto a posto :)
 *****************************************************************************/


#include "co_main24.h"
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services

#include	"CO_COMM.H"				// Object
#include	"CO_PDO.H"



#if CO_NUM_OF_PDO < 1
	#error "Number of PDOs too low, must have at least 1..."
#endif

#if CO_NUM_OF_PDO > 6
	#error "Number of PDOs too high, this version only supports up to 4 PDOs..."
#endif


