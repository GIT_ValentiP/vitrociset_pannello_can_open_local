/*****************************************************************************
 *
 * Microchip CANopen Stack (Network Management and Communications)
 *
 *****************************************************************************
 * FileName:        CO_NMT.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 *
 * This is the emergency management object.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio 	19.7.17		PIC24E
 * 
 *****************************************************************************/



// Global definitions
#if defined(__18CXX)
#include	"CO_TYPES.H"			// Data types
#include	"CO_CANDRV.H"			// Driver services
#else
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services
#endif
#include	"CO_COMM.H"				// Object
#include	"CO_TOOLS.H"			// COB ID tools

#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include  "co_main24.h"
#endif
#include	"CO_dev.H"				// Object

#include	"CO_EMCY.H"				// Object


UNSIGNED32 		_uEMCY_COBID;
	
void CO_COMM_EMCY_Open(void) {
//#warning FINIRE emergency, non riceve=no endpoint si spera

	_uEMCY_COBID.word=Canopen2Microchip(0x80L | configParms.nodeID);		// creo cmq una variabile per metterci CobID

	COMM_NETCTL_EMCY_EN=1;
	}

void _CO_COMM_EMCY_Close(void) {

	COMM_NETCTL_EMCY_EN = 0;
	}
	
void CO_COMM_EMCY_TXEvent(BYTE b) {

		// Set the COB
//	mTOOLS_CO2MCHP(0x80 | _uCO_nodeID.byte);
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(b)) = _uEMCY_COBID.word;

	// Set the length
	mCANPutDataLen(b,8);
		
// http://www.canopensolutions.com/english/about_canopen/emergency.shtml
	__eds__ unsigned char *pds=mCANGetPtrTxData(b);

	pds[0]=LOBYTE(LOWORD(uCO_DevErrReg2[0]));		// ultimo error register
	pds[1]=HIBYTE(LOWORD(uCO_DevErrReg2[0]));
	pds[2]=uCO_DevErrReg;		// error code 
	pds[3]=0;
	pds[4]=LOBYTE(HIWORD(uCO_DevErrReg2[0]));		//ossia ErroreI2C;			// i2c, TABELLARE! v. anche stuckKey
	pds[5]=0;
	pds[6]=0;
	pds[7]=0;


	uCO_DevErrReg=0;		//mah facciam cos�
	}

void CO_COMM_EMCY_TXFinEvent(void) {
	}

void CO_COMM_EMCY_LSTimerEvent(void) {
	}



void CO_COMM_EMCY_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information not required for this object
			break;

		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(_uSYNC_COBID.word);
// USARE _CO_COB_MCHP2CANopen_SID(_uSYNC_COBID.word)

			myCob=Microchip2Canopen(_uEMCY_COBID.word);

		// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = mTOOLS_GetCOBID();
			break;

		case DICT_OBJ_WRITE: 	// Write the object NO! non si fa!
			mCO_DictSetRet(E_CANNOT_WRITE);
			break;
		}	
	}



