/*****************************************************************************
 *
 * Microchip CANopen Stack (Test)
 *
 *****************************************************************************
 * FileName:        exttst.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.20.00 or higher
 * Linker:          MPLINK 03.40.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.01.17	PIC24 version 
 * 
 *****************************************************************************/

#include	<P24exxxx.H>
#include	"CO_MAIN24.H"
#include	"libpic30.H"


#include	"demoobj.H"
#include "co_pdo1.h"
#include "co_pdo2.h"
#include "co_pdo3.h"


void CO_NMTStateChangeEvent(void) {

	if(mNMT_StateIsOperational()) {		// controllare stato precedente?
		// Tell the stack data is loaded for transmit
		mTPDOWritten(1);			 // tasti

		GestDisplay(0,&statoDisplay[0]);
		GestDisplay(1,&statoDisplay[4]);
		GestDisplay(2,&statoDisplay[8]);
		GestDisplay(3,&statoDisplay[12]);
		GestDisplay(4,&statoDisplay[16]);
		GestDisplay(5,&statoDisplay[20]);
		GestDisplay(6,&statoDisplay[24]);

// finire con tutti
//				LCDX=0;LCDY=1;LCDXY_(2);
//				LCDWriteN(2,&statoLCD[2][20],20);


		GestLedI2C(NUM_BYTE_LED_I2C,&uLocalRcvBuffer[0]);// DUE DI SEGUITO non /andavano/vanno! usare pi� buffer tx...
// o inserire 	mCANSendMessage();

#warning vedere il buffer tx in uso!

#ifdef NUM_ANA
// per ora non vuole 22.9.17 o forse s�
//		GestPWM(statoPWM[0],statoPWM[1]);
		mTPDOWritten(3);
#endif

		}
	}

void CO_NMTResetEvent(void) {

// @#�$%	Reset();
	__asm__ volatile ("reset");  

	}

void CO_NMTAppResetRequest(void) {
	}


void CO_NMTENodeGuardErrEvent(void) {
	
	mLed0 = 1;
	mLed1 = 1;
	__delay_ms(100);
	ClrWdt();
	mLed2 = 1;
	__delay_ms(200);
	ClrWdt();
	mLed0 = 0;
	mLed1 = 0;
	__delay_ms(200);
	ClrWdt();
	mLed2 = 0;
	}


