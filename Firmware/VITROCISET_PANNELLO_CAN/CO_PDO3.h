#include "co_pdo.h"

#ifdef USE_PDO_3
void CO_COMM_PDO3_Create(WORD,WORD);
void CO_COMM_PDO3_Open(BYTE, BYTE);
void CO_COMM_PDO3_Close(void);
void CO_COMM_RPDO3_RXEvent(unsigned char);
void CO_COMM_RPDO3_TXEvent(BYTE);
void CO_COMM_TPDO3_RXEvent(unsigned char);
void CO_COMM_TPDO3_TXEvent(BYTE);

void CO_COMM_RPDO3_COBIDAccessEvent(void);
void CO_COMM_RPDO3_TypeAccessEvent(void);
void CO_COMM_TPDO3_COBIDAccessEvent(void);
void CO_COMM_TPDO3_TypeAccessEvent(void);

void CO_PDO3LSTimerEvent(void);
void CO_RPDO3TXFinEvent(void);
void CO_TPDO3TXFinEvent(void);
#endif


