/*****************************************************************************
 *
 * Microchip CANopen Stack (ECAN Driver For CANopen)
 *
 *****************************************************************************
 * FileName:        CO_CANDRV.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * GC 							23.1.17		PIC24E version
 * 
 *****************************************************************************/

// http://www.microchip.com/forums/m974604-p2.aspx#976052



#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include  "co_main24.h"
#endif
//#include	"CO_DEFS24.DEF"			// Global definitions
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services
#include	<libpic30.H>



// Definition of a Microchip CAN filter
typedef union __FILTER {
	unsigned long dword;
	struct __BYTES	{
		unsigned char	SIDL;
		unsigned char	SIDH;
		unsigned char EIDL;
		unsigned char EIDH;
		} bytes;
	} _FILTER;


// Parameter area


// Driver related static data
unsigned char 	_uCAN_Bitrate;						// Stored bitrate
unsigned char 	_uCANRxHndls[CAN_MAX_RCV_ENDP];		// Filter handles
_FILTER			_uCANRxIDRes[CAN_MAX_RCV_ENDP];		// Filters
unsigned char 	_uCANTxHndls[3];					// Transmit handles

unsigned char _uCANReq;					// Queued driver request

unsigned char _uCANOldMode;


__eds__ unsigned int ecan1MsgBuf[NUM_OF_ECAN_BUFFERS][8] __attribute__((__eds__,space(dma),aligned(NUM_OF_ECAN_BUFFERS*16)));
// uso i primi 8 per TX (attivo 4 per ora, v. relativi flag) e i secondi 8 per rx...
// v. ERRATA PIC !!
__eds__ unsigned char *ecanRXBufPtr=(unsigned char *)&ecan1MsgBuf[BASE_BUFFER_RX];		// non � bellissimo ma... :)
__eds__ unsigned char *ecanTXBufPtr=(unsigned char *)&ecan1MsgBuf[0];		// non � bellissimo ma... :)


/* ******** CAN Control Services ******* */

void CANEventManager(void) {

	if(_uCANReq)	{
		switch(_uCANReq)	{	
			// Filter has been modified, re-sync		
			case 1:	
				C1CTRL1bits.WIN = 1;		// mi preparo, nel caso

				// Check for config mode
				if(C1CTRL1bits.OPMODE2)	{

					C1RXM0SIDbits.SID = 0xf80;		// per NMT??
					C1RXM0SIDbits.EID = 0;
					C1RXM0EIDbits.EID = 0;		// 
					// Acceptance Filter 0 to check for Standard Identifier 
					C1RXM0SIDbits.MIDE = 1;
					C1RXM1SIDbits.SID = 0xfff;		// il SID ricevuto AND C1RXM0SID devono essere uguali a questo!
					C1RXM1SIDbits.EID = 0;
					C1RXM1EIDbits.EID = 0;		// 
					C1RXM1SIDbits.MIDE = 1;

// v. anche in init... sopra pare non prenderli @##�$%


					if(_uCANRxHndls[0]) {
						//*(_FILTER *)(&C1RXF0SID) = _uCANRxIDRes[0].dword << 3; 
						C1RXF0SID = _uCANRxIDRes[0].dword << 3; 	// trascod. microchip PIC24
// sia MIDE che EXIDE vengono messi a 0 qua... e sopra in C1RXM0SIDbits!

// bah usare forse						CAN_CID aCID;
//						aCID.std.CxTRBnSID.SID=_uCANRxIDRes[0].dword;
//						C1RXF0SID = aCID.std; 	// trascod. microchip PIC24

						C1BUFPNT1bits.F0BP = BASE_BUFFER_RX;		// filtro 0=>buffer 4+0
						C1FMSKSEL1bits.F1MSK=1;
						C1FEN1bits.FLTEN0 = 1;
						}
					else 
						C1FEN1bits.FLTEN0 = 0;

					#if CAN_MAX_RCV_ENDP > 1
					if(_uCANRxHndls[1]) {
						C1RXF1SID = _uCANRxIDRes[1].dword << 3; 
						C1BUFPNT1bits.F1BP = BASE_BUFFER_RX+1;		// filtro 1=>buffer 4+1
						C1FMSKSEL1bits.F2MSK=1;
						C1FEN1bits.FLTEN1 = 1;
						}
					else 
						C1FEN1bits.FLTEN1 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 2
					if(_uCANRxHndls[2]) {
						C1RXF2SID = _uCANRxIDRes[2].dword << 3; 
//						*(_FILTER *)(&C1RXF2SID) = _uCANRxIDRes[2].dword << 3; 
						C1BUFPNT1bits.F2BP = BASE_BUFFER_RX+2;		// filtro 2=>buffer 4+2
						C1FMSKSEL1bits.F2MSK=1;
						C1FEN1bits.FLTEN2 = 1;
						}
					else 
						C1FEN1bits.FLTEN2 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 3
					if(_uCANRxHndls[3]) {
						C1RXF3SID = _uCANRxIDRes[3].dword << 3; 
						C1BUFPNT1bits.F3BP = BASE_BUFFER_RX+3;		// filtro 3=>buffer 4+3
						C1FMSKSEL1bits.F3MSK=1;
						C1FEN1bits.FLTEN3 = 1;
						}
					else 
						C1FEN1bits.FLTEN3 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 4
					if(_uCANRxHndls[4]) {
						C1RXF4SID = _uCANRxIDRes[4].dword << 3; 
						C1BUFPNT2bits.F4BP = BASE_BUFFER_RX+4;		// filtro 4=>buffer 4+4
						C1FMSKSEL1bits.F4MSK=1;
						C1FEN1bits.FLTEN4 = 1;
						}
					else 
						C1FEN1bits.FLTEN4 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 5
					if(_uCANRxHndls[5]) {
						C1RXF5SID = _uCANRxIDRes[5].dword << 3; 
						C1BUFPNT2bits.F5BP = BASE_BUFFER_RX+5;		// filtro 5=>buffer 4+5
						C1FMSKSEL1bits.F5MSK=1;
						C1FEN1bits.FLTEN5 = 1;
						}
					else 
						C1FEN1bits.FLTEN5 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 6
					if(_uCANRxHndls[6]) {
						C1RXF6SID = _uCANRxIDRes[6].dword << 3; 
						C1BUFPNT2bits.F6BP = BASE_BUFFER_RX+6;		// filtro 6=>buffer 4+6
						C1FMSKSEL1bits.F6MSK=1;
						C1FEN1bits.FLTEN6 = 1;
						}
					else 
						C1FEN1bits.FLTEN6 = 0;
					#endif

					#if CAN_MAX_RCV_ENDP > 7
					if(_uCANRxHndls[7]) {
						C1RXF7SID = _uCANRxIDRes[7].dword << 3; 
						C1BUFPNT2bits.F7BP = BASE_BUFFER_RX+7;		// filtro 7=>buffer 4+7
						C1FMSKSEL1bits.F7MSK=1;
						C1FEN1bits.FLTEN7 = 1;
						}
					else 
						C1FEN1bits.FLTEN7 = 0;
					#endif

					#if CAN_MAX_RCV_ENDP > 8
					if(_uCANRxHndls[8]) {
						C1RXF8SID = _uCANRxIDRes[8].dword << 3; 
						C1BUFPNT3bits.F8BP = BASE_BUFFER_RX+8;		// filtro 8=>buffer 4+8
						C1FMSKSEL2bits.F8MSK=1;
						C1FEN1bits.FLTEN8 = 1;
						}
					else 
						C1FEN1bits.FLTEN8 = 0;
					#endif

					#if CAN_MAX_RCV_ENDP > 9
					if(_uCANRxHndls[9]) {
						C1RXF9SID = _uCANRxIDRes[9].dword << 3; 
						C1BUFPNT3bits.F9BP = BASE_BUFFER_RX+9;		// filtro 9=>buffer 4+9
						C1FMSKSEL2bits.F9MSK=1;
						C1FEN1bits.FLTEN9 = 1;
						}
					else 
						C1FEN1bits.FLTEN9 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 10
					if(_uCANRxHndls[10]) {
						C1RXF10SID = _uCANRxIDRes[10].dword << 3; 
						C1BUFPNT3bits.F10BP = BASE_BUFFER_RX+10;		// filtro 8=>buffer 4+10
						C1FMSKSEL2bits.F10MSK=1;
						C1FEN1bits.FLTEN10 = 1;
						}
					else 
						C1FEN1bits.FLTEN10 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 11
					if(_uCANRxHndls[11]) {
						C1RXF11SID = _uCANRxIDRes[11].dword << 3; 
						C1BUFPNT3bits.F11BP = BASE_BUFFER_RX+11;		// filtro 8=>buffer 4+11
						C1FMSKSEL2bits.F11MSK=1;
						C1FEN1bits.FLTEN11 = 1;
						}
					else 
						C1FEN1bits.FLTEN11 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 12
aaaaaCCHIO!					if(_uCANRxHndls[12]) {
						C1RXF12SID = _uCANRxIDRes[12].dword << 3; 
						C1BUFPNT3bits.F12BP = BASE_BUFFER_RX+12;		// filtro 8=>buffer 4+12
						C1FMSKSEL2bits.F12MSK=1;
						C1FEN1bits.FLTEN12 = 1;
						}
					else 
						C1FEN1bits.FLTEN12 = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 13
					if(_uCANRxHndls[13]) {
						*(_FILTER *)(&RXF13SIDH) = _uCANRxIDRes[13]; 
						RXFCON1bits.RXF13EN = 1;
						}
					else 
						RXFCON1bits.RXF13EN = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 14
					if(_uCANRxHndls[14]) {
						*(_FILTER *)(&RXF14SIDH) = _uCANRxIDRes[14]; 
						RXFCON1bits.RXF14EN = 1;
						}
					else 
						RXFCON1bits.RXF14EN = 0;
					#endif
					#if CAN_MAX_RCV_ENDP > 15
					if(_uCANRxHndls[15]) {
						*(_FILTER *)(&RXF15SIDH) = _uCANRxIDRes[15]; 
						RXFCON1bits.RXF15EN = 1;
						}
					else 
						RXFCON1bits.RXF15EN = 0;
					#endif
					
					// Cancel the request
					_uCANReq = 0;
					
					// Request normal operation
					C1CTRL1bits.REQOP = _uCANOldMode;
					}
				
				// Request config mode
				else 
					C1CTRL1bits.REQOP = 0b100;
				C1CTRL1bits.WIN = 0;
				break;
				
			case 2:
				// Check for config mode
				if(C1CTRL1bits.REQOP2)	{
					// Set the bitrate
					CANSetBitRate();
					
					// Cancel the request
					_uCANReq = 0;
					
					// Request normal operation
					C1CTRL1bits.REQOP = _uCANOldMode;
					}
				// Request config mode
				else 
					C1CTRL1bits.REQOP = 0b100;
				break;
			}			
		}
	}


void CANReset(void) {

	// Empty an queued requests
	_uCANReq = 0;
	
	C1CTRL1=C1CTRL2=0;
	C1INTE=0;
	C1FEN1=0;
	C1CTRL1bits.WIN = 0;

	// Force the CAN peripheral into Config mode
//	C1CTRL1bits.REQOP = 0b010;		// loopback
	C1CTRL1bits.REQOP = 0b100;		// config
	while(C1CTRL1bits.OPMODE != 0b100) 
		ClrWdt();
	

	// Set the bitrate
	CANSetBitRate();
	
	// Make all programmable buffers receive buffers
//	BSEL0 = 0x00;

  C1FCTRL = 0xC01F; // No FIFO, 32 Buffers

	// Assign 32x8word Message Buffers for ECAN1 in device RAM. This example uses DMA0 for TX.
	// Refer to 21.8.1 �DMA Operation for Transmitting Data� for details on DMA channel configuration for ECAN transmit.
	DMAPWC = 0;
  DMARQC = 0;

	DMA0CON=0;
	DMA0CONbits.SIZE = 0x0;
	DMA0CONbits.DIR = 0x1;
	DMA0CONbits.AMODE = 0x2;
	DMA0CONbits.MODE = 0x0;
	DMA0REQ = 0x46;
	DMA0CNT = 7;
	DMA0PAD = (volatile unsigned int)&C1TXD;
	DMA0STAL = __builtin_dmaoffset(&ecan1MsgBuf);
	DMA0STAH = __builtin_dmapage(&ecan1MsgBuf);
	DMA0CONbits.CHEN = 0x1;

	DMA1CON=0;
	DMA1CONbits.SIZE = 0x0;
	DMA1CONbits.DIR = 0x0;
	DMA1CONbits.AMODE = 0x2;
	DMA1CONbits.MODE = 0x0;
	DMA1REQ = 0x22;
	DMA1CNT = 7;
	DMA1PAD = (volatile unsigned int)&C1RXD;
	DMA1STAL = __builtin_dmaoffset(&ecan1MsgBuf);
	DMA1STAH = __builtin_dmapage(&ecan1MsgBuf);
	DMA1CONbits.CHEN = 0x1;

//	__eds__ BYTE *ecanRXBufPtr=&ecan1MsgBuf[b+8];		// non � bellissimo ma... :)

	// Setup the transmit/receive buffers
	C1TR01CON = 0b1000000010000000;		// questi 2 � transmit, v. anche sotto (per ora)
	C1TR23CON = 0;
	C1TR45CON = 0;
	C1TR67CON = 0;
	C1RXFUL1 = 0;
	C1RXFUL2 = 0;
	
	
	// Place the module in enhanced legacy mode, buffer 0
// boh...	ECANCON = 0x50;
	
	// Disable all filters
	C1FEN1 = 0;
	
	// No data byte filtering
// boh	SDFLC = 0;
	

	C1CTRL1bits.WIN = 0;			// non sarebbe necessario, questi sono in ambo i banchi...
	// Setup filter/mask association to Mask 0
	C1FMSKSEL1 = C1FMSKSEL2 = 0;
	// Select Acceptance Filter Mask 0 for Acceptance Filter 0 
	C1FMSKSEL1bits.F0MSK=0;		// uso la mask 0 per tutti... ?? no, nmt
	C1FMSKSEL1bits.F1MSK=1;
	C1FMSKSEL1bits.F2MSK=1;
	C1FMSKSEL1bits.F3MSK=1;
	C1FMSKSEL1bits.F4MSK=1;
	C1FMSKSEL1bits.F5MSK=1;
	C1FMSKSEL1bits.F6MSK=1;
	C1FMSKSEL1bits.F7MSK=1;
	C1FMSKSEL2bits.F8MSK=1;
	C1FMSKSEL2bits.F9MSK=1;
	// Configure Acceptance Filter Mask 0 register to mask SID<2:0> BOH PERCHE'?? era una demo...


	C1CTRL1bits.WIN = 0;
	// Filter 0 enabled for Identifier match with incoming message 
//	C1FEN1bits.FLTEN0=0x1;

	// Set I/O control
	
	C1TR01CONbits.TXEN0 = 1;			// preparo 4 buffer come tx... (gli altri saranno rx)
	C1TR01CONbits.TX0PRI = 3;
	C1TR01CONbits.TXEN1 = 1;
	C1TR01CONbits.TX1PRI = 2;
	C1TR23CONbits.TXEN2 = 0;
	C1TR23CONbits.TX2PRI = 2;
	C1TR23CONbits.TXEN3 = 0;
	C1TR23CONbits.TX3PRI = 2;
// e gli altri...

	// Set interrupts
//	C1INTE = 0x0003;		// mah, non li uso...
	C1INTE = 0x0000;

	
	// Reset all handles
	_uCANRxHndls[0] = 0;
	#if CAN_MAX_RCV_ENDP > 1
	_uCANRxHndls[1] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 2
	_uCANRxHndls[2] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 3
	_uCANRxHndls[3] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 4
	_uCANRxHndls[4] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 5
	_uCANRxHndls[5] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 6
	_uCANRxHndls[6] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 7
	_uCANRxHndls[7] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 8
	_uCANRxHndls[8] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 9
	_uCANRxHndls[9] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 10
	_uCANRxHndls[10] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 11
	_uCANRxHndls[11] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 12
	_uCANRxHndls[12] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 13
	_uCANRxHndls[13] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 14
	_uCANRxHndls[14] = 0;
	#endif
	#if CAN_MAX_RCV_ENDP > 15
	_uCANRxHndls[15] = 0;
	#endif
	}




void CANSetBitRate(void) {

	switch(_uCAN_Bitrate) {
		case 0:							/* 2Mbit */
			ecan1ClkInit(2000000L);
			break;
		case 1:							/* 1Mbits */
			ecan1ClkInit(1000000L);
			break;
		case 2:							/* 800kbits */
			ecan1ClkInit(800000L);
			break;
		case 3:							/* 500kbits */
			ecan1ClkInit(500000L);
			break;
		case 4:							/* 250kbits */
			ecan1ClkInit(250000L);
			break;
		case 5:							/* 125kbits */
			ecan1ClkInit(125000L);
			break;
		case 6:							/* 50kbits */
			ecan1ClkInit(50000L);
			break;
		case 7:							/* 20kbits */
			ecan1ClkInit(20000L);
			break;
		case 8:							/* 10kbits */
			ecan1ClkInit(10000L);
			break;
		default:
			C1CFG1 = CAN_BITRATE0_BRGCON1;
			C1CFG2 = CAN_BITRATE0_BRGCON2;
			break;
		}
	}




// CAN Baud Rate Configuration 
//	da http://www.microchip.com/forums/m766230.aspx , USARE!

#define FCAN    140000000L
// set baud rate to 250k 
#define BITRATE 250000
#define NTQ     20        // 20 Time Quanta in a Bit Time
#define BRP_VAL        ((FCAN/(2*NTQ*BITRATE))-1)

void ecan1ClkInit(DWORD b) {
	DWORD fCAN=FCAN;
	BYTE nTQ;
	WORD brp;

  //    Bit Time = (Sync Segment + Propagation Delay + Phase Segment 1 + Phase Segment 2)=20*TQ
  //    Phase Segment 1 = 8TQ
  //    Phase Segment 2 = 6Tq
  //    Propagation Delay = 5Tq
  //    Sync Segment = 1TQ
  //    CiCFG1<BRP> =(FCAN /(2 �N�FBAUD))� 1
   
	C1CTRL1bits.CANCKS = 0;			// errata dice che con 0 si ha 2x e viceversa! MA NON SEMBRA!

	if(b>=2000000L)
		nTQ=NTQ/4;
	else if(b>=1000000L)
		nTQ=NTQ/2;
	else if(b<100000L)
		nTQ=NTQ*2;
	else if(b<20000L)
		nTQ=NTQ*4;
	else 
		nTQ=NTQ;

  // Synchronization Jump Width set to 4 TQ 
  C1CFG1bits.SJW = (nTQ/5)-1;			//0x3;
  // Baud Rate Prescaler 
  brp = ((fCAN/(2L*nTQ*b))-1);
// che FARE se overflowa?? mah...
	if(brp>=64) {
		C1CTRL1bits.CANCKS = 1;
		fCAN /= 2;
	  brp = ((fCAN/(2L*nTQ*b))-1);
		}
  C1CFG1bits.BRP = brp;

  // Phase Segment 1 time is 8 TQ 
  C1CFG2bits.SEG1PH = ((nTQ*2)/5)-1;			//0x7;
  // Phase Segment 2 time is set to be programmable 
  C1CFG2bits.SEG2PHTS = 0x1;
  // Phase Segment 2 time is 6 TQ 
  C1CFG2bits.SEG2PH = ((nTQ*3)/10)-1;			//0x5;
  // Propagation Segment time is 5 TQ 
  C1CFG2bits.PRSEG = (nTQ/4)-1;				//0x4;
  // Bus line is sampled three times at the sample point 
  C1CFG2bits.SAM = 0x1;
 
	} 


/* ******** Message Write Services ******* */

BYTE CANIsPutReady(BYTE putHndl) {
	BYTE _uCAN_ret;


	// Check to see if buffer 0 is available
	if(!C1TR01CONbits.TXREQ0) {
//boh		ECANCON = 0x43; 
		_uCAN_ret = _uCANTxHndls[0] = putHndl;
		return 1;
		}

	// Else check to see if buffer 1 is available
	else if(!C1TR01CONbits.TXREQ1) {
//boh		ECANCON = 0x44; 
		_uCAN_ret = _uCANTxHndls[1] = putHndl;
		return 2;
		}

	// Else check to see if buffer 2 is available
	else if(!C1TR23CONbits.TXREQ2) {
//boh		ECANCON = 0x45; 
		_uCAN_ret = _uCANTxHndls[2] = putHndl;
		return 3;
		}

// qua ce ne sono anche altri...
/*	else if(!C1TR23CONbits.TXREQ3) {
		_uCAN_ret = _uCANTxHndls[3] = _uCAN_Handle;
		}*/


	// Else no more buffers available
	else {
		_uCAN_ret = 0;
		return 0;
		}
	}


signed char CANSendMessage(BYTE q) {

	switch(q){
		case 0:
			C1TR01CONbits.TXREQ0 = 1;
			return 1;
			break;
		case 1:
			C1TR01CONbits.TXREQ1 = 1;
			return 1;
			break;
		case 2:
			C1TR23CONbits.TXREQ2 = 1;
			return 1;
			break;
		}
	return 0;
	}


unsigned char CANIsPutFin(void) {
	BYTE _uCAN_ret;

// non si vede mai nulla... manco con C1INTE=1;
	// Check to see if buffer 0 has sent a message
	switch(C1VECbits.ICODE) {
		case 0b0000000:
//boh		ECANCON = 0x43;
			C1VECbits.ICODE=0b0000000; 
			_uCAN_ret = _uCANTxHndls[0];
			break;

	// Else check to see if buffer 1 has sent a message
		case 0b0000001:
	//boh		ECANCON = 0x44; 
			C1VECbits.ICODE=0b0000000; 
			_uCAN_ret = _uCANTxHndls[1];
			break;

	// Else check to see if buffer 2 has sent a message
		case 0b0000010:
	//boh		ECANCON = 0x45; 
			C1VECbits.ICODE=0b0000000;
			_uCAN_ret = _uCANTxHndls[2];
			break;

	// Else no message was sent
		default:
			_uCAN_ret = 0;
			break;
		}

	return _uCAN_ret;
	}




/* ******** Message Read Services ******* */

BYTE CANOpenMessage(unsigned char MsgTyp, unsigned long COBID) {
	unsigned char i;	
	BYTE _uCAN_ret;
	union UCAN_PARAM _uCAN_Param;

	
	_uCANOldMode = C1CTRL1bits.OPMODE;

	// eliminare ste cazzo di variabili globali!!
	_uCAN_Param.l = COBID;

	// Scan for an open filter
	for(i=0; i<CAN_MAX_RCV_ENDP; i++)	{
		if(_uCANRxHndls[i] == 0)	{
			// Save the handle to the object
			_uCAN_ret = _uCANRxHndls[i] = MsgTyp;
						
			// Store the ID in a buffer
			_uCANRxIDRes[i].dword = _uCAN_Param.l;
		
			// Set the filter change request flag
			_uCANReq = 1;
					
			return _uCAN_ret;
			}
		}
	
	_uCAN_ret = 0;
	return _uCAN_ret;
	}


unsigned char CANCloseMessage(unsigned char _uCAN_Handle) {	
	unsigned char i;
	unsigned char _uCAN_ret;
		
	_uCANOldMode = C1CTRL1bits.OPMODE;

	// Scan for an open filter
	for(i=0; i<CAN_MAX_RCV_ENDP; i++) {
		if(_uCANRxHndls[i] == _uCAN_Handle) {	
			// Remove the handle
			_uCANRxHndls[i] = 0;
									
			// Set the filter change request flag
			_uCANReq = 1;
			
			// Indicate a successful close of the message receive endpoint
			_uCAN_ret = 1;	
					
			return _uCAN_ret;
			}
		}
	
	_uCAN_ret = 0;	
	return _uCAN_ret;
	}



unsigned char CANIsGetReady(void) {
	BYTE _uCAN_ret;

//potrebbe ritornare anche C1VECbits.FILHIT

	// Check for a receive interrupt or flag
	if(IFS2bits.C1RXIF)	{				

//COMM_STATE_OPER=1;


		mLed2^=1;
		IFS2bits.C1RXIF=0;
		}
//	if(C1INTFbits.RBIF) {		// boh, serve?
//		C1INTFbits.RBIF=0;
//		}
//	if(C1INTFbits.TBIF) {		// boh, serve?
//		C1INTFbits.TBIF=0;
//		}


// DEBUG BLOCCHI 18.6.17; tolto 22.9
	if(C1RXOVF1) {
//		LATGbits.LATG9=1;
//		LATGbits.LATG7=1;
//		LATGbits.LATG8=1;
//		LATAbits.LATA0=1;

		__delay_ms(100);
		ClrWdt();
//		LATGbits.LATG9=0;
//		LATGbits.LATG7=0;
//		LATGbits.LATG8=0;
//		LATAbits.LATA0=0;

//		C1RXOVF1=0;		// per errori main loop

		}
	if(C1INTFbits.RBOVIF) {		// boh, serve?
		C1INTFbits.RBOVIF=0;
		}
	if(C1INTFbits.IVRIF) {			// invalid message...
//		LATGbits.LATG9=1;
//		LATGbits.LATG7=1;
//		LATGbits.LATG8=1;

		__delay_ms(100);
		ClrWdt();
//		LATGbits.LATG9=0;
//		LATGbits.LATG7=0;
//		LATGbits.LATG8=0;

		C1INTFbits.IVRIF=0;	// per errori main loop

		}



		
	// Check for an interrupt and set the appropriate buffer
	if(C1RXFUL1bits.RXFUL4) {		// qua c'� NMT; BASE_BUFFER_RX
//			mLed2^=1;
//			LATAbits.LATA10^=1;
		_uCAN_ret = _uCANRxHndls[0];
		C1RXFUL1bits.RXFUL4=0;
//			C1RXOVF1bits.RXOVF4=0;		pulire? gestire?
		return 1;
		}

	#if CAN_MAX_RCV_ENDP > 1
	if(C1RXFUL1bits.RXFUL5) {		// qua SYNC
//			LATAbits.LATA2^=1;
		_uCAN_ret = _uCANRxHndls[1];
		C1RXFUL1bits.RXFUL5=0;
//			C1RXOVF1bits.RXOVF5=0;		pulire? gestire?
		return 2;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 2
	if(C1RXFUL1bits.RXFUL6) {	// qua SDO
//			LATAbits.LATA3^=1;
		_uCAN_ret = _uCANRxHndls[2];
		C1RXFUL1bits.RXFUL6=0;
//			C1RXOVF1bits.RXOVF6=0;		pulire? gestire?
		return 3;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 3
	if(C1RXFUL1bits.RXFUL7) {	// qua PDO1
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[3];
		C1RXFUL1bits.RXFUL7=0;
//			C1RXOVF1bits.RXOVF7=0;		pulire? gestire?
		return 4;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 4
	if(C1RXFUL1bits.RXFUL8) {	// qua PDO2
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[4];
		C1RXFUL1bits.RXFUL8=0;
//			C1RXOVF1bits.RXOVF8=0;		pulire? gestire?
		return 5;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 5
	if(C1RXFUL1bits.RXFUL9) {	// qua PDO3
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[5];
		C1RXFUL1bits.RXFUL9=0;
//			C1RXOVF1bits.RXOVF9=0;		pulire? gestire?
		return 6;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 6
	if(C1RXFUL1bits.RXFUL10) {	// qua PDO4
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[6];
		C1RXFUL1bits.RXFUL10=0;
//			C1RXOVF1bits.RXOVF10=0;		pulire? gestire?
		return 7;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 7
	if(C1RXFUL1bits.RXFUL11) {	// qua PDO5
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[7];
		C1RXFUL1bits.RXFUL11=0;
//			C1RXOVF1bits.RXOVF11=0;		pulire? gestire?
		return 8;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 8
	if(C1RXFUL1bits.RXFUL12) {	// qua PDO6
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[8];
		C1RXFUL1bits.RXFUL12=0;
//			C1RXOVF1bits.RXOVF12=0;		pulire? gestire?
		return 9;
		}
	#endif
	#if CAN_MAX_RCV_ENDP > 9
	if(C1RXFUL1bits.RXFUL13) {	// qua ev. NMTE... che � dinamico
//			LATAbits.LATA4^=1;
		_uCAN_ret = _uCANRxHndls[9];
		C1RXFUL1bits.RXFUL13=0;
//			C1RXOVF1bits.RXOVF12=0;		pulire? gestire?
		return 10;
		}
	#endif
			
	// Remove the interrupt, it will set again if more are pending
	//PIR3bits.RXB1IF = 0;
				

//		}
//	else 
//		_uCAN_ret = 0;

	return 0;
	}



