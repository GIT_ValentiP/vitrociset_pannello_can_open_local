/*****************************************************************************
 *
 * Microchip CANopen Stack (Network Management and Communications)
 *
 *****************************************************************************
 * FileName:        CO_NMT.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * This is the network management object.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * CinziaG					23.3.2017		PIC24E 
 *****************************************************************************/



#ifndef _CO_NMT_DEFINED_H
#define _CO_NMT_DEFINED_H

void CO_COMM_NMT_RXEvent(unsigned char);

void CO_COMM_NMT_Open(void);

void CO_COMM_NMT_Close(void);

#define mNMT_Start()					COMM_STATE_STOP = 0

#define mNMT_Stop()						COMM_STATE_STOP = 1

#define mNMT_GotoPreopState()			{COMM_STATE_PREOP = 1; COMM_STATE_OPER = 0; COMM_STATE_STOP = 0;}

#define mNMT_GotoOperState()			{COMM_STATE_PREOP = 0; COMM_STATE_OPER = 1; COMM_STATE_STOP = 0;}

#define mNMT_StateIsStopped()			COMM_STATE_STOP

#define	mNMT_StateIsOperational()		(COMM_STATE_OPER && (!COMM_STATE_STOP))

#define	mNMT_StateIsPreOperational()	(COMM_STATE_PREOP && (!COMM_STATE_OPER) && (!COMM_STATE_STOP))

#endif

