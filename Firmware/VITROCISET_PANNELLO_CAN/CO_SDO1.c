/*****************************************************************************
 *
 * Microchip CANopen Stack (The Default SDO)
 *
 *****************************************************************************
 * FileName:        CO_SDO1.C
 * Dependencies:    
 * Processor:       PIC18F/PIC24E with CAN
 * Compiler:       	C18/C30 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * CinziaG					5.7.17		varie + eds ptr
 * 
 *****************************************************************************/



#include "co_main24.h"
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services

#include	"CO_ABERR.H"			// Abort types
#include	"CO_memio.H"			// 
#include	"CO_COMM.H"				// Object
#include	"CO_DICT.H"				// Dictionary Object Services
#include	"CO_TOOLS.H"			// COB ID tools




const rom unsigned char 	_uSDO1COMMIndx = 2;	// The length of the object

UNSIGNED32 			_uSDO1_CS_COBID;	// COB IDs used by the default SDO
UNSIGNED32 			_uSDO1_SC_COBID;

unsigned char 		_hSDO1;				// Handle to the connection

union _SDO_STATE	_uSDO1State;		// State bitmap for this SDO
REQ_STAT			_uSDO1ACode;		// Abort code
union _SDO_CTL 		_uSDO1Ctl;			// Received control byte buffer
static BYTE 		segmentCnt,blockCnt,blockTotal;
DICT_OBJ			_uSDO1Dict;			// Local dictionary object, loaded during inits
unsigned int	 	_uSDO1Timer;		// Watchdog, defined by the application
UNSIGNED16	 		_uSDO1BytesLeft;	// Bytes to send
extern BYTE timedSDOSend;

/* Buffers used in this design */
unsigned char		_uSDO1RxBuf[CO_SDO1_MAX_RX_BUF];	// Receive space for downloads
unsigned char 	_uSDO1TxBuf[8];		// Transmit space for uploads

BYTE storeAndDecode(DICT_OBJ *,BYTE);


// Process access events to the COB ID
void CO_COMM_SDO1_CS_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(_uSDO1_CS_COBID.word);
			myCob=Microchip2Canopen(_uSDO1_CS_COBID.word);

			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = myCob;
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		case DICT_OBJ_WRITE: 	// 
			break;
		}	
	}


// Process access events to the COB ID
void CO_COMM_SDO1_SC_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(_uSDO1_SC_COBID.word);

//USARE _CO_COB_MCHP2CANopen_SID(_uSDO1_SC_COBID.word)

			myCob=Microchip2Canopen(_uSDO1_SC_COBID.word);

			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) =myCob;				
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		case DICT_OBJ_WRITE: 	// Read the object
			break;
		}
	}


#ifdef USA_BOOTLOADER 

void CO_COMM_SDO1_BootloaderKey(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			_uSDO1Dict.pReqBuf[0]=keyForBootloader;			//
			_uSDO1Dict.pReqBuf[1]=0;			//
			_uSDO1Dict.pReqBuf[2]=0;			//
			_uSDO1Dict.pReqBuf[3]=0;			//
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		case DICT_OBJ_WRITE: 	// 
			if(_uSDO1Dict.len==4) {
				if(_uSDO1RxBuf[0]=='e' && _uSDO1RxBuf[1]=='v' && _uSDO1RxBuf[2]=='a' && _uSDO1RxBuf[3]=='s') {
					keyForBootloader++;
					if(keyForBootloader>=2)
						asm("reset");		//Reset();

					}
				else
					keyForBootloader=0;
				}
			else
				keyForBootloader=0;

			_uSDO1TxBuf[4] = keyForBootloader;
			_uSDO1TxBuf[5] = 0;
			_uSDO1TxBuf[6] = 0;
			_uSDO1TxBuf[7] = 0;
			break;
		}	
	}
#endif

void CO_COMM_SerialNumberKey(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		case DICT_OBJ_WRITE: 	// 
			_uSDO1TxBuf[4] = 0;
			if(_uSDO1Dict.len==4) {
				if(_uSDO1RxBuf[0]=='*' && _uSDO1RxBuf[1]=='t' && _uSDO1RxBuf[2]=='K' && _uSDO1RxBuf[3]=='*') {
					saveSettings(100);
					_uSDO1TxBuf[4] = 1;
					}
				}	
//o anche uDict.ret

			_uSDO1TxBuf[5] = 0;
			_uSDO1TxBuf[6] = 0;
			_uSDO1TxBuf[7] = 0;

			break;
		}	
	}

void CO_COMM_SerialNumber(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			_uSDO1Dict.pReqBuf[0]=LOBYTE(LOWORD(configParms.serialNumber));			//
			_uSDO1Dict.pReqBuf[1]=HIBYTE(LOWORD(configParms.serialNumber));			//
			_uSDO1Dict.pReqBuf[2]=LOBYTE(HIWORD(configParms.serialNumber));			//
			_uSDO1Dict.pReqBuf[3]=HIBYTE(HIWORD(configParms.serialNumber));			//
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		case DICT_OBJ_WRITE: 	// 
			if(_uSDO1Dict.len==4) {
				configParms.serialNumber=MAKELONG(MAKEWORD(_uSDO1RxBuf[0],_uSDO1RxBuf[1]),
					MAKEWORD(_uSDO1RxBuf[2],_uSDO1RxBuf[3]));
				}

//o anche uDict.ret
			_uSDO1TxBuf[4] = 1;
			_uSDO1TxBuf[5] = 0;
			_uSDO1TxBuf[6] = 0;
			_uSDO1TxBuf[7] = 0;

			break;
		}	
	}


void CO_COMM_SDO1_Open(void) {	


	// Set the client to server COB ID and convert it to MCHP
//	_uSDO1_CS_COBID.word = 0x600L | _uCO_nodeID.byte;
//USARE _CO_COB_CANopen2MCHP_SID(0x600L | _uCO_nodeID.byte)
//	mTOOLS_CO2MCHP(_uSDO1_CS_COBID.word);
	_uSDO1_CS_COBID.word = Canopen2Microchip(0x600 | configParms.nodeID);

	// Set the server to client COB ID and convert it to MCHP
//	_uSDO1_SC_COBID.word = 0x580L | _uCO_nodeID.byte;
//USARE _CO_COB_CANopen2MCHP_SID(0x580L | _uCO_nodeID.byte)
//	mTOOLS_CO2MCHP(_uSDO1_SC_COBID.word);
	_uSDO1_SC_COBID.word = Canopen2Microchip(0x580 | configParms.nodeID);

	// Open a receive message endpoint in the driver
	_hSDO1=CANOpenMessage(COMM_MSGGRP_SDO | COMM_SDO_1, _uSDO1_CS_COBID.word);
	if(_hSDO1)
		COMM_SDO_1_EN = 1;
	
	// Reset internal variables
	_uSDO1State.word=0;
	}


void CO_COMM_SDO1_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_hSDO1);
	COMM_SDO_1_EN = 0;
	}


void CO_COMM_SDO1_LSTimerEvent(void) {

	// Process only if the connection is in the middle of active segmented comm
	if(_uSDO1State.bits.start)	{		
		// Adjust the time
		_uSDO1Timer -= CO_TICK_PERIOD;
		
		// Reset SDO1 states if a timeout while receiving
		if((signed int)_uSDO1Timer <= 0)	{
			// Reset the states
			_uSDO1State.word=0;
			
			// Queue an abort, SDO timeout	
			_uSDO1ACode = E_SDO_TIME;
			COMM_SDO_1_TF = 1;
			}
		}
	}


void CO_COMM_SDO1_TXFinEvent(void) {
	
	}



void CO_COMM_SDO1_TXEvent(BYTE w) {


//	w=0;		// se no non va...
//#warning buffer TX per SDO...


	// Set the CobID
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(w)) = _uSDO1_SC_COBID.word;
	
	// Set the length
	mCANPutDataLen(w,8);

	if(_uSDO1ACode == E_SUCCESS) {
		// Move the data to the transmit buffer
		*(__eds__ _DATA8 *)(mCANGetPtrTxData(w)) = *(_DATA8 *)_uSDO1TxBuf;

		}
	else {
		// Set the abort code
//		*(mCANGetPtrTxData(0)) = 0x80;
		mCANPutDataByte0(w,0x80);

		// Set the multiplexor

		mCANPutDataByte1(w,((UNSIGNED16 *)(&(_uSDO1Dict.index)))->bytes.B0.byte);
		mCANPutDataByte2(w,((UNSIGNED16 *)(&(_uSDO1Dict.index)))->bytes.B1.byte);
		mCANPutDataByte3(w,_uSDO1Dict.subindex);

		DWORD err=0xffffffff		/*in caso andasse storto :) */;

		switch(_uSDO1ACode) {
			case E_TOGGLE:						// Toggle not alternated
//				*(unsigned long *)(mCANGetPtrTxData(0) + 4) = 0x05030000L;
				err = 0x05030000L;
				break;
	
			case E_SDO_TIME:					// SDO protocol timed out
				err = 0x05040000L;
				break;
	
			case E_CS_CMD:						// Client/server command specifier not valid or unknown
				err = 0x05040001L;
				break;
	
			case E_BLOCK:							// invalid block # (solo Block)
				err = 0x05040002L;
				break;
	
			case E_SEQUENCE:					// invalid sequence (solo Block)
				err = 0x05040003L;
				break;
	
			case E_CRC:							// CRC (solo Block)
				err = 0x05040004L;
				break;
	
			case E_MEMORY_OUT:					// Out of memory
				err = 0x05040005L;
				break;
	
			case E_UNSUPP_ACCESS:				// Unsupported access to an object
				err = 0x06010000L;
				break;
	
			case E_CANNOT_READ:					// Attempt to read a write only object
				err = 0x06010001L;
				break;
	
			case E_CANNOT_WRITE:				// Attempt to write a read only object
				err = 0x06010002L;
				break;
	
			case E_OBJ_NOT_FOUND:				// Object does not exist in the object dictionary
				err = 0x06020000L;
				break;
	
			case E_OBJ_CANNOT_MAP:				// Object cannot be mapped to the PDO
				err = 0x06040041L;
				break;
	
			case E_OBJ_MAP_LEN:					// The number and length of the objects to be mapped would exceed PDO length
				err = 0x06040042L;
				break;
	
			case E_GEN_PARAM_COMP:				// General parameter incompatibility reason
				err = 0x06040043L;
				break;
	
			case E_GEN_INTERNAL_COMP:			// General internal incompatibility in the device
				err = 0x06040047L;
				break;
	
			case E_HARDWARE:					// Access failed due to a hardware error
				err = 0x06060000L;
				break;
	
			case E_LEN_SERVICE:					// Data type does not match, length of service parameter does not match
				err = 0x06070010L;
				break;
	
			case E_LEN_SERVICE_HIGH:			// Data type does not match, length of service parameter too high
				err = 0x06070012L;
				break;
	
			case E_LEN_SERVICE_LOW:				// Data type does not match, length of service parameter too low
				err = 0x06070013L;
				break;
	
			case E_SUBINDEX_NOT_FOUND:			// Sub-index does not exist
				err = 0x06090011L;
				break;
	
			case E_PARAM_RANGE:					// Value range of parameter exceeded
				err = 0x06090030L;
				break;
	
			case E_PARAM_HIGH:					// Value of parameter written too high
				err = 0x06090031L;
				break;
	
			case E_PARAM_LOW:					// Value of parameter written too low
				err = 0x06090032L;
				break;
	
			case E_MAX_LT_MIN:					// Maximum value is less than minimum value
				err = 0x06090036L;
				break;
	
			case E_GENERAL:						// general error
				err = 0x08000000L;
				break;
	
			case E_TRANSFER:					// Data cannot be transfered or stored to the application
				err = 0x08000020L;
				break;
	
			case E_LOCAL_CONTROL:			// Data cannot be transfered or stored to the application because of local control
				err = 0x08000021L;
				break;
			
			case E_DEV_STATE:					// Data cannot be transfered or stored to the application because of the device present state
				err = 0x08000022L;
				break;
	
			case E_DICTIONARY:				// dictionary not present or ready
				err = 0x08000023L;
				break;
	
			case E_SUCCESS:					// 
				break;
			}

		__eds__ unsigned char *pds=mCANGetPtrTxData(w);
		//CO_MEMIO_CopyToEds
		pds[4]=LOBYTE(LOWORD(err));
		pds[5]=HIBYTE(LOWORD(err));
		pds[6]=LOBYTE(HIWORD(err));
		pds[7]=HIBYTE(HIWORD(err));
		}
	}



void CO_COMM_SDO1_RXEvent(unsigned char b) {			
	int i;

// ok 27.3.17 LATAbits.LATA5^=1;

	// Process only if all 8 bytes of data are received
	if(mCANGetDataLen(b) == 8) {
		// Init the abort code to success
		_uSDO1ACode = E_SUCCESS;

		// Get the first byte of the received message
		_uSDO1Ctl.byte = mCANGetDataByte0(b);


//SE SI USA BLOCK, PASSARE A 512!!!		
if(_uSDO1State.bits.start && (_uSDO1State.bits.dnld2 || _uSDO1State.bits.upld2)) {		// per Block
// SERVIRA' una specie di timer per mandare i segmenti... (v. anche sotto)
			if(_uSDO1State.bits.upld2) {		// siamo in block upload
				goto uploadBlock;
				}
			if(_uSDO1State.bits.dnld2) {		// siamo in block download 
downloadBlock: ;

				__eds__ unsigned char *pds=mCANGetPtrRxData(b);

				CO_MEMIO_CopyFromEds(pds+1,_uSDO1RxBuf+_uSDO1Dict.reqOffst,7);

				_uSDO1Dict.reqOffst+=7;
			//	_uSDO1Dict.reqLen		// controllarlo se � stato impostato!
				if(mCANGetDataByte0(b) & 0b10000000) {		// controllare sequenza
					_uSDO1Dict.reqOffst=0;		// sarebbe un'idea, per proteggersi... tanto qua non va oltre 512
					blockCnt++;		// 
// com'� il blocco vs. i byte? questo va inviato a fine blocco... o cmq a esaurimento byte - direi
					_uSDO1TxBuf[0] = 0xA2;		// continua
					_uSDO1TxBuf[1] = mCANGetDataByte0(b) & 0b01111111 /* o magari segmentCnt*/;			// last seqno received
					_uSDO1TxBuf[2] = 1; // max blocksize che accetto: ossia quante transazioni formate da n segmenti possiamo fare )in questo caso 1= 889 max ci basta)
					COMM_SDO_1_TF = 1;
					}
				if(segmentCnt != (mCANGetDataByte0(b) & 0b01111111)) {
// mandare ABORT? fuori sequenza
//						_uSDO1TxBuf[0] = 0x80;		// abort
//						_uSDO1ACode = E_SEQUENCE;
//						COMM_SDO_1_TF = 1;
					}
				segmentCnt++;		// secondo il doc. "semplice" seqno parte da 0, secondo l'ultimo parte da 1... 
				}
			}			// in block up/download
		else {
		switch(_uSDO1Ctl.byte & 0xE0)	{
			case 0xC0:		// Block Download (i.e. write), DS-301
//ftp://ztchs.p.lodz.pl/KURS_ARM/Bibioteki_specyfikac/DS301.pdf
//https://www.nikhef.nl/pub/departments/ct/po/doc/CANopen30.pdf
//https://books.google.it/books?id=CstN61COZe4C&pg=PA106&lpg=PA106&dq=canopen+Download+Block+Segment&source=bl&ots=UrQwiVPOZl&sig=pwdqPcIypRRFIeqyX1CGjUp-4Co&hl=it&sa=X&ved=0ahUKEwiM9J7S9IPWAhWLJZoKHX3DBjYQ6AEIZDAH#v=onepage&q=canopen%20Download%20Block%20Segment&f=false
				if(!(_uSDO1Ctl.byte & 0b00000001)) {
					_uSDO1State.bits.start=0;
					if(storeAndDecode(&_uSDO1Dict,b) == E_SUCCESS) {		// Was the object in the dictionary
						if(_uSDO1Dict.ctl & WR_BIT) {
							if(_uSDO1Ctl.byte & 0b00000010) {
								_uSDO1State.bits.sizeindicated=1;
								_uSDO1Dict.reqLen=MAKELONG(MAKEWORD(mCANGetDataByte4(b),mCANGetDataByte5(b)),MAKEWORD(mCANGetDataByte6(b),mCANGetDataByte7(b)));
								}
							else {
								_uSDO1State.bits.sizeindicated=1;
								_uSDO1Dict.reqLen=0;
								}
							_uSDO1State.bits.crc=_uSDO1Ctl.byte & 0b00000100 ? 1 : 0;
							_uSDO1TxBuf[0] = 0xA0 | 0b00000000;		// no crc
							_uSDO1TxBuf[4] = 1; // max blocksize che accetto: ossia quante transazioni formate da n segmenti possiamo fare )in questo caso 1= 889 max ci basta)
							COMM_SDO_1_TF = 1;
							_uSDO1State.bits.dnld2=1;
							_uSDO1State.bits.start=1;		// questo abilita il timeout...
							}
						else {								// abort, access problem	
							_uSDO1ACode = E_CANNOT_WRITE;
							COMM_SDO_1_TF = 1;
							}
						}
					else {					// Abort, return the appropriate code
						_uSDO1ACode = mCO_DictGetRet();
						COMM_SDO_1_TF = 1;
						}
					}
				else {
					if(_uSDO1State.bits.start) {
						if(blockCnt<blockTotal) {		// che fare se?
							}
						_uSDO1TxBuf[0] = 0xA1;		// fine
	//					_uSDO1TxBuf[1] = blockCnt;		// blocchi ricevuti
	//					_uSDO1TxBuf[2] = 0 /*totalebyte */;		// totale byte
						if(_uSDO1State.bits.crc) {
							// qua c'� CRC : mCANGetDataByte1(b),mCANGetDataByte2(b)
							}
						COMM_SDO_1_TF = 1;
						}
					_uSDO1State.bits.dnld2=0;
					_uSDO1State.bits.start=0;
					segmentCnt=0;
					blockCnt=0;
					}
				break;

			case 0xA0:		// Block Upload (i.e. read)
//    A4 08 10 00 21 00 00 00 // Initiate req: Read [1008], Blocksize 33, CRC supported
//    C2 08 10 00 1A 00 00 00 // Initiate resp: It's 26 bytes long, no CRC
//    A3 00 00 00 00 00 00 00 // Initiate block req: Let's go
//    01 54 69 6E 79 20 4E 6F // Upload block resp, Segment = 1
//    02 64 65 20 2D 20 4D 65 // Upload block resp, Segment = 2
//    03 67 61 20 44 6F 6D 61 // Upload block resp, Segment = 3
//    84 69 6E 73 20 21 00 00 // Last segment, Segment = 4
//    A2 04 21 00 00 00 00 00 // Upload block req: 4 segments received, Blocksize 33
//    C9 00 00 00 00 00 00 00 // End resp: 2 bytes free
//    A1 00 00 00 00 00 00 00 // End req: Thank you

				switch(_uSDO1Ctl.byte & 0b00000011) {
					case 0b00000011:		//"let's go"
						if(_uSDO1State.bits.start) {
							_uSDO1State.bits.upld2=1;
							CO_DictObjectRead(&_uSDO1Dict);				// metto payload...
	
							_uSDO1Dict.reqOffst = 0;
	// SERVIRA' una specie di timer per mandare i segmenti... (v. anche sopra)
							timedSDOSend=1;
uploadBlock:
							_uSDO1Dict.reqOffst += 7;
							_uSDO1TxBuf[0] = segmentCnt++;			// secondo il doc. "semplice" seqno parte da 0, secondo l'ultimo parte da 1... 
							if(_uSDO1Dict.reqOffst >= _uSDO1Dict.reqLen) {				// alzare b7 se � l'ultimo
								_uSDO1TxBuf[0] |= 0b10000000;
	// fermare il timer...
								timedSDOSend=0;
								}
							COMM_SDO_1_TF = 1;
							}
						break;
					case 0b00000010:		//confirmation
						if(_uSDO1State.bits.start) {
							if(blockCnt<blockTotal) {
								}
							else {		// anche if((_uSDO1Ctl.byte & 0xE0) == 0x80) ???? eh?
								if((mCANGetDataByte1(b)) != segmentCnt) {
									}
								_uSDO1TxBuf[1] = segmentCnt;		// segmenti ricevuti
								_uSDO1TxBuf[2] = 0 /*totalebyte */;		// totale byte
								}
							_uSDO1TxBuf[0] = 0xC1 | 0;		// mettere il #byte free nell'ultimo segmento
							// in 1 e 2 ci metteremmo il CRC
							COMM_SDO_1_TF = 1;
							}
						break;
					case 0b00000001:		//end, thank you
						if(_uSDO1State.bits.start) {
							_uSDO1State.bits.upld2=0;
							}
						break;
					case 0b00000000:		// init block
						_uSDO1State.bits.start=0;
						if(storeAndDecode(&_uSDO1Dict,b) == E_SUCCESS) {		// Was the object in the dictionary
							if(_uSDO1Dict.ctl & RD_BIT) {
								_uSDO1State.bits.start=1;		// questo abilita il timeout...
								_uSDO1State.bits.crc=_uSDO1Ctl.byte & 0b00000100 ? 1 : 0;
								_uSDO1TxBuf[0] = 0xC0 | 0b00000010 /* no crc; size indicated*/;
								_uSDO1TxBuf[1] = _uSDO1RxBuf[1];
								_uSDO1TxBuf[2] = _uSDO1RxBuf[2];
								_uSDO1TxBuf[3] = _uSDO1RxBuf[3];		// sicuri??
								blockTotal=_uSDO1RxBuf[4];			// questo contiene segments per block
		// _uSDO1RxBuf[5] questo contiene "switch protocol if..."
								_uSDO1TxBuf[4] = LOBYTE(LOWORD(_uSDO1Dict.reqLen));
								_uSDO1TxBuf[5] = HIBYTE(LOWORD(_uSDO1Dict.reqLen));
								_uSDO1TxBuf[6] = LOBYTE(HIWORD(_uSDO1Dict.reqLen));
								_uSDO1TxBuf[7] = HIBYTE(HIWORD(_uSDO1Dict.reqLen));
								COMM_SDO_1_TF = 1;
								}
							else {								// abort, access problem	
								_uSDO1ACode = E_CANNOT_READ;
								COMM_SDO_1_TF = 1;
								}
							}
						else {						// Abort, return the appropriate code
							_uSDO1ACode = mCO_DictGetRet();
							COMM_SDO_1_TF = 1;
							}
						break;
					}
				segmentCnt=0;
				blockCnt=0;
				break;

			case 0x00:		// Download
				// Continue only if initiated and in download state
				if(_uSDO1State.bits.start) {
					if(_uSDO1State.bits.dnld) {
						// Compare the toggle bits. 
						if((_uSDO1State.word ^ _uSDO1Ctl.byte) & 0x10)	{
							// Reset states
							_uSDO1State.word = 0;
							
							// abort, toggle not alternated
							_uSDO1ACode = E_TOGGLE;
							COMM_SDO_1_TF = 1;
							return;
							}	
										
						// Copy data from the driver 
//						*((_DATA7 *)(_uSDO1RxBuf + _uSDO1Dict.reqOffst)) = *((__eds__ _DATA7 *)(mCANGetPtrRxData(b) + 1));
// no madonna bastarda, eccezione ODD address!!
	//USARE	=mCANGetDataByte0(b);
	//	=mCANGetDataByte1(b);

						__eds__ unsigned char *pds=mCANGetPtrRxData(b);
//						for(i=0; i<7; i++)		v. sotto i for non vanno
							CO_MEMIO_CopyFromEds(pds+1,_uSDO1RxBuf+_uSDO1Dict.reqOffst,7);
											
						// Adjust the count or offset to the next segment
						_uSDO1Dict.reqOffst += (~_uSDO1Ctl.rctl.n) & 0x07; 	
						
						// If the count is greater than the object length then abort
						if(_uSDO1Dict.reqOffst > _uSDO1Dict.len) {
							// Reset states
							_uSDO1State.word=0;
							
							// Abort
							_uSDO1ACode = E_LEN_SERVICE;
							COMM_SDO_1_TF = 1;
							return;	
							}					
						
						// Set the next expected toggle
						_uSDO1State.bits.tog ^=1;
						
						// Set the header to respond, b'001t0000
						_uSDO1TxBuf[0] = (_uSDO1Ctl.byte | 0x20) & 0xF0;
						
						// Check for a complete flag
						if(_uSDO1Ctl.rctl.c)	{
							// Set the pointer to the buffer
							_uSDO1Dict.pReqBuf = _uSDO1RxBuf;
							
							// Write the object
							CO_DictObjectWrite(&_uSDO1Dict);
	
							// Reset states
							_uSDO1State.word = 0;
	
							// Send code, whatever was returned from the object
							_uSDO1ACode = mCO_DictGetRet();
							COMM_SDO_1_TF = 1;
							}
						else {
							// Queue to send acknowledge
							COMM_SDO_1_TF = 1;
							}
						}		// in download state
					}
				break;
							
			case 0x20:		// Initiate Download			(i.e. write)
				// Kill the current start if any
				_uSDO1State.bits.start = 0;
		
				if(storeAndDecode(&_uSDO1Dict,b) == E_SUCCESS) {		// Was the object in the dictionary
					// Check the access for this request; must have write capability
					if(_uSDO1Dict.ctl & WR_BIT) {
						// Expedited download
						if(_uSDO1Ctl.ictl.e)	{
							// If the count is specified, set the request length from the header
							if(_uSDO1Ctl.ictl.s) 
								_uSDO1Dict.reqLen = (((~_uSDO1Ctl.ictl.n) & 0x03) + 1);
							// Otherwise set the length from the object
							else 
								_uSDO1Dict.reqLen = _uSDO1Dict.len;
										
							if(_uSDO1Dict.reqLen == _uSDO1Dict.len)	{
								// Reset states
								_uSDO1State.word=0;
																	
								// Set the pointer to the driver
								_uSDO1Dict.pReqBuf = _uSDO1RxBuf;

								__eds__ unsigned char *pds=mCANGetPtrRxData(b);

								CO_MEMIO_CopyFromEds(pds+4,&_uSDO1RxBuf[0],4);
								
								// Call the object dictionary write function 
								CO_DictObjectWrite(&_uSDO1Dict);			
								
								// If the write was successful, queue to send acknowledge
								_uSDO1TxBuf[0] = 0x60;
	
								// Send abort, code returned from object
								_uSDO1ACode = mCO_DictGetRet();
									
								COMM_SDO_1_TF=1;
								}
							else {
								// abort, data length does not match
								_uSDO1ACode = E_LEN_SERVICE;
								COMM_SDO_1_TF = 1;
								}
							}
						
						// Segmented download
						else {							
							// If the count is specified
							if(_uSDO1Ctl.ictl.s) {
								// Test the two upper most bytes, should be zero
								if(mCANGetDataByte6(b) | mCANGetDataByte7(b)) {
									// Send abort, length does not match, 0607 0010
									_uSDO1ACode = E_LEN_SERVICE;
									COMM_SDO_1_TF=1;	
									return;
									}

								WORD n;
								n=MAKEWORD(mCANGetDataByte4(b),mCANGetDataByte5(b));
								// Compare the length against the object's defined length
//								if(*((__eds__ unsigned int *)(mCANGetPtrRxData(b) + 4)) != _uSDO1Dict.len)	{
//USARE	=mCANGetDataByte0(b);
								if(n != _uSDO1Dict.len)	{
									// abort, data length does not match
									_uSDO1ACode = E_LEN_SERVICE;
									COMM_SDO_1_TF=1;
									return;
									}
								}
							
							// Set the requested length
							_uSDO1Dict.reqLen = _uSDO1Dict.len;
														
							// Indicate a start download
							_uSDO1State.bits.start = 1;
							_uSDO1State.bits.dnld = 1;
							
							// First toggle should be 0
							_uSDO1State.bits.tog = 0;
							
							// Reset the data count 
							_uSDO1Dict.reqOffst = 0;	
							
							// Start the watchdog
							_uSDO1Timer = CO_SDO1_MAX_SEG_TIME;
							//_uSDO1State.bits.ntime = 0;
							
							// Queue to send acknowledge
							_uSDO1TxBuf[0] = 0x60;
							COMM_SDO_1_TF=1;
							}
						}
					else {
						// abort, access problem	
						_uSDO1ACode = E_CANNOT_WRITE;
						COMM_SDO_1_TF=1;
						}
					}
				
				// Object not found
				else {
					// Abort, return the appropriate code
					_uSDO1ACode = mCO_DictGetRet();
					COMM_SDO_1_TF = 1;
					}
				break;
			
			
			case 0x40:		// Initiate Upload (i.e. read)

//http://www.canopensolutions.com/english/about_canopen/device_configuration_canopen.shtml

//http://www.byteme.org.uk/canopenparent/canopen/sdo-service-data-objects-canopen/
//upload & download

				// Kill the current start
				_uSDO1State.bits.start = 0;
		
				if(storeAndDecode(&_uSDO1Dict,b) == E_SUCCESS) {		// Was the object in the dictionary
					// Check the access for this request; must have read capability
					if(_uSDO1Dict.ctl & RD_BIT) {
						// Reset offset
						_uSDO1Dict.reqOffst = 0;
						
						// Reset states
						_uSDO1State.word = 0;
						
						// If the object len is greater than 4 then segment upload
						if(_uSDO1Dict.len > 4)	{															
							// Set the watchdog
							_uSDO1Timer = CO_SDO1_MAX_SEG_TIME;
							//_uSDO1State.bits.ntime = 0;
							
							// Indicate a start upload
							_uSDO1State.bits.start = 1;
							
							// Set pointer to internal buffer
							_uSDO1Dict.pReqBuf = &_uSDO1TxBuf[4];
														
							// Set the size of the object
							_uSDO1TxBuf[4] = ((UNSIGNED16 *)(&(_uSDO1Dict.len)))->bytes.B0.byte;
							_uSDO1TxBuf[5] = ((UNSIGNED16 *)(&(_uSDO1Dict.len)))->bytes.B1.byte;
							
							// Set the response command, size indicated
							_uSDO1TxBuf[0] = 0x41;
							
							// Queue to send acknowledge
							COMM_SDO_1_TF = 1;
							}
						
						// Expedited upload
						else {	
							// Set the length in the header
							switch((unsigned char)_uSDO1Dict.len) {
								case 1:	_uSDO1TxBuf[0] = 0x4F; break;
								case 2: _uSDO1TxBuf[0] = 0x4B; break;
								case 3: _uSDO1TxBuf[0] = 0x47; break;
								case 4: _uSDO1TxBuf[0] = 0x43; break;				
								}
									
							// Set the read length						
							_uSDO1Dict.reqLen = _uSDO1Dict.len;									
																															
							// Set the pointer to the transmit buffer
							_uSDO1Dict.pReqBuf = &_uSDO1TxBuf[4];
							
							// Read the data from the object into the buffer
							CO_DictObjectRead(&_uSDO1Dict);			// i byte non usati rimangono "sporchi" da ev. msg precedenti...

							// Pass any codes
							_uSDO1ACode = mCO_DictGetRet();
							COMM_SDO_1_TF = 1;
							}
						}
					else {
						// abort, access problem		
						_uSDO1ACode = E_CANNOT_READ;
						COMM_SDO_1_TF = 1;
						}
					}
				
				// Object not found
				else {
					// Abort, return the appropriate code
					_uSDO1ACode = mCO_DictGetRet();
					COMM_SDO_1_TF = 1;
					}
				break;
						
			
			case 0x60:		// Upload
//				memcpy(_uSDO1TxBuf,dataBufferOut,8);			// 

				if(_uSDO1State.bits.start) {		// Continue only if initiated
					if(!_uSDO1State.bits.dnld) {		// and in a upload state
					
						// Compare the toggle bits. 
						if((_uSDO1State.word ^ _uSDO1Ctl.byte) & 0x10) {
							// Reset states
							_uSDO1State.word = 0;
	
							// abort, toggle not alternated
							_uSDO1ACode = E_TOGGLE;
							COMM_SDO_1_TF = 1;
							return;
							}	
																																
						// Set the pointer to the transmit buffer
						_uSDO1Dict.pReqBuf = &_uSDO1TxBuf[1];
						
						// Set the offset
						_uSDO1BytesLeft.word = _uSDO1Dict.len - _uSDO1Dict.reqOffst;
								
						// If the data to be sent is less than 8 then set n and c
						if(_uSDO1BytesLeft.word > 7) {
							_uSDO1TxBuf[0] = 0x00; 
							_uSDO1Dict.reqLen = 7;
							}
						else {
							// Reset states
							_uSDO1State.word = 0;
	
							switch(_uSDO1BytesLeft.bytes.B0.byte)	{
								case 1:		_uSDO1TxBuf[0] = 0x0D; _uSDO1Dict.reqLen = 1; break;		// v. tabella PDF, ma questo include LSB
								case 2: 	_uSDO1TxBuf[0] = 0x0B; _uSDO1Dict.reqLen = 2; break;
								case 3: 	_uSDO1TxBuf[0] = 0x09; _uSDO1Dict.reqLen = 3; break;
								case 4: 	_uSDO1TxBuf[0] = 0x07; _uSDO1Dict.reqLen = 4; break;	
								case 5: 	_uSDO1TxBuf[0] = 0x05; _uSDO1Dict.reqLen = 5; break;
								case 6: 	_uSDO1TxBuf[0] = 0x03; _uSDO1Dict.reqLen = 6; break;
								case 7: 	_uSDO1TxBuf[0] = 0x01; _uSDO1Dict.reqLen = 7; break;
								}
							}
						
						// Setup the toggle
						((union _SDO_CTL *)_uSDO1TxBuf)->rctl.t = _uSDO1Ctl.rctl.t;
							
						// Read the data from the object into the transmit buffer
						CO_DictObjectRead(&_uSDO1Dict);
						
						// Adjust the offset
						_uSDO1Dict.reqOffst += _uSDO1Dict.reqLen;
						
						// Set the next expected toggle
						_uSDO1State.bits.tog ^= 1;
				
						// Queue to send the data
						_uSDO1ACode = mCO_DictGetRet();
						COMM_SDO_1_TF = 1;
						}		// in upload state
					}
				break;
											
			case 0x80:		// Abort Request
				// Reset SDO states
				_uSDO1State.word = 0;
				break;
												
			default:
				// Send abort, not a valid command	
				_uSDO1ACode = E_CS_CMD;	
				COMM_SDO_1_TF = 1;
				break;
			}
		}
		}		// upld2/dnld2
	}

BYTE storeAndDecode(DICT_OBJ *d,BYTE b) {

	uDict.obj=d;

	// Store the multiplexor locally
	_uSDO1TxBuf[1] = mCANGetDataByte1(b);
	_uSDO1TxBuf[2] = mCANGetDataByte2(b);
	_uSDO1Dict.index=MAKEWORD(_uSDO1TxBuf[1],_uSDO1TxBuf[2]);
	_uSDO1TxBuf[3] = _uSDO1Dict.subindex = mCANGetDataByte3(b);
	
	// Preload potentially reserved locations
	_uSDO1TxBuf[4] = _uSDO1TxBuf[5] = _uSDO1TxBuf[6] = _uSDO1TxBuf[7] = 0;
	
	// Decode the object; this gets the data from the dictionary
	// If the object is functionally defined, then the data is retrieved 
	// from the object's defined function.
	CO_DictObjectDecode(&_uSDO1Dict);

	// Check the return status from the decode
	return uDict.ret /*mCO_DictGetRet()*/;
	}

