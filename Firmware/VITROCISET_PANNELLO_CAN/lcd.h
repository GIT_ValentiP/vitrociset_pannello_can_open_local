#ifndef _LCD_H_INCLUDED
#define _LCD_H_INCLUDED


// Flag bits (FLAGS)
#define NOSCANK	0
#define NOKCLICK	1
#define BIG_CLOCK_ON		2
#define CLOCK_ON		3
#define IN_ESCAPE_CHAR  4
#define BUZZON		5
#define TRIGM2	6
#define TRIGK		7
#define TRIGM		7


extern BYTE I2CBuffer[128];					// per letture di paintBitmap...
extern BYTE FLAGS;        //GENERAL PURPOSE FLAG
extern BYTE LCDX,LCDY;
extern volatile BYTE kbKeys[4];




void LCDClearLine(BYTE,BYTE);
#define LCDClearCurrentLine(q) LCDClearLine(q,LCDY)
void LCDWrite(BYTE,const char *);
void LCDWriteN(BYTE,const char *,BYTE);
void drawLinea(BYTE,BYTE,BYTE,BYTE);
void drawRect(BYTE,BYTE,BYTE,BYTE,BYTE);
#define LCDXY(q,y) {	LCDY=y;	LCDXY_(q); } //anche in picbell18.c...

void LCDHome(BYTE q);
void LCDCls(BYTE q);
BYTE LCDInit(BYTE q);
BYTE KBInit(void);


#define LCDRSBit	6
//#define LCDRWBit	7
#define LCDEnBit	2
//#define LCDEn2Bit	5				// per display 4x40
#define m_LCDEn1Bit	LATDbits.LATD2
#define m_LCDEn2Bit	LATDbits.LATD3
#define m_LCDEn3Bit	LATDbits.LATD4
#define m_LCDRSBit	LATDbits.LATD5
//#define m_LCDRWBit	LATGbits.LATG7 qua no

#define LCDCursor(q,n)   LCDOutCmd(q,n | 0x0c)          // cursor & display set
	//         entra W=2 on, W=0 off; bit 0=blink
#else
void LCDCursor(BYTE,BYTE);
#endif

void aggLuceContrasto(void);
void LCDPutChar(BYTE,BYTE);
#define LCDPutBlank(q) LCDPutChar(q,' ')

BYTE LCDOutPortNoW(BYTE,BYTE,BYTE);
BYTE LCDOutPortBit(BYTE);

BYTE LCDOutPort(BYTE,BYTE,BYTE);
#define LCDOutCmd(q,n) LCDOutPort(q,0,n)						// manda comando al display
#define LCDOutData(q,n) LCDOutPort(q,1,n)						// manda dato al display

#ifdef LCD_T6963
void LCDCheckSta2_3(BYTE);
#endif

void LCDXY_(BYTE);
//void LCDXY(BYTE y);			// W=Y, impostare LCDX

#define LCDPrintLF(q) LCDPutChar(q,10)
void LCDScroll(BYTE);
void LCDScrollDown(BYTE);
#define LCDScrollUp(q) LCDScroll(q)

void LCDOutBlankRaw(BYTE);
void LCDPrintHex(BYTE,BYTE);
void LCDPrintDec2(BYTE,BYTE);

#ifndef LCD_TEXT
void setGraphCursorAsText(void);
void PaintBitmap(BYTE );
#endif


void kbEmptyBuf(void);
BYTE checkKey(void);
#define kbKeyPressed()  ( kbKeys[0] )

void SetBeep(void);

