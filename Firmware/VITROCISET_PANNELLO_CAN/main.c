/*****************************************************************************
 *
 * Microchip CANopen Stack (Main Entry)
 *
 *****************************************************************************
 * FileName:        main.C
 * Dependencies:    
 * Processor:       PIC24 with CAN / PIC18F with CAN
 * Compiler:       	C30 3.31, or C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * This is the main entry into the demonstration. In this file some startup
 * and running conditions are demonstrated.
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Dario Greggio		30.10.17	PIC24 version; Sure. Fine. Porcodio.
 *
 // https://github.com/canboat/canboat
 // http://www.repubblica.it/economia/diritti-e-consumi/trasporti/2017/12/09/news/vitrociset_un_milione_di_euro_per_il_sistema_che_previene_i_mini_armageddon_-183133911/?ref=RHPPBT-VE-I0-C6-P10-S3.2-T1
 * 
 *****************************************************************************/



#include	"CO_MAIN24.H"
#include	<libpic30.H>

#include	"CO_dev.H"
#include	<Timer.H>
#include	"myTimer.H"
#include	"DemoObj.h"
#include	"swi2c.h"

#include	<stdio.h>
#include	<string.h>			// per memcpy...
#include	"lcd.h"

#ifdef __XC16__

// PIC24EP512GP202 Configuration Bit Settings
// 'C' source line config statements

// FICD
#pragma config ICS = PGD1               // ICD Communication Channel Select bits (Communicate on PGEC1 and PGED1)
#pragma config JTAGEN = OFF             // JTAG Enable bit (JTAG is disabled)

// FPOR
#pragma config ALTI2C1 = ON            // Alternate I2C1 pins (I2C1 mapped to SDA1/SCL1 pins)
#pragma config ALTI2C2 = ON            // Alternate I2C2 pins (I2C2 mapped to SDA2/SCL2 pins)
#pragma config WDTWIN = WIN25           // Watchdog Window Select bits (WDT Window is 25% of WDT period)

// FWDT
#pragma config WDTPOST = PS4096         // Watchdog Timer Postscaler bits (1:4,096)
#pragma config WDTPRE = PR32            // Watchdog Timer Prescaler bit (1:32)
#pragma config PLLKEN = ON              // PLL Lock Enable bit (Clock switch to PLL source will wait until the PLL lock signal is valid.)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable bit (Watchdog Timer in Non-Window mode)
#pragma config FWDTEN = ON              // Watchdog Timer Enable bit (Watchdog timer always enabled)

// FOSC
#pragma config POSCMD = NONE            // Primary Oscillator Mode Select bits (Primary Oscillator disabled)
#pragma config OSCIOFNC = ON            // OSC2 Pin Function bit (OSC2 is general purpose digital I/O pin)
#pragma config IOL1WAY = ON             // Peripheral pin select configuration (Allow only one reconfiguration)
#pragma config FCKSM = CSDCMD           // Clock Switching Mode bits (Both Clock switching and Fail-safe Clock Monitor are disabled)

// FOSCSEL
#pragma config FNOSC = HSPLL           // Oscillator Source Selection (Fast RC Oscillator with divide-by-N with PLL module (FRCPLL))
#pragma config IESO = ON                // Two-speed Oscillator Start-up Enable bit (Start up device with FRC, then switch to user-selected oscillator source)

// FGS
#pragma config GWRP = OFF               // General Segment Write-Protect bit (General Segment may be written)
#pragma config GCP = OFF                // General Segment Code-Protect bit (General Segment Code protect is Disabled)
#endif

#if defined(__PIC24EP512GU810__)		// 
_FGS( GWRP_OFF & GSS_OFF & GSSK_OFF ) 
_FOSCSEL(FNOSC_PRIPLL & IESO_OFF)			//FNOSC_FRCPLL 
_FOSC( POSCMD_HS & OSCIOFNC_OFF & IOL1WAY_OFF & FCKSM_CSDCMD )
_FWDT( WDTPOST_PS4096 & WDTPRE_PR32 & PLLKEN_ON & WINDIS_OFF & FWDTEN_ON )		//4ms
_FPOR( FPWRT_PWR8 & BOREN_ON & ALTI2C1_ON & ALTI2C2_ON )
_FICD( ICS_PGD1 & RSTPRI_PF & JTAGEN_OFF )		// DIVERSO PGDx dai PIC24 vecchi! non va cmq...
_FAS( AWRP_OFF & APL_OFF & APLK_OFF)
//_FUID0(x) simpa..
#endif


#ifdef USA_BOOTLOADER 
//#define BL_ENTRY_BUTTON PORTEbits.RE0 //button 1...

BYTE __attribute__((address(0x3000))) keyForBootloader=0;			// METTERE A INDIRIZZO FISSO!

//If defined, the reset vector pointer and boot mode entry delay
// value will be stored in the device's vector space at addresses 0x100 and 0x102
#define USE_VECTOR_SPACE
//****************************************

//Bootloader Vectors *********************
#ifdef USE_VECTOR_SPACE
	/*
		Store delay timeout value & user reset vector at 0x100 
		(can't be used with bootloader's vector protect mode).
		
		Value of userReset should match reset vector as defined in linker script.
		BLreset space must be defined in linker script.
	*/
	unsigned int userReset  __attribute__ ((space(prog),section(".BLreset"))) = 0x4000; 
	unsigned char timeout  __attribute__ ((space(prog),section(".BLreset"))) = 0x0A;
//cambiato ordine o le metteva a cazzo... 
#else
	/*
		Store delay timeout value & user reset vector at start of user space
	
		Value of userReset should be the start of actual program code since 
		these variables will be stored in the same area.
	*/
	unsigned int userReset  __attribute__ ((space(prog),section(".init"))) = 0x4004;
	unsigned char timeout  __attribute__ ((space(prog),section(".init"))) = 5;
#endif
#endif



static const char CopyrString[]= {'K','-','t','r','o','n','i','c',' ','-',' ','V','i','t','r','o','C','i','s','e','t',' ','C','A','N','O','p','e','n',' ','O','e','t','z','t','a','l',' ','a','u','t','o','m','a','t','i','o','n',
#ifdef USA_BOOTLOADER 
	' ','B','L',
#endif
	VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ','2','4','/','0','1','/','1','8', 0 };
// il mio sogno � pisciare sulle tombe di tutti i figli degli umani

const BYTE table_7seg[]={ ~0, 	//ABCDEFGP
													~0b11111100,~0b01100000,~0b11011010,~0b11110010,~0b01100110,
										 			~0b10110110,~0b10111110,~0b11100000,~0b11111110,~0b11110110,
													~0b00000010,

//finire!
													~0b11101110,~0b00111110,~0b10011100,~0b01111010,~0b10011110,~0b10001110,	//A..F
													~0b10111100,~0b01101110,~0b00001100,~0b01110000,~0b01101110,~0b00001100,	//G..L
													~0b11101100,~0b00101010,~0b00111010,~0b11001110,~0b11100110,~0b00001010,	//M..R
													~0b10110110,~0b00011110,~0b01111100,~0b00111000,~0b00111000,~0b01101100,	//S..X
													~0b01000110,~0b11011010		//Y..Z
	};


struct SAVED_PARAMETERS configParms;
BYTE timedSDOSend,doCheckStuckKey=TRUE;
DWORD test32bit;




/*********************************************************************
 * Function:        void main(void)
 *
 * PreCondition:    none
 *
 * Input:       	none
 *                  
 * Output:         	none  
 *
 * Side Effects:    none
 *
 * Overview:        Main entry into the application.
 *
 * Note:          	The following is initialization and running the CANopen stack. 
 *
 ********************************************************************/
int main(void) {	
	int divider=0;
	int i;
	// Perform any application specific initialization

	__delay_ms(100);			// OCCHIO � prima del PLL!
	DemoInitHW();					// Initialize my demo
	ClrWdt();
	__delay_ms(100);
	TimerInit();				// Init my timer
	ClrWdt();



//	InitUART();
#ifndef __DEBUG
rifo:
	loadSettings(1);
	if(!mPuls4 /*PORTCbits.RC13*/) {
		resetSettings(1);
		goto rifo;
		}
#else
		resetSettings(1);
#endif

	setTerminators(configParms.terminatorsMode);

	if(mPuls2 || mPuls3) {			// se NON modalit� test (meglio)
	//	mTOOLS_CO2MCHP(0x80);		// Set the SYNC COB ID (CAN format, => MCHP format)
		mSYNC_SetCOBID(Canopen2Microchip(0x80));		// Set the SYNC COB ID (MCHP format)
		mTIME_SetCOBID(Canopen2Microchip(0x100));		// Set the TIME COB ID (MCHP format)
		mCO_SetBaud(configParms.baudRate);			// Set the baudrate, 250K
		
		mNMTE_SetHeartBeat(configParms.heartbeat /* 1000 = 1 secondo 8.7.17 con CO_TICK_PERIOD=1 */ );	// Set the initial heartbeat QUAL � il default comune??
		mNMTE_SetGuardTime(0000);	// Set the initial guard time
		mNMTE_SetLifeFactor(0x00);	// Set the initial life time
	
		mCO_InitAll();				// Initialize CANopen to run, bootup will be sent
		}


	LCDInit(0);
	LCDInit(1);
	LCDInit(2);

	LCDCls(0);
	if(!mPuls2 && !mPuls3) 			// modalit� test
		LCDWrite(0,"Booting 1...");
	else {
//		LCDWrite(0,"Starting CAN...");
		}
	LCDCls(1);
//	LCDWrite(1,"Wait 2 seconds.");
	LCDCls(2);
//	LCDWrite(2,"Booting 3...");
//	LCDWrite(2,CopyrString);


	mResetI2C2=1;
	InitI2C();
// impostare MCP9800 a 12bit se si vuole...


	if(!mPuls2 && !mPuls3) {			// modalit� test
		int delay=50;		// usato x 4
		int n=1,n1=0;
		long n2;
		char buf[32];

		__delay_ms(1000);
		ClrWdt();
		__delay_ms(1000);
		ClrWdt();

		LCDWrite(0,"\r\nEntering Test...");
		LCDWrite(1,"\r\nEntering di...");
		LCDWrite(2,"\r\nEntering cazz...\nNO CAN!");

		while(!mPuls2 && !mPuls3) {			// modalit� test
	
			for(i=0; i<28-n1; i++)
				statoDisplay[i]=0;
			for(i=27-n1; i<28; i++) {
				statoDisplay[i]++;
				if(statoDisplay[i]>10)
					statoDisplay[i]=10;
				}


//			statoDisplay[0]=1;
//			statoDisplay[1]=2;
//			statoDisplay[2]=3;
//			statoDisplay[3]=4;

	
			I2CSTART();
			I2CTXByte((0x20 << 1) | 0);
			I2CTXByte(0x14);		// LATA
//			I2CTXByte(LOBYTE(~n));
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[0]+1]);
				}	
	//		I2CSTOP2();
	//		I2CSTART2();		// siccome SEQOP � attivo di default, potremmo scrivere di seguito!
	//		I2CTXByte2((0x20 << 1) | 0);
	//		I2CTXByte2(0x15);		// LATB
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[1]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x21 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[2]+1]);
				}	
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[3]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x22 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[4]+1]);
				}	
			if(!mPuls1) {
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[5]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x23 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[6]+1]);
				I2CTXByte(table_7seg[statoDisplay[7]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x24 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[8]+1]);
				I2CTXByte(table_7seg[statoDisplay[9]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x25 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[10]+1]);
				I2CTXByte(table_7seg[statoDisplay[11]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x26 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[12]+1]);
				I2CTXByte(table_7seg[statoDisplay[13]+1]);
				}	
			I2CSTOP();
			I2CSTART();
			I2CTXByte((0x27 << 1) | 0);
			I2CTXByte(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte(0);
				I2CTXByte(0);
				}	
			else {
				I2CTXByte(table_7seg[statoDisplay[14]+1]);
				I2CTXByte(table_7seg[statoDisplay[15]+1]);
				}	
			I2CSTOP();

			I2CSTART2();
			I2CTXByte2((0x20 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[16]+1]);
				I2CTXByte2(table_7seg[statoDisplay[17]+1]);
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x21 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[18]+1]);
				I2CTXByte2(table_7seg[statoDisplay[19]+1]);
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x22 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[20]+1]);
				I2CTXByte2(table_7seg[statoDisplay[21]+1]);
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x23 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[22]+1]);
				I2CTXByte2(table_7seg[statoDisplay[23]+1]);
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x24 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[24]+1]);
				I2CTXByte2(table_7seg[statoDisplay[25]+1]);
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x25 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(table_7seg[statoDisplay[26]+1]);
				I2CTXByte2(table_7seg[statoDisplay[27]+1]);
				}	
			I2CSTOP2();

			I2CSTART2();			// qua i led
			I2CTXByte2((0x26 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(LOBYTE(~n));
				I2CTXByte2(HIBYTE(~n));
				}	
			I2CSTOP2();
			I2CSTART2();
			I2CTXByte2((0x27 << 1) | 0);
			I2CTXByte2(0x14);		// LATA
			if(!mPuls1) {
				I2CTXByte2(0);
				I2CTXByte2(0);
				}	
			else {
				I2CTXByte2(LOBYTE(~n));
				I2CTXByte2(HIBYTE(~n));
				}	
			I2CSTOP2();
	
			ClrWdt();
			__delay_ms(50)
			__delay_ms(50)
			ClrWdt();
			__delay_ms(50)
			__delay_ms(50)
	
			mLed1 ^= 1;
	
			n <<= 1;
			if(!n)
				n=1;

			n1++;
			if(n1>=28)
				n1=0;

			i=leggi_tempMCP(0);

			if(!mPuls1) {
				LCDCls(0);
				for(i=0; i<7; i++) {
					LCDWrite(0,"1111111111");
					}
				LCDCls(1);
				for(i=0; i<7; i++) {
					LCDWrite(1,"8888888888");
					}
				LCDCls(2);
//				for(i=0; i<7; i++) {
//					LCDWrite(2,"0123456789");
//					}
//				LCDWrite(2,"012345678");
				LCDX=0;LCDY=0;LCDXY_(2);
				LCDWrite(2,"A0123456789012345678");
				LCDX=0;LCDY=1;LCDXY_(2);
				LCDWrite(2,"B0123456789012345678");
				LCDX=0;LCDY=2;LCDXY_(2);
				LCDWrite(2,"C0123456789012345678");
				LCDX=0;LCDY=3;LCDXY_(2);
				LCDWrite(2,"D0123456789012345678");
				}
			else {
				LCDCls(0);
				sprintf(buf,"%5u, ",n);
				LCDWrite(0,buf);
				n2=GestTasti();
				sprintf(buf,"%08lx",n2);
				LCDWrite(0,buf);
	
				LCDCls(2);
				sprintf(buf,"T: %u.%01u",i/10,i%10);
				LCDWrite(2,buf);
				}

			}
		}



	// stato iniziale
	for(i=0; i<NUM_BYTE_LED_I2C/2; i++) {
		statoLedI2C[i]=configParms.outputFeat[i+1].bootState;
		}
	GestLedI2C(NUM_BYTE_LED_I2C,statoLedI2C);


	mLed2=1;



	while(1)	{

		ClrWdt();

		divider++;
		if(divider> (COMM_STATE_OPER ? 15000 : 30000)) {
			mLed0 ^= 1;
			divider=0;
			}

		// Process CANopen events
		CO_ProcessAllEvents();		
		

		// 1ms timer events
		if(TimerIsOverflowEvent()) {
			static WORD emcyDivider=0;

			// Process application specific functions
			DemoProcessEvents();				// mah, credo sia meglio ogni 1mS pure questo!

			// Process timer related events
			mLed1 ^= 1;
			mCO_ProcessAllTimeEvents();	

			if(timedSDOSend) {
				CO_COMM_SDO1_RXEvent(0);			// provare...
				}

			emcyDivider++;
			if(emcyDivider>1000) {		// 1 sec; ma forse potremmo salvare gli errori pi� frequente e mandare i msg "ogni tanto"
				emcyDivider=0;

				if(C1INTF & 0b1111111110100000) {
					uCO_DevErrReg=0x11 /*comm.err+generic*/;			// errori (mappare su spec CAN??)
	
					PushErrorsFIFO(C1INTF,0);		//
					C1INTF=0;
	
					uCO_DevManufacturerStatReg=C1EC;			// stat errori
					}
	
				if(C1RXOVF1) {
					uCO_DevErrReg=0x10 /*comm.err*/;			// errori (mappare su spec CAN??)
	
	//				COMM_NETCTL_EMCY_TF=1;			// PROVA emergency! 20.9.17
					PushErrorsFIFO(0x8110,0);		//
					C1RXOVF1=0;
	
	//				uCO_DevManufacturerStatReg=C1EC;			// stat errori?
					}
	
				if(C1TR01CON & 0b0011000000110000 || C1TR23CON & 0b0011000000110000) {
					uCO_DevErrReg=0x10 /*comm.err*/;			// errori (mappare su spec CAN??)
					COMM_NETCTL_EMCY_TF=1;			// PROVA emergency! 19.7.17
					PushErrorsFIFO(0x8120,0);		//
	//				uCO_DevManufacturerStatReg=;			// stat errori?
					}
	
				if(RCONbits.WDTO) {
					uCO_DevErrReg=0x01 /*generic*/;			// errori (mappare su spec CAN??)
					COMM_NETCTL_EMCY_TF=1;			// PROVA emergency! 20.7.17
					PushErrorsFIFO(0x6101,0);		// sw error
	//				uCO_DevManufacturerStatReg=;			// stat errori?
					}
	
				if(ErroreI2C) {
					uCO_DevErrReg=0x21 /*dev specific+generic*/;			// errori (mappare su spec CAN??)
	
					COMM_NETCTL_EMCY_TF=1;			// PROVA emergency! 19.7.17
	
					PushErrorsFIFO(0x5002,ErroreI2C);		// errore HW, i2c :) differenziare analog, tasti (v. errore I2C
					ErroreI2C=0;
	//				uCO_DevManufacturerStatReg=;			// stat errori?
					}

				if(doCheckStuckKey) {
					if(!checkStuckKey()) {
						doCheckStuckKey=0;
						}
					else {
						uCO_DevErrReg=0x21 /*dev specific+generic*/;			// errori (mappare su spec CAN??)
		
						COMM_NETCTL_EMCY_TF=1;			// matteo 22.9.17
		
						PushErrorsFIFO(0x5003,0);		// errore HW, tast stuck
		//				uCO_DevManufacturerStatReg=;			// stat errori?
						}
					}

				}


			}

//		__delay_ms(1);


//		if(!PORTFbits.RF4) {
//			_CANReset();			//***** DEBUG 23.6.17
//			}


		}

	}




#if 0
signed char InitUART(BYTE doClearBuffer) {
	unsigned int UBRG;
	DWORD parm1;
	int n;

// UART
	UART1TX_TRIS = 0;		// sembra cmq inutile qua
	UART1RX_TRIS = 1;
//		UMODE = 0x8000;			// Set UARTEN.  Note: this must be done before setting UTXEN

//			USTA = 0x0400;		// UTXEN set

	UBRG = getBRGFromBaudRate(baudRateValues[configTerminale.baudRate]);

// finire parita ecc
	parm1=UART_EN & UART_IDLE_CON & UART_DIS_WAKE & UART_DIS_LOOPBACK & UART_DIS_ABAUD   
		& UART_IrDA_DISABLE & UART_MODE_SIMPLEX & UART_UEN_00 & UART_UXRX_IDLE_ONE & UART_BRGH_FOUR;
	if(configTerminale.dataStop==1)
		parm1 &= UART_1STOPBIT;
	else
		; // non c'� parm1 &= UART_2STOPBIT;

	n=attribGrafici.ACM ? 8 : configTerminale.dataBits;
	switch(n) {		// v. interblocco tra databit e stop (v. pag 22; anche in setup)
		case 7:
			break;
		case 8:
			break;
		}
	switch(configTerminale.dataParity) {
		case 0:
			parm1 &= UART_EVEN_PAR_8BIT;
			break;
		case 1:
			parm1 &= UART_ODD_PAR_8BIT;
			break;
		case 2:
			break;
		case 3:
			break;
		case 4:
			parm1 &= UART_NO_PAR_8BIT;
			break;
		case 5:		// ignore
			parm1 &= UART_NO_PAR_8BIT;
			break;
		}
	if(configTerminale.useHandshake) {
		}
	if(configTerminale.useXOnXOff) {
		}
	OpenUART1(parm1,UART_INT_TX_BUF_EMPTY & UART_SYNC_BREAK_DISABLED & UART_TX_ENABLE & UART_INT_RX_CHAR & UART_ADR_DETECT_DIS 
		& UART_IrDA_POL_INV_ZERO /*questo fa TX IDLE= HIGH!!*/,UBRG);
//ConfigIntUART1(UART_RX_INT_EN & UART_TX_INT_DIS & UART_RX_INT_PR1 & UART_TX_INT_PR7 ) ;


	SetPriorityIntU1RX(5);
	EnableIntU1RX;
	DisableIntU1TX;


	if(configTerminale.test)			// tanto per... se non d� fastidio...
		my_putsUSART("..self test...");


	if(doClearBuffer) {
		memset((void *)Buf232,0,BUF_232_SIZE);
	//	memset((void *)Buf232_2,0,BUF_232_SIZE);
		clearBuffer232();
		}



	return 1;
  }

void my_putsUSART(const char *data) {			// xch� l'altra fa zero-term...
  
	while(*data) {  // Transmit a byte
    while(BusyUART1())
			ClrWdt();
    putcUART1(*data++);
	  }
	}

#endif



signed char GestLedI2C(unsigned char q, BYTE *r1) {
	unsigned char myAddress=0x26,i2cprog=0;			// 2
	unsigned char retVal=1;

	while(q) {
		I2CSTART2();
		if(!I2CTXByte2((myAddress << 1) | 0)) {
			retVal=0;
			break;
			}
		I2CTXByte2(0x14);		// LATA
		statoLedI2C[i2cprog++]=*r1;
		I2CTXByte2(~*r1++);			// logica inversa
		q--;
//		I2CSTOP2();
//		I2CSTART2();		// siccome SEQOP � attivo di default, potremmo scrivere di seguito!
//		I2CTXByte2((0x20 << 1) | 0);
//		I2CTXByte2(0x15);		// LATB
		statoLedI2C[i2cprog++]=*r1;
		I2CTXByte2(~*r1++);
		q--;
		I2CSTOP2();
		myAddress++;
		}

	return retVal;
	}

signed char GestDisplay(unsigned char q, BYTE *r1) {
	unsigned char myAddress=0x20;			// 
	unsigned char retVal=1;
	unsigned char b,b1;

	if(q<4) {
		myAddress+=q*2;
		I2CSTART();
		if(!I2CTXByte((myAddress << 1) | 0)) {
			retVal=0;
			goto fine;
			}
		I2CTXByte(0x14);		// LATA
		b=r1[0] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[0] & 0b10000000 ? ~1 : ~0;
		I2CTXByte(table_7seg[b] & b1);
		b=r1[1] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[1] & 0b10000000 ? ~1 : ~0;
		I2CTXByte(table_7seg[b] & b1);
		I2CSTOP();
		myAddress++;
		I2CSTART();
		if(!I2CTXByte((myAddress << 1) | 0)) {
			retVal=0;
			goto fine;
			}
		I2CTXByte(0x14);		// LATA
		b=r1[2] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[2] & 0b10000000 ? ~1 : ~0;
		I2CTXByte(table_7seg[b] & b1);
		b=r1[3] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[3] & 0b10000000 ? ~1 : ~0;
		I2CTXByte(table_7seg[b] & b1);
		I2CSTOP();
		}
	else {
		myAddress+=(q-4)*2;
		I2CSTART2();
		if(!I2CTXByte2((myAddress << 1) | 0)) {
			retVal=0;
			goto fine;
			}
		I2CTXByte2(0x14);		// LATA
		b=r1[0] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[0] & 0b10000000 ? ~1 : ~0;
		I2CTXByte2(table_7seg[b] & b1);
		b=r1[1] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[1] & 0b10000000 ? ~1 : ~0;
		I2CTXByte2(table_7seg[b] & b1);
		I2CSTOP2();
		myAddress++;
		I2CSTART2();
		if(!I2CTXByte2((myAddress << 1) | 0)) {
			retVal=0;
			goto fine;
			}
		I2CTXByte2(0x14);		// LATA
		b=r1[2] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[2] & 0b10000000 ? ~1 : ~0;
		I2CTXByte2(table_7seg[b] & b1);
		b=r1[3] & 0b01111111;
		b= b ? b - '0' + 1 : 0;
		b1=r1[3] & 0b10000000 ? ~1 : ~0;
		I2CTXByte2(table_7seg[b] & b1);
		I2CSTOP2();
		}

fine:
	return retVal;
	}

unsigned long GestTasti(void) {
	unsigned long n;

	n = PORTAbits.RA4 ? 1 : 0;
	n |= PORTAbits.RA5 ? 2 : 0;
	n |= PORTAbits.RA6 ? 4 : 0;
	n |= PORTAbits.RA7 ? 8 : 0;
	n |= PORTAbits.RA9 ? 16 : 0;
	n |= PORTAbits.RA10 ? 32 : 0;
	n |= PORTAbits.RA14 ? 64 : 0;
	n |= PORTAbits.RA15 ? 128 : 0;

	n |= PORTBbits.RB0 ? 256 : 0;
	n |= PORTBbits.RB1 ? 512 : 0;
	n |= PORTBbits.RB2 ? 1024 : 0;
	n |= PORTBbits.RB3 ? 2048 : 0;
	n |= PORTBbits.RB4 ? 4096 : 0;
	n |= PORTBbits.RB5 ? 8192 : 0;
	n |= PORTBbits.RB10 ? 16384 : 0;
	n |= PORTBbits.RB11 ? 32768 : 0;

	n |= PORTBbits.RB12 ? 0x10000L : 0;
	n |= PORTBbits.RB13 ? 0x20000L : 0;
	n |= PORTCbits.RC1 ? 0x40000L : 0;
	n |= PORTCbits.RC2 ? 0x80000L : 0;
	n |= PORTCbits.RC3 ? 0x100000L : 0;
	n |= PORTCbits.RC4 ? 0x200000L : 0;
    n |= PORTDbits.RD0 ? 0x400000L : 0;
    n |= PORTDbits.RD1 ? 0x800000L : 0;

	return n ^ 0xffffff;			// 24 puls. a massa //erano 22 con maschera 0x3fffff
	}


signed char checkStuckKey(void) {
	WORD n,n1;
	DWORD n2,n3;

	n1=GestTasti() ^ configParms.inputFeat[0].uIOinPolarity;
	if(n1)
		return 1;


	return 0;
	}

#if defined(NUM_BYTE_TASTI_I2C) || defined(NUM_BYTE_LED_I2C)

#define EXPECTED_IOEXP_VALUE 0xC9			//CiN :)
void InitI2C(void) {
	BYTE i;

	for(i=0x20; i<0x28; i++) {			// 16 display
		I2CSTART();
		I2CTXByte((i << 1) | 0);			// addr. =2
		I2CTXByte(0x0);					// IODIRA
		I2CTXByte(0b00000000);					// 
		I2CTXByte(0b00000000);					// 
		I2CTXByte(0);					// IPOL
		I2CTXByte(0b00000000);					// 
		I2CTXByte(0);					// GPINTEN
		I2CTXByte(0);
		I2CTXByte(EXPECTED_IOEXP_VALUE);					// DEFVAL usato come CHECK (v.)
	//	I2CTXByte2(EXPECTED_IOEXP_VALUE+1);

// reset su pin IRQ attivo basso-alto
		I2CTXByte(0);					// secondo DEFVAL

		I2CTXByte(0);					// INTCON
		I2CTXByte(0);

		I2CTXByte(0b00000010);			// IOCON/INTPOL

		I2CSTOP();

		I2CSTART();
		I2CTXByte((i << 1) | 0);
		I2CTXByte(0x14);		// LATA
		I2CTXByte(0xff);
		I2CTXByte(0xff);
		I2CSTOP();

		}

	for(i=0x20; i<0x28; i++) {			// 12 display + 32 led
		I2CSTART2();
		I2CTXByte2((i << 1) | 0);			// addr. =0
		I2CTXByte2(0x0);					// IODIRA
		I2CTXByte2(0b00000000);					// 
	//	I2CSTOP2();
	//	I2CSTART2();		// siccome SEQOP � attivo di default, potremmo scrivere di seguito!
	// IOCON sta bene cos�...
	//	I2CTXByte2((0x20 << 1) | 0);			// addr. =0
	//	I2CTXByte2(0x1);					// IODIRB
		I2CTXByte2(0b00000000);					// 
		I2CTXByte2(0);					// IPOL
		I2CTXByte2(0);
		I2CTXByte2(0);					// GPINTEN
		I2CTXByte2(0);
		I2CTXByte2(EXPECTED_IOEXP_VALUE);					// DEFVAL usato come CHECK (v.)
	//	I2CTXByte2(EXPECTED_IOEXP_VALUE+1);

// reset su pin IRQ attivo basso-alto
		I2CTXByte2(0);					// secondo DEFVAL

		I2CTXByte2(0);					// INTCON
		I2CTXByte2(0);

		I2CTXByte2(0b00000010);			// IOCON/INTPOL

		I2CSTOP2();

		I2CSTART2();
		I2CTXByte2((i << 1) | 0);
		I2CTXByte2(0x14);		// LATA
		I2CTXByte2(0xff);
		I2CTXByte2(0xff);
		I2CSTOP2();

		}

	}


#ifndef I2CCheck

BYTE I2CCheck(void) {
	BYTE i,cc;

	for(i=0x20; i<0x28; i++) {

		I2CSTART2();
	  // Reset condizione di start
		I2CTXByte2((i << 1) | 0);			// il 6�
		I2CTXByte2(0x06);		// DEFVAL, una specie di dummy usato come check
		I2CSTART2();
		I2CTXByte2((i << 1) | 1);
		cc=I2CRXByte2();
		if(cc != EXPECTED_IOEXP_VALUE)
			break;
		I2CBITOUT2(1);		      // to stop transmission
		I2CSTOP2();
		}

//	return 1;
	return cc == EXPECTED_IOEXP_VALUE;

#warning aggiungere altro i2c!
	}
#endif





#endif


// ----------------------------------------------------
void resetSettings(unsigned char gruppo) {
	int i;
	BYTE *p,*p2;
	extern const struct SAVED_PARAMETERS DefaultParameters;

  p=(BYTE *)&DefaultParameters;
	p2=(BYTE *)&configParms;
	switch(gruppo) {			// direi che non ha senso la differenziazione
		case 0:
		case 1:
		case 2:
		case 3:
		case 4:
			for(i=0; i<sizeof(struct SAVED_PARAMETERS); i++) {
		 		*p2++=*p++;
				} 
			break;
		}

	for(i=0; i<3; i++) {
		mLed0=mLed1=mLed2=1;
		__delay_ms(200);
		ClrWdt();
		mLed0=mLed1=mLed2=0;
		__delay_ms(200);
		ClrWdt();
		}

	temp_baud=configParms.baudRate;			// necessario per la fottutissima variabile specchio
	temp_nodeID=configParms.nodeID;			// 
	saveSettings(gruppo);
	}

void loadSettings(unsigned char gruppo) {
	int i,j;

rifo:
	I2CRead16Seq(0,16);			// 
	memcpy((unsigned char*)&configParms,I2CBuffer,16 /* solo la parte iniziale! */);
	if(configParms.signature != 0x4444) {
		resetSettings(1 /*gruppo*/);
		goto rifo;
		}

	// per serial number, lo leggo sempre
	I2CRead16Seq(512,4);			// 
	memcpy((unsigned char*)&configParms.serialNumber,I2CBuffer,4);

	switch(gruppo) {
		case 1:
		case 2:
			temp_baud=configParms.baudRate;			// 
			temp_nodeID=configParms.nodeID;
			i=(unsigned char*)&configParms.inputFeat[0].uIOinFilter-(unsigned char*)&configParms.signature;
			I2CRead16Seq(0,sizeof(I2CBuffer));			// 
			memcpy((unsigned char*)&configParms,I2CBuffer,i);
			if(gruppo==2)
				goto fine;

		case 3:
			j=(unsigned char*)&configParms.inputFeat[0].uIOinFilter-(unsigned char*)&configParms.signature;
#ifdef NUM_ANA 
			i=(unsigned char*)&configParms.anaRange[0]-(unsigned char*)&configParms.inputFeat[0].uIOinFilter;
#else
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.inputFeat[0].uIOinFilter;
#endif
			I2CRead16Seq(128,sizeof(I2CBuffer));			// 
			memcpy(((unsigned char*)&configParms)+j,I2CBuffer,i);

		case 4:
#ifdef NUM_ANA 
			j=(unsigned char*)&configParms.anaRange[0]-(unsigned char*)&configParms.signature;
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.anaRange[0];
#else
			j=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.signature;
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.fine;
#endif
			I2CRead16Seq(256,sizeof(I2CBuffer));			// 
			memcpy(((unsigned char*)&configParms)+j,I2CBuffer,i);
			break;
		}

/*	addr=0;
	i=sizeof(configTerminale);
	p=(BYTE *)&configTerminale;
	do {
		I2CRead16Seq(addr,min(i,EEPROM_CHUNK));			// 
		memcpy(p,I2CBuffer,min(i,EEPROM_CHUNK));
		addr+=EEPROM_CHUNK;
		p+=EEPROM_CHUNK;
		i-=EEPROM_CHUNK;
		} while(i>0);
*/
fine: ;
	}

void saveSettings(unsigned char gruppo) {
	int i,j;

/*
    Sub-index 0 the amount of sub-indexes
    Sub-index 1 refers to all parameters that can be stored on the device.
    Sub-index 2 refers to communication related parameters (Index 1000h - 1FFFh manufacturer specific communication parameters)
        Epec devices have communication parameters in index 2003h or 2150h, using sub-index 2 saves also these communication parameters
    Sub-index 3 refers to application related parameters (Index 6000h - 9FFFh manufacturer specific application parameters)
    Sub-indexes 4 - 127 manufacturers may define their own parameter groups
        In Epec units, sub-index 4 saves all in all ODs.
        The application can use sub-indexes 5-127 to define the application specific store/restore areas. The application specific sub-indexes can be defined in MultiTool, for more information see MultiTool manual (available in Epec extranet).
    Sub-indexes 128-254 are reserved for future use
*/

	configParms.signature=0x4444;		// c'� anche di l� :)
#warning MAX STRUCT PARAMETERS salvati=128

	switch(gruppo) {
		case 1:			// tutti i parametri, inclusi comunicazione
		case 2:			// idem
			configParms.baudRate=temp_baud;
			configParms.nodeID=temp_nodeID;
			i=(unsigned char*)&configParms.inputFeat[0].uIOinFilter-(unsigned char*)&configParms.signature;
			memcpy(I2CBuffer,(unsigned char*)&configParms, i);
			I2CWritePage16(0,sizeof(I2CBuffer));				// 
			if(gruppo==2)
				goto fine;

		case 3:			// parametri definizione I/O
			j=(unsigned char*)&configParms.inputFeat[0].uIOinFilter-(unsigned char*)&configParms.signature;
#ifdef NUM_ANA 
			i=(unsigned char*)&configParms.anaRange[0]-(unsigned char*)&configParms.inputFeat[0].uIOinFilter;
#else
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.inputFeat[0].uIOinFilter;
#endif
			memcpy(I2CBuffer,((unsigned char*)&configParms)+j, i);
			I2CWritePage16(128,sizeof(I2CBuffer));				// 

		case 4:
#ifdef NUM_ANA 
			j=(unsigned char*)&configParms.anaRange[0]-(unsigned char*)&configParms.signature;
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.anaRange[0];
#else
			j=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.signature;
			i=(unsigned char*)&configParms.fine-(unsigned char*)&configParms.fine;
#endif
			memcpy(I2CBuffer,((unsigned char*)&configParms)+j, i);
			I2CWritePage16(256,sizeof(I2CBuffer));				// 
			break;

		case 100:			// per serial number!
			memcpy(I2CBuffer,(unsigned char*)&configParms.serialNumber, 4);
			I2CWritePage16(512,4);				// 
			break;
		}

fine: ;
	}

#if defined(__PIC24FV32KA302__) || defined(__PIC24FV32KA301__)
//__HAS_EEDATA__ forse c'� solo su XC...
struct SAVED_PARAMETERS __attribute__ ((space(eedata))) eeData = 
#else
#warning NON C'E' EEPROM!
const struct SAVED_PARAMETERS DefaultParameters =
#endif
	{ 
	0x4444, 	// signature 4437

	5, 				// baudrate		125K
	1,				// nodeId
	0,				// terminatorsMode
	0,			// NMTEHeartBeat
	{255,255},	// uSync

//#warning gestisco blocchi da 128 in EEprom, mettendo a indirizzi fissi 0/128/256 i blocchetti
	{{0x0000,0x0000,0xffff,0x0000,0x0000,0xffff},		//uIOinFilter,uIOinPolarity,uIOinIntChange,uIOinIntRise,uIOinIntFall,uIOinIntEnable
	{0x0000,0x0000,0xffff,0x0000,0x0000,0xffff},		//

	},

	{{0x0000,0x0000,0x0000},			// statoOutput
#ifdef NUM_BYTE_LED_I2C
	{0x0000,0x0000,0x0000},
#if NUM_BYTE_LED_I2C>(2)			// 4 fisso qua
	{0x0000,0x0000,0x0000},
#endif
#endif
	},


#ifdef USA_PWM 
	{50,			// duty cycle pwm
#if NUM_PWM>(1)
	50,
#if NUM_PWM>(2)
	50,
#if NUM_PWM>(3)
	50
#endif
#endif
#endif
	},

	{1100,			// freq pwm; min 1068
#if NUM_PWM>(1)
	1100,
#if NUM_PWM>(2)
	1100,
#if NUM_PWM>(3)
	1100
#endif
#endif
#endif
	},
#endif

	0,				// fine-dummy
	SERNUM		// serial number

	};		

// ----------------------------------------------------------------------------------
void setTerminators(unsigned char t) {

	switch(t) {
		case 0:
			mSwitch1=mSwitch2=0;
			break;
		case 1:
			mSwitch1=1;
			mSwitch2=0;
			break;
		case 2:
			mSwitch1=1;
			mSwitch2=1;
			break;
		}
	}

extern REQ_STAT	_uSDO1ACode;		// Abort code...

void _setTerminatorsMode(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(BYTE *)(uDict.obj->pReqBuf) = configParms.terminatorsMode;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			n=*(BYTE *)(uDict.obj->pReqBuf);
//			_uSDO1TxBuf[4] = 0;
//			_uSDO1TxBuf[5] = 0;
//			_uSDO1TxBuf[6] = 0;
//			_uSDO1TxBuf[7] = 0;
			if(n>=0 && n<=2) {
				configParms.terminatorsMode=n;
				setTerminators(configParms.terminatorsMode);
//				_uSDO1TxBuf[4] = 1;
				}
			else {
				_uSDO1ACode=E_PARAM_RANGE;
//o anche uDict.ret
				}	
			break;
		}	
	}

void CO_SetBaudRate(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(BYTE *)(uDict.obj->pReqBuf) = temp_baud;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			n=*(BYTE *)(uDict.obj->pReqBuf);
//			_uSDO1TxBuf[4] = 0;
//			_uSDO1TxBuf[5] = 0;
//			_uSDO1TxBuf[6] = 0;
//			_uSDO1TxBuf[7] = 0;
			if(n>=0 && n<=9) {			// v. CANSetBitRate e ecan1ClkInit
				temp_baud=n;
				}
			else {
				_uSDO1ACode=E_PARAM_RANGE;
//o anche uDict.ret
				}
			break;
		}	
	}

void CO_SetNodeId(void) {
	BYTE n;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(BYTE *)(uDict.obj->pReqBuf) = temp_nodeID;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			n=*(BYTE *)(uDict.obj->pReqBuf);
//			_uSDO1TxBuf[4] = 0;
//			_uSDO1TxBuf[5] = 0;
//			_uSDO1TxBuf[6] = 0;
//			_uSDO1TxBuf[7] = 0;
			if(n>=1 && n<=127) {
				temp_nodeID=n;
				}
			else {
				_uSDO1ACode=E_PARAM_RANGE;
//o anche uDict.ret
				}
			break;
		}	
	}


extern unsigned char _uSDO1RxBuf[CO_SDO1_MAX_RX_BUF];
extern DICT_OBJ	_uSDO1Dict;
void CO_RestoreDefault(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
//			*(BYTE *)(uDict.obj->pReqBuf) = ?;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			if(_uSDO1RxBuf[0]=='d' && _uSDO1RxBuf[1]=='a' && _uSDO1RxBuf[2]=='o' && _uSDO1RxBuf[3]=='l') {
				resetSettings(_uSDO1Dict.subindex); 
				}
//o anche uDict.ret
			break;
		}	
	}

void CO_SaveParameters(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
//			*(BYTE *)(uDict.obj->pReqBuf) = ?;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			if(_uSDO1RxBuf[0]=='e' && _uSDO1RxBuf[1]=='v' && _uSDO1RxBuf[2]=='a' && _uSDO1RxBuf[3]=='s') {
				saveSettings(_uSDO1Dict.subindex); 
				}
//o anche uDict.ret
 			break;
		}	
	}

signed char PushErrorsFIFO(WORD w,WORD w2) {		// sarebbe doubleword, ma la parte alta � "user defined" quindi 0 per ora
	int i;

	for(i=(sizeof(uCO_DevErrReg2)/sizeof(DWORD))-1; i; i--) {
		uCO_DevErrReg2[i]=uCO_DevErrReg2[i-1];
		}
	uCO_DevErrReg2[0]=MAKELONG(w,w2);

	return 1;
	}

DWORD PopErrorsFIFO() {
	DWORD w=0;
	int i;

	for(i=(sizeof(uCO_DevErrReg2)/sizeof(DWORD)); i; i--) {
		if(uCO_DevErrReg2[i-1]) {
			w=uCO_DevErrReg2[i-1];
			uCO_DevErrReg2[i-1]=0;
			}
		}
	return w;
	}

void CO_COMM_AccessErrorsFIFO(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
//			(*(UNSIGNED16 *)(uDict.obj->pReqBuf)).word = PopErrorsFIFO();
			(*(UNSIGNED16 *)(uDict.obj->pReqBuf)).word = uCO_DevErrReg2[_uSDO1Dict.subindex-1];
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// ahem? s�? :) uCO_DevErrReg2 = (*(UNSIGNED16 *)(uDict.obj->pReqBuf)).word;
			break;
		}	
	}

void CO_COMM_AccessErrorsFIFOCount(void) {
	int i;

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			for(i=0; i<(sizeof(uCO_DevErrReg2)/sizeof(DWORD)); i++) {
				if(!uCO_DevErrReg2[i])
					break;
				}
			(*(UNSIGNED16 *)(uDict.obj->pReqBuf)).word = i;
			break;

		case DICT_OBJ_WRITE: 	// Write the object ossia pulisce
			for(i=0; i<(sizeof(uCO_DevErrReg2)/sizeof(DWORD)); i++) 
				uCO_DevErrReg2[i]=0;
			break;
		}	
	}


#ifdef USA_BOOTLOADER 
// per BootLoader remap
#if 0			// cmq non serve...
void __attribute__ ((address(0x1000))) ISRTable(){

	asm("reset"); //reset instruction to prevent runaway code
	asm("goto %0"::"i"(&_T2Interrupt));  //T2Interrupt's address
//	asm("goto %0"::"i"(&_T3Interrupt));  //T3Interrupt's address
	asm("goto %0"::"i"(&_AddressError));  //AddressError's address
	asm("goto %0"::"i"(&_StackError));  	//StackError's address
//	asm("goto %0"::"i"(&_U3RXInterrupt));  //U3RXInterrupt's address
	asm("goto %0"::"i"(&_U1RXInterrupt));  //U1RXInterrupt's address
	asm("goto %0"::"i"(&_U1TXInterrupt));  //U1TXInterrupt's address
	asm("goto %0"::"i"(&_U2RXInterrupt));  //U2RXInterrupt's address
	asm("goto %0"::"i"(&_U2TXInterrupt));  //U2TXInterrupt's address
	}
#endif
#endif
