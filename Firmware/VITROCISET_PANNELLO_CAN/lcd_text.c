/* LCD.c
	x TRE LCD basati su HD44870, senza R/W

*/
//8bit

//#include "GenericTypeDefs.h"
//#include "Compiler.h"
//#include "hardwareprofile.h"		// per CS_EEPROM
#include	"CO_MAIN24.H"
#include	<libpic30.H>

#include	"CO_dev.H"
#include	<Timer.H>
#include	"myTimer.H"
#include	"DemoObj.h"
#include	"swi2c.h"

#include <timer.h>
#include <stdlib.h>
#include <ctype.h>
#include <outcompare.h>
#include <ports.h>
#include <libpic30.h>
#include "lcd.h"



#define LCD_MAX_COLS 20
#define LCD_MAX_ROWS 4


void subLCDInit(BYTE);

void subLCDget(BYTE);


void SetBeep(void);


#pragma udata

BYTE LCDCols;			
BYTE LCDRows;			
BYTE LCDX;				
BYTE LCDY;			  
BYTE LuceLCD;					// mirrored in EEPROM
BYTE ContrLCD;		
BYTE StatoLCD;					// usato per indicare se il cursore � attivo (b0);
BYTE BeepFreq;
BYTE TimerBuzz;

BYTE FLAGS_ESC;					// per sequenze ESCAPE: 0=SetLuce, 1=SetContr, 2=SetFreq; 3=SetLed; 4&5=SetXY (1&2); 6=SetKClick; 7=invert_bitmap
BYTE FLAGS;

extern WORD I2CAddr;
BYTE I2CData;
BYTE I2CCnt;
BYTE I2CCntB;

BYTE lcd_temp0;			// usato dentro LCDOutCmd da 6963, in GetCharDef da KS0108
BYTE lcd_temp1;			// temporanea, usata da LCD
BYTE lcd_temp2;
BYTE lcd_temp3;
BYTE LCDtmpBuf[40];					// temp per scroll display (almeno 40 BYTE), BANCO 1

//BYTE *iconptr;						//	temp. per icone/bigchar LCD
WORD iconptr;						//	temp. per icone/bigchar LCD


//#define LCD_DELAY { Nop(); 	Nop(); 	Nop();	ClrWdt(); Nop(); 	Nop(); 	Nop();	Nop(); 	Nop(); 	Nop();	Nop(); 	Nop(); 	Nop();	Nop(); 	Nop(); 	Nop();	}
#define LCD_DELAY __delay_us(10);


void I2CDelay(void);



// ---------------------------------------------------------------------


//lang_1:				// italiano (in caso si volessero + lingue...)

const char CopyrDate[32] = {"4/1/18 - S/N 0001\n" };
//	messg "Numero di serie: SERNUM"

static const char Copyright[32] = { 'P','I','C','B','e','l','l',' ','f','w',' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',0 };
//	dt	"ADPM Synthesis - Terminale grafico USB "
//	dt  VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0', ' '
//	dt "08/04/05",0
const char *CopyrString=Copyright;
static const char *Copyr1= { "(C) Cyberdyne 2009-2018; ADPM Synthesis 1999-2008 - G.Dar\n" };


const char String1_l1[32] = { "Cyberdyne" /*"ADPM Synthesis"*/  /*	" K-tronic sas" */ };
const char String2_l1[] = {	' ','P','I','C','B','e','l','l',' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ',0 };
//static const char String2_l1[] = {	'S','k','y','N','E','T',' ','v',VERNUMH+'0','.',VERNUML/10+'0',(VERNUML % 10)+'0',' ',0 };
const char String3_l1[2] = 
	{ '_',0	};			// icona splash-screen ?


//const BYTE LUTLuce[16]= { 0,1,2,3, 4,5,7,8, 10,12,14,18, 20,24,28,32 };		 qua non serve..

void aggLuceContrasto(void) {
//	WORD w;


	}


  



// ---------------------------------------------------------------------

void print_init(BYTE q) {

	LCDCls(q);
	LCDX= (LCD_MAX_COLS-14)/2;				// "skynet/picbell vx.xx": 16 char
	LCDXY(q,1);						// X,Y=[2,1]

#if LCD_MAX_ROWS>1
	LCDWrite(q,String2_l1);
	LCDX=(LCD_MAX_COLS-10)/2;				// "cyberdyne": 10 char
	LCDY++;
	LCDXY_(q);						// X,Y=[3,2]

	LCDWrite(q,String1_l1);

	if(!mSwitch1) {
		LCDPrintLF(q);
#if LCD_MAX_ROWS>2
		LCDPrintHex(q,LuceLCD);
		LCDPrintHex(q,ContrLCD);
//		LCDPrintLF();
#endif
//		return;
		}

//	LCDWrite(String3_l1);		tolgo questa e da main stampo la data copyr.! (v.)
#else
	LCDWrite(q,String2_l1);
#endif

	}


// ---------------------------------------------------------------------

void LCDPutChar(BYTE q,BYTE ch) { 
	static BYTE parm1,parm2,nparms;

	if(FLAGS & (1 << IN_ESCAPE_CHAR)) {

		if(FLAGS_ESC & 1) {
			FLAGS_ESC=0;
			FLAGS &= ~(1 << IN_ESCAPE_CHAR);
//			EEscrivi_(&LuceLCD,ch-1);					// 1..16
			aggLuceContrasto();
			return;
			}

		if(FLAGS_ESC & 2) {
			FLAGS_ESC=0;
			FLAGS &= ~(1 << IN_ESCAPE_CHAR);
//			EEscrivi_(&ContrLCD,ch-1);					// 1..16
			aggLuceContrasto();
			return;
			}
		if(FLAGS_ESC & 4) {
			FLAGS_ESC=0;
			FLAGS &= ~(1 << IN_ESCAPE_CHAR);
			// minimo 32 (v. PWM luce) (circa 10KHz per buzzer)
			BeepFreq= ch < 32 ? 32 : ch;
			aggLuceContrasto();
			return;
			}
		if(FLAGS_ESC & 8) {
			FLAGS_ESC=0;
			FLAGS &= ~(1 << IN_ESCAPE_CHAR);
//			LATE = ch & 0xf;		// OCCHIO!
			return;
			}
		if(FLAGS_ESC & 0x10) {
// http://ascii-table.com/ansi-escape-sequences-vt-100.php
			if(isdigit(ch)) {		// set xy ESC [ x;y f/H
				if(!nparms)
					parm1=ch;			//NO! usare atoi... su + char...
				if(nparms==1)
					parm2=ch;			//NO! usare atoi... su + char...
				nparms++;
				}
			else {
				if(ch=='m') {
//					if(nparms)
//						bigFont=parm1 & 1;
					}	
				if(ch=='f' || ch=='H') {
					if(nparms>1) {
						LCDY = parm1;
						LCDX = parm2;
						LCDXY_(q);
						}
					}
				if(ch=='j') {
					if(nparms && parm1=='2' /*sistemare!*/)
						LCDCls(q);
					}	
				FLAGS_ESC=0;
				nparms=0;
				FLAGS &= ~(1 << IN_ESCAPE_CHAR);
				}	
					
			return;
			}
		if(FLAGS_ESC & 0x20) {
			if(ch & 1)
				FLAGS &= ~(1 << NOKCLICK);
			else
				FLAGS |= (1 << NOKCLICK);
			return;
			}

		switch(ch) {
			case 17:						// v. sopra
				FLAGS_ESC |= 1;
				break;
			case 18:						// un po' a caso...
				FLAGS_ESC |= 2;
				break;
			case 7:						// come "BEL"
				FLAGS_ESC |= 4;
				break;
			case 19:						// v. sopra
				FLAGS_ESC |= 8;
				break;
			case 91:						// vagamente ANSI... (VT100 dice: "ESC [ ROW ; COLUMN f" , e simili...
				FLAGS_ESC |= 0x10;
				nparms=0;
				break;
			}

		if(!FLAGS_ESC)		// se non e' stato trovato nessun char. accettabile, tolgo QUI ESC!
			FLAGS &= ~(1 << IN_ESCAPE_CHAR);

		return;
		}


	if(ch >= ' ') {

		if(ch & 0x80)			// i caratteri >=0x80 sono usati per i char user-defined...
			ch &= 0x7f;			// ...banalmente, se solo testo, li riporto al valore in LCD

LCDPutChar2:

  	LCDOutData(q,ch);


	  LCDX++;
	  if(LCDX >= LCDCols)
			goto LCDPrintLF;
		/*
		if(LCDRows == 1) {
		  if(LCDX == 8)
				LCDXY_();
			}		per 1-riga, v. anche LCDXY e frequanzimetro
			*/
		}

	else {

		switch(ch) {
	
			case 2:
				StatoLCD |= 1;
				LCDCursor(q,3);				// STX=cursor On
				break;
	
			case 3:
				StatoLCD &= 0xfe;			// ETX=cursor Off
				LCDCursor(q,0);
				break;
	
			case 7:
	//  call StdBeep						; resa ASINCRONA (per velocita' I2C e anche SER)!
				SetBeep();
				break;
	  
	
			case 9:
LCDtab:     
				LCDX++;
	
				LCDOutBlankRaw(q);		// meglio non LCDPutBlank, per lo stack!
								// cos� per� non gestisce l'a-capo...
	
			  if(LCDX & 7)
					goto LCDtab;
				break;
	
			case 11:
			  LCDScrollDown(q);
				break;
	
			case 12:					// FF = scroll up
	  		LCDScrollUp(q);
				break;
		
			case 10:				// LF
LCDPrintLF:
				LCDX=0;
				LCDY++;
	// METTERE un FLAG per disattivare scroll automatico (CHR$13) su ultima riga, per sfruttarla meglio??
			  LCDScroll(q);			// scroll SE necessario
				break;
	
			case 13:				// CR
			  LCDX=0;
				LCDXY_(q);
				break;
	  
	
			case 8:					//; BS
			  if(LCDX) {				// solo se X>0...
	
				  LCDX--;
	
				  LCDOutCmd(q,16);
					LCDOutBlankRaw(q);
				  LCDOutCmd(q,16);
					}
				break;
	
	
			case 14:			// shift-in (cursor left)
			  LCDOutCmd(q,16);
				break;
			case 15:			// shift-out (cursor right)
			  LCDOutCmd(q,20);
				break;

			case 17:			// DC1..2 usati per Luce LCD
				LuceLCD=15;
LCDPC_NoPrint_17_:
	//			EEcopiaAEEPROM(&LuceLCD);
				aggLuceContrasto();			// 
				break;
	
			case 18:
				LuceLCD=0;
				goto LCDPC_NoPrint_17_;
				break;
	
			case 19:					// DC3..4 usati per Led on/off
				mLed0=1;
	//			LED1_IO=1;
				break;
	
			case 20:
				mLed0=0;
				break;
	
			case 24:					// CAN (opp. FormFeed ?) = CLS
				LCDCls(q);
				break;
	
	
	
			case 27:					// ESC (comandi ausiliari)
				FLAGS |= (1 << IN_ESCAPE_CHAR);
				break;
	
			default:
	// LCDPut1:			eliminato, eqv. a return
				break;
	
			}
		}

	}


// ---------------------------------------------------------------------

void LCDXY_(BYTE q) {
	 BYTE i,lcd_temp2;

	lcd_temp2=0;

	switch(LCDRows) {
		case 1:
			if(LCDRows == 1 && LCDX >= 8) {			// patch x 1 riga... (v. anche PutChar)
				LCDOutCmd(q,0xc0 | (LCDX & 7));
				return;
				}
			else {
LCDXY1:
				i=LCDX & 0x7f;
				i+=lcd_temp2;
				LCDOutCmd(q,i | 0x80);
				}

			if(LCDX & 0x80) {
				LCDX &= 0x7f;
				LCDX-=LCDCols;
				}

			break;

		case 2:
LCDXY2:
			lcd_temp2=(LCDY & 1) * 0x40;
			goto LCDXY1;
			break;
  
		case 4:
			if(LCDCols < 40) {		// se e' 4x40 (M4024) va bene cosi' (hanno 2 ENABLE)...
				if(LCDY >= 2) {			// ...altrimenti (4x20), se la riga e' 2 opp. 3...
					LCDX+=LCDCols;		// ...aggiungo a X la larghezza (16 opp. 20)
					LCDX |= 0x80;			 //e me lo segno per poi ri-toglierla!
					}
				}
			goto LCDXY2;
			break;

		default:								// NON DEVE succedere!
			break;
			
		}
	}



// ---------------------------------------------------------------------


void subLCDInit(BYTE q) {

  LCDOutPortNoW(q,0,0x38);          // init 1st
	__delay_ms(4);								// >4.1mS
  LCDOutPortNoW(q,0,0x38);         // init 2nd
	__delay_ms(1);								// >100uS
  LCDOutPortNoW(q,0,0x38);          // init 3rd
	__delay_ms(1);								// >39uS
  LCDOutCmd(q,0b00111000);			      // function Set 0011NF**
  LCDOutCmd(q,0b00001100);      // display ON/OFF = 00001DCB, Display ON,Cursor&Blink OFF

  LCDOutCmd(q,0b00000110);	          // entry Mode Set = 000001IS, increment & shift off

//  LCDOutCmd(q,0b00000110);	             // cursore, blink e ON/OFF

  LCDOutCmd(q,0b00010000);	          // shift/move su OLED
  LCDOutCmd(q,0b00010111);	          // char/graph, power su OLED??
	}


const BYTE LCDUserDefChar[7][8]={ 
	{ 0b00000000,0b00000000,0b00000000,0b00000000,0b00011111,0b00011111,0b00011111,0b00011111 }, //1=mezzo basso
	{ 0b00011111,0b00011111,0b00011111,0b00011111,0b00000000,0b00000000,0b00000000,0b00000000 }, //2=mezzo alto
	{ 0b00000000,0b00000000,0b00011111,0b00011111,0b00011111,0b00011111,0b00000000,0b00000000	}, //3=mezzo centrale
	{ 0b11111111,0b11111111,0b00011111,0b00011111,0b00011111,0b00011111,0b11111111,0b11111111	}, //4=pieno
	{ 0b11111111,0b11111111,0b00011111,0b00011111,0b00011111,0b00011111,0b00000000,0b00000000 }, //5=un po' meno pieno
//purtroppo ce ne sono solo 8!
	{ 0b00000000,0b00000100,0b00001010,0b00000100,0b00000000,0b00000000,0b00000000,0b00000000 },	// 6=grado �
	{ 0b00000110,0b00001001,0b00011100,0b00001000,0b00011100,0b00001001,0b00000110,0b00000000	},		// EURO
// const BYTE LCDUserDefChar[]={        db 0h,0h,0h,0h,0h,0h,0,0 ; sole che lampeggia
//	{ 0b00011000,0b00011100,0b00001010,0b00001001,0b00001010,0b00011100,0b00011000,0b00000000 },	// 7=luna normale
//const BYTE LCDUserDefChar[]={  db 1fh,1fh,1fh,1fh,1fh,1fh,0,0 ; luna che lampeggia
//	{ 0b00000010,0b00000101,0b00000101,0b00001001,0b00011111,0b00001110,0b00010000,0b00000000 }	// sveglia
	};

void subLCDdefChar(BYTE q) {
	 BYTE x,y;

	LCDOutCmd(q,0x48);			// definisco char user-def partendo da 1 (non 0!)
	for(y=0; y<7; y++) {	// parametrizzare!!!
		for(x=0; x<8; x++) {
		  LCDOutData(q,LCDUserDefChar[y][x]);
			}
		}
  
	}


BYTE LCDInit(BYTE q) {

	FLAGS_ESC=FLAGS=0;

	LCDY=0;
	__delay_ms(10);				// 15mS
  subLCDInit(q);

	subLCDdefChar(q);



	LuceLCD=8;
	ContrLCD=10;

  LCDRows=LCD_MAX_ROWS;            // PARAMETRIZZARE (da FLASH?)
//            cp  3
//            jp m,LCDInit2 

//            movlw 2
//            movwf LCDY
//            call subLCDInit
            
LCDInit2:
  LCDCols=LCD_MAX_COLS;            // PARAMETRIZZARE (da FLASH?)

	LCDCls(q);

	aggLuceContrasto();

	return 1;
	}
// CONTINUA

// ---------------------------------------------------------------------

void LCDCls(BYTE q) {

//	movfw LCDRows
//  cp  3
//            jp m,LCDCls1
//            ld  a,2
//            ld  (LCDY),a
//            dec a
//  call LCDOutPort

LCDCls1:    
	LCDX=0;
  LCDY=0;
//	bcf  STATUS,C

  LCDOutCmd(q,1);
// CONTINUA                        ; cls & home (comando 1)
	LCDHome(q);		// questo merdosissimo coso pare NON fare home dopo CLS... 22.12.17

	__delay_ms(5);
	}

void LCDHome(BYTE q) {

	LCDX=0;
  LCDY=0;

  LCDOutCmd(q,2);

	__delay_ms(2);
	}

// ---------------------------------------------------------------------

BYTE LCDOutPort(BYTE q,BYTE c, BYTE n) {												// scrivo W nel display
                                  // dato se C=1, comando se C=0
	BYTE t;


	LCDOutPortNoW(q,c,n);
	}

BYTE LCDOutPortNoW(BYTE q,BYTE c,BYTE n) {									// qui per non aspettare BUSY!

	if(c)
		m_LCDRSBit=1;										// C=1, dato
	else
		m_LCDRSBit=0;										// C=0, comando
	TRISE &= ~0b0000000011111111;


	LCD_DELAY;
	switch(q) {
		case 0:
			m_LCDEn1Bit=1;										// scrivo!
			break;
		case 1:
			m_LCDEn2Bit=1;
			break;
		case 2:
			m_LCDEn3Bit=1;
			break;
		}
	LCD_DELAY;
	            
	LATE &= ~0b0000000011111111;
	LATE |= n;				// 
	LCD_DELAY;

	switch(q) {
		case 0:
			m_LCDEn1Bit=0;										// scrivo!
			break;
		case 1:
			m_LCDEn2Bit=0;
			break;
		case 2:
			m_LCDEn3Bit=0;
			break;
		}
	LCD_DELAY;


//	TRISE |= 0b0000000011111111;

	m_LCDRSBit=1;

	ClrWdt();
	__delay_ms(1);
	
  return n;
	}

BYTE LCDOutPort4Bit(BYTE q,BYTE n) {
/*
	m_LCDRSBit=0;										// C=0, comando
	TRISE &= ~0b0000000001111000;

	LCD_DELAY;
	m_LCDEn1Bit=1;										// scrivo!
	LCD_DELAY;
	      
	LATE &= ~0b00000001111000;
	LATE |= ((n & 0x0f) << 3);				// nibble L, shift 3 a sx
	LCD_DELAY;

	m_LCDEnBit=0;
	LCD_DELAY;

	TRISE |= 0b0000000001111000;
*/
  return n;
	}




void LCDOutBlankRaw(BYTE q) {
  char c;

	c=' ';

  LCDOutData(q,c);
	}


// ---------------------------------------------------------------------
void LCDWrite(BYTE q,const char *p) {
	
	while(*p)
	  LCDPutChar(q,*p++);
	}

void LCDWriteN(BYTE q,const char *p, BYTE n) {
	
	do {
	  LCDPutChar(q,*p++);
		} while(--n);
	}

// ---------------------------------------------------------------------

void subLCDget(BYTE q) {
	BYTE *p;
	BYTE i,lcd_temp1,t;


	}


void LCDScroll(BYTE q) {
	BYTE i,*p;
	BYTE lcd_temp4;

	if(LCDRows > 1) {				// se 1 riga, niente scroll!

		if(LCDY == LCDRows) {

LCDScrollUp:
// qua, ciccia...
		
			LCDY--;
//			LCDClearCurrentLine(q);
			}
	
	  LCDXY_(q);
		}

	}


void LCDScrollDown(BYTE q) {
	BYTE i,*p;
	BYTE lcd_temp4;


	}



void LCDPrintHex(BYTE q,BYTE w) {					// stampa in HEX il BYTE in W
	 BYTE n;

	n=(w >> 4) & 0xf;
	if(n>=10)
		n+=7;
	LCDPutChar(q,n+'0');
	n=w & 0xf;
	if(n>=10)
		n+=7;
	LCDPutChar(q,n+'0');
	LCDPutBlank(q);
	}



void LCDPrintDec2(BYTE q,BYTE n) {				// stampa BYTE in W come decimale a 2 cifre ('0' trailing)
	char myBuf[4];

	itoa(n,myBuf,10);
	LCDOutData(q,myBuf[0]);
	LCDOutData(q,myBuf[1]);
	}



void LCDClearLine(BYTE q,BYTE n) {					// entra n=linea da pulire
	 BYTE temp;
	
	LCDX=0;
	LCDXY(q,n);
	temp=LCDCols;
	while(temp--) {
		LCDOutBlankRaw(q);
		}
	LCDXY_(q);					// in caso servisse, sono a inizio riga!
	}



// ----------------------------------------------------------------------------







//         -------------------------------------------------------------------------
#if PIXEL_PER_CELLA==6
BYTE drawLineaVertTable[8]= { 0xfd,0xfc,0xfb,0xfa,0xf9,0xf8,0xfd,0xfc };
	//	movlw 11111101b					; SetBit (su 6...)
#else
BYTE drawLineaVertTable[8]= { 0xff,0xfe,0xfd,0xfc,0xfb,0xfa,0xf9,0xf8 };
	//	movlw 11111101b					; SetBit (su 8...)
#endif

void drawLinea(BYTE x1,BYTE y1,BYTE x2,BYTE y2) {		// entrano coord. (lcd_temp2:lcd_temp3) (lcd_temp4:lcd_temp5)
								// le X VENGONO ALTERATE!
								//	alla fine si fa LCDXY per rimettere l'addr ptr a posto per il testo...
	BYTE temp;
	WORD bPos;
	BYTE lcd_temp5;



	}




void drawRect(BYTE x1,BYTE y1,BYTE x2,BYTE y2,BYTE w) {
		// entrano coord. (lcd_temp6:lcd_temp7) (lcd_temp8:lcd_temp9); W=0 se vuoto, 1 se pieno, 2=grigio


	}



void SetBeep(void) {									// asincrono, viene poi resettato da interrupt

//	PR2=BeepFreq;
//	CCPR1L=BeepFreq >> 1;		// /2 (duty cycle 50%)
#ifndef SOFT_BUZZER
	SetDCOC3PWM(BeepFreq,BeepFreq>>1);
#else
	TRISCbits.TRISC14=0;
#endif

//	movlw  00000111b
//	movwf  T2CON
	TimerBuzz=2;
	}


/*
char *itoa(int i,char *buffer) {
	unsigned int n;
	unsigned char negate = 0;
	int c = 6;

	if(i < 0) {
		negate=1;
		n = -i;
		} 
	else if(i == 0) {
		buffer[0] = '0';
		buffer[1] = 0;
		return buffer;
		} 
	else {
		n = i;
		}
	buffer[c--] = 0;
	do {
		buffer[c--] = (n % 10) + '0';
		n = n / 10;
		} while (n);
	if(negate) {
		buffer[c--] = '-';
		}
	return &buffer[c+1];
	}
*/

