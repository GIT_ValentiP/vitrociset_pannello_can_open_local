#ifndef __I2C_INCLUDED
#define __I2C_INCLUDED

#define m_I2CClkBit           LATDbits.LATD10
#define m_I2CDataBit          LATDbits.LATD9
#define m_I2CDataBitI         PORTDbits.RD9
#define m_I2CClkBitI          PORTDbits.RD10
#define I2CDataTris      TRISDbits.TRISD9
#define I2CClkTris       TRISDbits.TRISD10
#define SPI_232_I 			PORTDbits.RD9
#define SPI_232_IO 			LATDbits.LATD9
#define SPI_232_I_TRIS 			TRISDbits.TRISD9
#define SPI_232_O 			LATDbits.LATD10
#define SPI_232_OI 			PORTDbits.RD10
#define SPI_232_O_TRIS 			TRISDbits.TRISD10

#define m_I2CClkBit2           LATAbits.LATA3
#define m_I2CDataBit2          LATAbits.LATA2
#define m_I2CDataBitI2         PORTAbits.RA2
#define m_I2CClkBitI2          PORTAbits.RA3
#define I2CDataTris2      TRISAbits.TRISA2
#define I2CClkTris2       TRISAbits.TRISA3



unsigned char I2CRX1Byte(void);
unsigned char I2CRXByte(void);
unsigned char I2CTXByte(unsigned char );
unsigned char I2CWritePage(unsigned char,unsigned char);
unsigned char I2CWritePage16(unsigned int,unsigned char);
unsigned char I2CWritePagePoll(void);
unsigned char I2CReadRandom(unsigned char);
unsigned char I2CReadRandom16(unsigned int);
unsigned char I2CRead8Seq(unsigned char,unsigned char);
unsigned char I2CRead16Seq(unsigned int,unsigned char);
void I2CWriteRTC(unsigned char ,unsigned char );
unsigned char I2CReadRTC(unsigned char );
void I2CSTART(void);
void I2CSTOP(void);
unsigned char I2CBITIN(void);
void I2CBITOUT(unsigned char);
void I2CDelay(void);
void waitClockHigh(void);
unsigned char releaseBus(void);

unsigned char I2CRX1Byte2(void);
unsigned char I2CRXByte2(void);
unsigned char I2CTXByte2(unsigned char );
void I2CSTART2(void);
void I2CSTOP2(void);
unsigned char I2CBITIN2(void);
void I2CBITOUT2(unsigned char);
void waitClockHigh2(void);
unsigned char releaseBus2(void);


extern unsigned char I2CBuffer[128];

#endif

