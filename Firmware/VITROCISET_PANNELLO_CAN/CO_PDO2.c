

// The communications conducted in this object incorporates 
// one transmit and one receive endpoint


// Global definitions
#if defined(__18CXX)
#include	"CO_TYPES.H"			// Standard types
#else
#include "co_main24.h"
#include	"CO_TYPES24.H"
#endif
#include	"CO_DICT.H"				
#include	"CO_COMM.H"				// Communications
#include	"CO_PDO.H"				// PDO functions
#include	"CO_PDO2.H"				// PDO functions


rom unsigned long uRPDOMap2 = 0x38947928L;

#ifdef USE_PDO_2
UNSIGNED32		uRPDO2Comm;		// RPD2 communication setting
UNSIGNED32		uTPDO2Comm;		// TPD2 communication setting
_PDOBUF 		_uPDO2;
unsigned char 	_uRPDO2Handles,_uTPDO2Handles;
#endif



#ifdef USE_PDO_2

// Process access events to the transmission type
void CO_COMM_RPDO2_TypeAccessEvent(void) {

	switch (mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}

// Process access events to the COB ID
void CO_COMM_TPDO2_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(mTPDOGetCOB(2));
//USARE _CO_COB_MCHP2CANopen_SID(mTPDOGetCOB(2))

			myCob=Microchip2Canopen(mTPDOGetCOB(2));
			
			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) =myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS)	{
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mTPDOGetCOB(2)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mTPDOIsOpen(2)) {
						mTPDOClose(2);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(2))).PDO_DIS = 1;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mTPDOGetCOB(2)) & 0xFFFFEFFFL)) {
					// but only open if the PDO endpoint was closed
					if(!mTPDOIsOpen(2)) {
						mTPDOOpen(2);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(2))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		}	
	}

void CO_COMM_TPDO2_TypeAccessEvent(void) {
	unsigned char tempType;
	
	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the 
			// structure with legth, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// Write the Type to the buffer
			*(uDict.obj->pReqBuf) = configParms.uSyncSet[1];
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			tempType = *(uDict.obj->pReqBuf);
			if((tempType >= 0) && (tempType <= 240)) {
				// Set the new type and resync
				uDemoSyncCount[1] = configParms.uSyncSet[1] = tempType;
				}
			else {
				if((tempType == 254) || (tempType == 255))	{
					configParms.uSyncSet[1] = tempType;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			
			break;
		}	
	}


// Process access events to the inhibit time
void CO_COMM_TPDO2_ITimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}


// Process access events to the event timer
void CO_COMM_TPDO2_ETimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}


void CO_COMM_RPDO2_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(mRPDOGetCOB(2));
//USARE _CO_COB_MCHP2CANopen_SID(mRPDOGetCOB(2))

			myCob=Microchip2Canopen(mRPDOGetCOB(2));

			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS) {
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mRPDOGetCOB(2)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mRPDOIsOpen(2)) {
						mRPDOClose(2);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(2))).PDO_DIS = 1;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mRPDOGetCOB(2)) & 0xFFFFEFFFL))	{
					// but only open if the PDO endpoint was closed
					if(!mRPDOIsOpen(2)) {
						mRPDOOpen(2);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(2))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			break;
		case DICT_OBJ_INFO: 	// Read the object
		break;
		}	
	}


void CO_COMM_PDO2_Create(WORD tSID,WORD rSID) {
//2	// Convert to MCHP
//	mTOOLS_CO2MCHP(0x280 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x280L | _uCO_nodeID.byte)
	
	// Store the COB
	mTPDOSetCOB(2,Canopen2Microchip((tSID << 7) | configParms.nodeID));

	// Convert to MCHP
//	mTOOLS_CO2MCHP(0x300 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x300L | _uCO_nodeID.byte)

	// Store the COB
	mRPDOSetCOB(2,Canopen2Microchip((rSID << 7) | configParms.nodeID));
	}

void CO_COMM_PDO2_Open(BYTE enableRX, BYTE enableTX) {
	
/*	uTPDOComm2.word = 0x200L + _uCO_nodeID.byte;
	mTOOLS_CO2MCHP(uRPDOComm2.word);
//USARE _CO_COB_CANopen2MCHP_SID(0x200L | _uCO_nodeID.byte)
	uTPDOComm2.word = mTOOLS_GetCOBID();*/

// Call the driver and request to open a receive endpoint
	_uRPDO2Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_RPDO_2, uRPDO2Comm.word);
	if(_uRPDO2Handles) {
		if(enableRX)
			COMM_RPDO_2_EN = 1;	
		}

	_uTPDO2Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_TPDO_2, uTPDO2Comm.word);
	if(_uTPDO2Handles) {
		if(enableTX)	
			COMM_TPDO_2_EN=1;
		}
	}

void CO_COMM_PDO2_Close(void) {
	
	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_uRPDO2Handles);
	CANCloseMessage(_uTPDO2Handles);
	COMM_RPDO_2_EN=0;	
	COMM_TPDO_2_EN=0;
	}

void CO_COMM_TPDO2_RXEvent(unsigned char b) {
//LATCbits.LATC1^=1;

	mTPDORead(2);


//	*((_DATA8 *)(_uPDO4.RPDO.buf)) = *((__eds__ _DATA8 *)(mCANGetPtrRxData(b)));
// QUA NO, non � previsto e non abbiamo messo un buffer :)
// se servono, v. mRPDOSetRxPtr in DemoObj


	if(mCANIsGetRTR(b))	{
		mTPDOWritten(2);
		}

	}

void CO_COMM_RPDO2_RXEvent(unsigned char b) {

//LATAbits.LATA0^=1;

	mRPDORead(2);

	*((_DATA8 *)(_uPDO2.RPDO.buf)) = *((__eds__ _DATA8 *)(mCANGetPtrRxData(b)));
//	_uPDO2.RPDO.len = mCANGetDataLen(b);
//USARE	_uPDO2.RPDO.buf[0]=mCANGetDataByte0(b);
//	_uPDO2.RPDO.buf[1]=mCANGetDataByte1(b);

	_uPDO2.RPDO.len = mCANGetDataLen(b);

	}


void CO_COMM_TPDO2_TXEvent(BYTE b) {

//		mCANPutDataByte0(0,LOBYTE(n));
//		mCANPutDataByte1(0,HIBYTE(n));
	// Call the driver, load data to transmit
	*((__eds__ _DATA8 *)(mCANGetPtrTxData(b))) = *((_DATA8 *)(_uPDO2.TPDO.buf));

	mCANPutDataLen(b,_uPDO2.TPDO.len);
//	mCANPutDataByte0(0,_uPDO2.TPDO.buf[0]);
//	mCANPutDataByte1(0,_uPDO2.TPDO.buf[1]);

//	mTOOLS_CO2MCHP(0x280 | _uCO_nodeID.byte);
//USARE _CO_COB_CANopen2MCHP_SID(0x280L | _uCO_nodeID.byte)
//	mTPDOSetCOB(2,Canopen2Microchip(0x280 | _uCO_nodeID.byte));
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(b)) = uTPDO2Comm.word;

	}

void CO_COMM_RPDO2_TXEvent(BYTE b) {

	}


void CO_PDO2LSTimerEvent(void) {
	
	}

void CO_RPDO2TXFinEvent(void) {
	
	}

void CO_TPDO2TXFinEvent(void) {
	
	}

#endif



