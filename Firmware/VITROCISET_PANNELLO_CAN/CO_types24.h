/*****************************************************************************
 *
 * Microchip CANopen Stack (Data types)
 *
 *****************************************************************************
 * FileName:        CO_TYPES.H
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.01.17	PIC24E version
 * 
 *****************************************************************************/

#ifndef _CO_TYPES_DEFINED
#define _CO_TYPES_DEFINED

#include "GenericTypeDefs.h"

#define rom const

typedef struct _SID {
	union _SIDH {
		unsigned char byte;
		struct _SIDH_BITS {
			unsigned	SIDH:8;	
			} bits;
		} h;
	union _SIDL {
		unsigned char byte;
		struct _SIDL_BITS {
			unsigned 	:2;
			unsigned	fen:1;
			unsigned	EXIDEN:1;
			unsigned	:1;
			unsigned	SIDL:3;
			} bits;
		} l;
	} __attribute__ ((packed)) CAN_SID;


typedef struct _EID {
	union _EIDUH {
		unsigned char byte;
		struct _EIDUH_BITS {
			unsigned	EIDUH:8;
			} bits;
		} uh;
	union _EIDUL {
		unsigned char byte;
		struct _EIDUL_BITS {
			unsigned 	EIDUL1:2;
			unsigned	fen:1;
			unsigned	EXIDEN:1;
			unsigned	:1;
			unsigned	EIDUL2:3;
			} bits;
		} ul;
	union _EIDH 	{
		unsigned char byte;
		struct _EIDH_BITS {
			unsigned	EIDH:8;
			} bits;
		} h;
	union _EIDL {
		unsigned char byte;
		struct _EIDL_BITS {
			unsigned 	EIDL:8;
			} bits;
		} l;
	}	__attribute__ ((packed)) CAN_EID;



typedef struct {
    struct {           //************ ECAN MESSAGE BUFFER WORD 0 **************
      unsigned char IDE :  1;        // Standard/Extended message bit (0-standard; 1-extended)
      unsigned char SRR :  1;        // Substitute Remote Request bit (0-normal msg; 1-remote transmition rqst)
      unsigned int  SID : 11;        // Standard Identifier bits (SID[10:0])
      unsigned char dummy :  3;        // Don�t care
    	} CxTRBnSID;       // CAN SID register
    struct {           //************ ECAN MESSAGE BUFFER WORD 1 **************
      unsigned int EID17_6 : 12;    // Extended Identifier bits (EID[17:6])
      unsigned char dummy  :  4;    // Don�t care
    	} CxTRBnEID;       // CAN EID register
    struct {           //************ ECAN MESSAGE BUFFER WORD 2 **************
      unsigned char DLC    : 4;      // Data Length Code bits (DLC[3:0])
      unsigned char RB0    : 1;      // Reserved Bit 0 (user must set this bit to '0' per CAN protocol)
      unsigned char dummy  : 3;      // Don�t care
      unsigned char RB1    : 1;      // Reserved Bit 1 (user must set this bit to '0' per CAN protocol)
      unsigned char RTR    : 1;      // Remote Transmission Request bit (0-normal msg; 1-remote transmition rqst)
      unsigned char EID5_0 : 6;      // Extended Identifier bits (EID[5:0])
    	} CxTRBnDLC;       // CAN DLC register
    struct d {          //*** ECAN MESSAGE BUFFER WORD 3, WORD 4, WORD 5, WORD 6
      unsigned char Data[8];    // Data Field Buffer
    	} CxTRBnData;      // CAN DATA registers
    struct {           //************ ECAN MESSAGE BUFFER WORD 7 **************
      unsigned char dummy  : 8;      // Don�t care
      unsigned char FILHIT : 5;      // Filter Hit Code bits (only written by module for receive buffers, unused for transmit buffers)
                       // Encodes number of filter that resulted in writing this buffer.
      unsigned char dummy2 : 3;      // Don�t care
    } CxTRBnSTAT;      // CAN status register
  } __RxTxBuffer;      // CAN rx/tx buffer structure in DMA RAM


typedef union _CID {
	CAN_EID ext;
	CAN_SID	std;
	}	CAN_CID;



typedef union _DLC {
	unsigned char byte;
	struct _DLC_BITS {
		unsigned 	count:4;
		unsigned 	:2;
		unsigned	rtr:1;
		unsigned 	:1;
		} bits;	
	} CAN_DLC;



typedef struct _CAN_MSG {
	CAN_CID cid;
	CAN_DLC dlc;
	unsigned char D0;
	unsigned char D1;
	unsigned char D2;
	unsigned char D3;
	unsigned char D4;
	unsigned char D5;
	unsigned char D6;
	unsigned char D7;
	} __attribute__ ((packed)) CAN_MSG;
	



/* Message handle */
typedef union _CAN_MSGHANDLE {
	unsigned char byte;
	struct _CAN_MSGHANDLE_BITS 	{
		unsigned	h1:1;
		unsigned	h2:1;
		unsigned	h3:1;
		unsigned	h4:1;
		unsigned	h5:1;
		unsigned	h6:1;
		unsigned	h7:1;
		unsigned	h8:1;
		} bits;
	struct _CAN_MSGHANDLE_NIB 	{
		unsigned num:3;		// 8 possible messages
		unsigned grp:2;		// in 4 possible groups
		unsigned hwbuf:3;	// 8 possible buffers
		} msg;
	} CAN_HMSG;



typedef struct _CAN_DATA {
	unsigned char B0;
	unsigned char B1;
	unsigned char B2;
	unsigned char B3;
	unsigned char B4;
	unsigned char B5;
	unsigned char B6;
	unsigned char B7;
	} __attribute__ ((packed)) CAN_DATA;


struct _BITS {
	unsigned char b0:1;
	unsigned char b1:1;
	unsigned char b2:1;
	unsigned char b3:1;
	unsigned char b4:1;
	unsigned char b5:1;
	unsigned char b6:1;
	unsigned char b7:1;
	};

union _UINT8 {								// Array of 8 bits
	unsigned char byte;	
	struct _BITS bits;
	};

union _SINT8 {								// Array of 8 bits
	char byte;	
	struct _BITS bits;
	};

struct _INT16 {
	union _UINT8 B0;
	union _UINT8 B1;
	} __attribute__ ((packed));

struct _INT24 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B3;
	} __attribute__ ((packed));

struct _INT32 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B2;
	union _UINT8 B3;
	} __attribute__ ((packed));


struct _INT40 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B2;
	union _UINT8 B3;
	union _UINT8 B4;
	} __attribute__ ((packed));


struct _INT48 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B2;
	union _UINT8 B3;
	union _UINT8 B4;
	union _UINT8 B5;
	} __attribute__ ((packed));


struct _INT56 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B2;
	union _UINT8 B3;
	union _UINT8 B4;
	union _UINT8 B5;
	union _UINT8 B6;
	} __attribute__ ((packed));


struct _INT64 {
	union _UINT8 B0;
	union _UINT8 B1;
	union _UINT8 B2;
	union _UINT8 B3;
	union _UINT8 B4;
	union _UINT8 B5;
	union _UINT8 B6;
	union _UINT8 B7;
	} __attribute__ ((packed));


union _UINT16 {
	unsigned int word;
	struct _INT16 bytes;
	} __attribute__ ((packed));

union _SINT16 {
	signed int word;
	struct _INT16 bytes;	
	} __attribute__ ((packed));


union _UINT32 {
	unsigned long word;
	struct _INT32 bytes;	
	};

union _SINT32 {
	signed long word;
	struct _INT32 bytes;	
	};


union _UINT40 {
	unsigned long word;
	struct _INT40 bytes;	
	};

union _SINT40 {
	signed long word;
	struct _INT40 bytes;	
	};


union _UINT48 {
	unsigned long word;
	struct _INT48 bytes;	
	};

union _SINT48 {
	signed long word;
	struct _INT48 bytes;	
	};


union _UINT56 {
	unsigned long word;
	struct _INT56 bytes;	
	};

union _SINT56 {
	signed long word;
	struct _INT56 bytes;	
	};


union _UINT64 {
	unsigned long word;
	struct _INT64 bytes;	
	};

union _SINT64 {
	signed long word;
	struct _INT64 bytes;	
	};


union _FLOAT {
	double word;	
	};



//typedef enum _BOOL { FALSE = 0, TRUE } BOOLEAN;		// Boolean
typedef union _SINT8 INTEGER8;						// Signed 8-bit number
typedef union _UINT8 UNSIGNED8;						// Unsigned 8-bit number
typedef union _SINT16 INTEGER16;					// Signed 16-bit number
typedef union _UINT16 UNSIGNED16;					// Unsigned 16-bit number
typedef union _SINT24 INTEGER24;					// Signed 24-bit number
typedef union _UINT24 UNSIGNED24;					// Unsigned 24-bit number
typedef union _SINT32 INTEGER32;					// Signed 32-bit number
typedef union _UINT32 UNSIGNED32;					// Unsigned 32-bit number
typedef union _SINT40 INTEGER40;					// Signed 40-bit number
typedef union _UINT40 UNSIGNED40;					// Unsigned 40-bit number
typedef union _SINT48 INTEGER48;					// Signed 48-bit number
typedef union _UINT48 UNSIGNED48;					// Unsigned 48-bit number
typedef union _SINT56 INTEGER56;					// Signed 56-bit number
typedef union _UINT56 UNSIGNED56;					// Unsigned 56-bit number
typedef union _SINT64 INTEGER64;					// Signed 64-bit number
typedef union _UINT64 UNSIGNED64;					// Unsigned 64-bit number
typedef union _FLOAT FLOAT;
typedef unsigned char * STRING;						// String data type



//  Data types used for block copy 
typedef struct _DATA_BLOCK_4 {
	unsigned char bytes[4];
	} _DATA4;

typedef struct __DATA7 {
	unsigned char myDat[7];
	} _DATA7;

typedef struct __DATA8 {
	unsigned char myDat[8];
	} _DATA8;

#endif

