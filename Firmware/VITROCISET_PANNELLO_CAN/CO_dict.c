
/*****************************************************************************
 *
 * Microchip CANopen Stack (Dictionary Services)
 *
 *****************************************************************************
 * FileName:        CO_DICT.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * Dictionary services.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.01.17	PIC24 version
 * 									07.17  risistemato dictionary
 *****************************************************************************/

#if defined(__18CXX)
#include	"CO_TYPES.H"			// Standard types
#else
#include	"CO_TYPES24.H"
#define rom const
#endif

#include	"CO_ABERR.H"			// Abort types
#include	"CO_DICT.H"				// Dictionary header
#include	"CO_MEMIO.H"			// Memory IO
#include	"CO_MFTR.h"			// Manufacturer specific objects
//#include	"CO_STD.h"			// CANopen standard objects
#include	"CO_pdo1.h"			// CANopen standard objects
#include	"CO_pdo2.h"			// CANopen standard objects


// Params used by the dictionary
DICT_PARAM	uDict;



const rom unsigned char	__dummy[4] = {0,0,0,0};




/*********************************************************************
 * Function:        void _CO_DictObjectRead(void)
 *
 * PreCondition:    _CO_DictObjectDecode() must have been called to 
 *					fill in the object structure.
 *
 * Input:       	none		
 *                  
 * Output:      	uDict.ret
 *
 * Side Effects:    none
 *
 * Overview:        Read the object referenced by uDict.obj.
 *
 * Note:            
 ********************************************************************/
BYTE CO_DictObjectRead(DICT_OBJ *uDictObj) {

	uDict.obj=uDictObj;

	// If the object is valid, control code must be something other than 0
	if(uDictObj->ctl) {	
		// Process any functionally defined objects
		if(uDictObj->ctl & FDEF_BIT) {
			uDict.ret = E_SUCCESS;
			uDict.cmd = DICT_OBJ_READ;
			uDictObj->p.pFunc();	
			return uDict.ret;
			}
		else {

			// Decode the type of object
			switch(uDictObj->ctl & ACCESS_BITS) {
				case CONST:
					//Copy ROM to RAM, uDict.obj->reqLen specifies the amount
					CO_MEMIO_CopySram(uDictObj->p.pRAM + uDictObj->reqOffst, uDictObj->pReqBuf, uDictObj->reqLen);
					break;
	
				case RO:
				case RW:
					//Copy RAM to RAM, uDict.obj->reqLen specifies the amount
					CO_MEMIO_CopySram(uDictObj->p.pRAM + uDictObj->reqOffst, uDictObj->pReqBuf, uDictObj->reqLen);
					break;
					
	//			case RW_EE:
	//			case RO_EE:
	//				break;
							
				default:
					// Error, cannot read object
					uDict.ret = E_CANNOT_READ;
					return uDict.ret;
				}
			}
		}
	
	uDict.ret = E_SUCCESS;

	return uDict.ret;
	}



BYTE CO_DictObjectWrite(DICT_OBJ *uDictObj) {

	uDict.obj=uDictObj;

	// If the object is found
	if(uDictObj->ctl) {
		if(uDictObj->ctl & FDEF_BIT) {
			uDict.ret = E_SUCCESS;
			uDict.cmd = DICT_OBJ_WRITE;
			uDictObj->p.pFunc();	
			return uDict.ret;
			}
		else {

			// Decode the type of object
			switch(uDictObj->ctl & ACCESS_BITS) {							
				case RW:
				case WO:
					//Copy RAM to RAM, uDict.obj->reqLen specifies the amount
					CO_MEMIO_CopySram(uDictObj->pReqBuf, uDictObj->p.pRAM, uDictObj->reqLen);
					break;
				
	//			case RW_EE:
	//			case WO_EE:
	//				break;
							
				default:
					// Error, write not allowed
					uDict.ret = E_CANNOT_WRITE;
					return uDict.ret;
				}
			}
		}
	
	uDict.ret = E_SUCCESS;
	return uDict.ret;
	}


/* Decode the object*/
BYTE CO_DictObjectDecode(DICT_OBJ *uDictObj) {
	rom unsigned char * _pTmpDBase;
	unsigned char 		_tDBaseLen;
	unsigned char		_uDictTemp[4];
	MULTIPLEXOR			_tMplex;

	uDict.obj=uDictObj;

	// Copy the multiplexor to a local storage area
	_tMplex = *(MULTIPLEXOR *)(&(uDictObj->index));

	switch(_tMplex.index.bytes.B1.byte & 0xF0) {		
		case 0x00:		// Data types
			_pTmpDBase = (rom unsigned char *)_db_objects;
			_tDBaseLen = sizeof_db_objects;
			break;
		
		case 0x20:		// Manufacturer specific
			_pTmpDBase = (rom unsigned char *)_db_manufacturer_g1;
			_tDBaseLen = sizeof_db_manufacturer_g1;
			break;
		case 0x30:
			_pTmpDBase = (rom unsigned char *)_db_manufacturer_g2;
			_tDBaseLen = sizeof_db_manufacturer_g2;
			break;
		case 0x40:
			_pTmpDBase = (rom unsigned char *)_db_manufacturer_g3;
			_tDBaseLen = sizeof_db_manufacturer_g3;
			break;
//		#ifdef _db_manufacturer_g4		// non va perch� non sono nei suoi include...
//#if !defined(USA_BOOTLOADER) && !defined(IS_BOOTLOADER) 		// facciam cos�
		case 0x50:
			_pTmpDBase = (rom unsigned char *)_db_manufacturer_g4;
			_tDBaseLen = sizeof_db_manufacturer_g4;
			break;
//		#endif
			
		case 0x60:		// Standard
			_pTmpDBase = (rom unsigned char *)_db_standard_g1;
			_tDBaseLen = sizeof_db_standard_g1;
			break;
		case 0x70:
			_pTmpDBase = (rom unsigned char *)_db_standard_g2;
			_tDBaseLen = sizeof_db_standard_g2;
			break;
		#ifdef _db_standard_g3
		case 0x80:
			_pTmpDBase = (rom unsigned char *)_db_standard_g3;
			_tDBaseLen = sizeof_db_standard_g3;
			break;
		#endif
		#ifdef _db_standard_g4
		case 0x90:
			_pTmpDBase = (rom unsigned char *)_db_standard_g4;
			_tDBaseLen = sizeof_db_standard_g4;
			break;
		#endif
					
		case 0x10:
			switch (_tMplex.index.bytes.B1.byte & 0x0F)	{
				case 0x00:		// Device specific information
					_pTmpDBase = (rom unsigned char *)_db_device;
					_tDBaseLen = sizeof_db_device;
					break;
					
				case 0x02:		// SDO Server/Client
					_pTmpDBase = (rom unsigned char *)_db_sdo;
					_tDBaseLen = sizeof_db_sdo;
					break;
					
				case 0x04:		// PDO Receive Comm
// nel bootloader ho messo ifdef anche sul primo PDO!
#if !defined(USA_BOOTLOADER) && !defined(IS_BOOTLOADER) 		// facciam cos�
//					#ifdef _db_pdo1_rx_comm			// non va perch� non sono nei suoi include...
					if(_tMplex.index.bytes.B0.byte == 0x00) {
						_pTmpDBase = (rom unsigned char *)_db_pdo1_rx_comm;
						_tDBaseLen = sizeof_db_pdo1_rx_comm;
					}
					else 
					#endif
#ifdef USE_PDO_2
					if(_tMplex.index.bytes.B0.byte == 0x01)	{
						_pTmpDBase = (rom unsigned char *)_db_pdo2_rx_comm;
						_tDBaseLen = sizeof_db_pdo2_rx_comm;
					}
					else
					#endif
#ifdef USE_PDO_3
					if(_tMplex.index.bytes.B0.byte == 0x02)	{
						_pTmpDBase = (rom unsigned char *)_db_pdo3_rx_comm;
						_tDBaseLen = sizeof_db_pdo3_rx_comm;
						}
					else 
					#endif
#ifdef USE_PDO_4
					if(_tMplex.index.bytes.B0.byte == 0x03) {
						_pTmpDBase = (rom unsigned char *)_db_pdo4_rx_comm;
						_tDBaseLen = sizeof_db_pdo4_rx_comm;
					}
					else 
					#endif 
					{
						// Index not found in database
						uDict.ret = E_OBJ_NOT_FOUND;
						return uDict.ret;
					}
					break;
					
					
				case 0x06:		// PDO Receive Map					
#if !defined(USA_BOOTLOADER) && !defined(IS_BOOTLOADER) 		// facciam cos�
//					#ifdef _db_pdo1_rx_map			// non va perch� non sono nei suoi include...
					if(_tMplex.index.bytes.B0.byte == 0x00)	{
						_pTmpDBase = (rom unsigned char *)_db_pdo1_rx_map;
						_tDBaseLen = sizeof_db_pdo1_rx_map;
						}
					else 
					#endif
#ifdef USE_PDO_2
					if(_tMplex.index.bytes.B0.byte == 0x01) {
						_pTmpDBase = (rom unsigned char *)_db_pdo2_rx_map;
						_tDBaseLen = sizeof_db_pdo2_rx_map;
					}
					else
					#endif
#ifdef USE_PDO_3
					if(_tMplex.index.bytes.B0.byte == 0x02) {
						_pTmpDBase = (rom unsigned char *)_db_pdo3_rx_map;
						_tDBaseLen = sizeof_db_pdo3_rx_map;
					}
					else 
					#endif
#ifdef USE_PDO_4
					if(_tMplex.index.bytes.B0.byte == 0x03)
					{
						_pTmpDBase = (rom unsigned char *)_db_pdo4_rx_map;
						_tDBaseLen = sizeof_db_pdo4_rx_map;
					}
					else  
					#endif
					{
						// Index not found in database
						uDict.ret = E_OBJ_NOT_FOUND;
						return uDict.ret;
					}
					break;
					
				case 0x08:		// PDO Transmit Comm				
#if !defined(USA_BOOTLOADER) && !defined(IS_BOOTLOADER) 		// facciam cos�
//					#ifdef _db_pdo1_tx_comm
					if(_tMplex.index.bytes.B0.byte == 0x00)	{
						_pTmpDBase = (rom unsigned char *)_db_pdo1_tx_comm;
						_tDBaseLen = sizeof_db_pdo1_tx_comm;
						}
					else 
					#endif
#ifdef USE_PDO_2
					if(_tMplex.index.bytes.B0.byte == 0x01) {
						_pTmpDBase = (rom unsigned char *)_db_pdo2_tx_comm;
						_tDBaseLen = sizeof_db_pdo2_tx_comm;
					}
					else
					#endif
#ifdef USE_PDO_3
					if(_tMplex.index.bytes.B0.byte == 0x02) {
						_pTmpDBase = (rom unsigned char *)_db_pdo3_tx_comm;
						_tDBaseLen = sizeof_db_pdo3_tx_comm;
					}
					else 
					#endif
#ifdef USE_PDO_4
					if(_tMplex.index.bytes.B0.byte == 0x03) {
						_pTmpDBase = (rom unsigned char *)_db_pdo4_tx_comm;
						_tDBaseLen = sizeof_db_pdo4_tx_comm;
					}
					else  
					#endif
					{
						// Index not found in database
						uDict.ret = E_OBJ_NOT_FOUND;
						return uDict.ret;
					}
					break;
				
				
				case 0x0A:		// PDO Transmit Map					
#if !defined(USA_BOOTLOADER) && !defined(IS_BOOTLOADER) 		// facciam cos�
//					#ifdef _db_pdo1_tx_map
					if(_tMplex.index.bytes.B0.byte == 0x00)	{
						_pTmpDBase = (rom unsigned char *)_db_pdo1_tx_map;
						_tDBaseLen = sizeof_db_pdo1_tx_map;
						}
					else 
					#endif
#ifdef USE_PDO_2
					if(_tMplex.index.bytes.B0.byte == 0x01) {
						_pTmpDBase = (rom unsigned char *)_db_pdo2_tx_map;
						_tDBaseLen = sizeof_db_pdo2_tx_map;
					}
					else
					#endif
#ifdef USE_PDO_3
					if(_tMplex.index.bytes.B0.byte == 0x02) {
						_pTmpDBase = (rom unsigned char *)_db_pdo3_tx_map;
						_tDBaseLen = sizeof_db_pdo3_tx_map;
					}
					else 
					#endif
#ifdef USE_PDO_4
					if(_tMplex.index.bytes.B0.byte == 0x03) {
						_pTmpDBase = (rom unsigned char *)_db_pdo4_tx_map;
						_tDBaseLen = sizeof_db_pdo4_tx_map;
					}
					else  
					#endif
					{
						// Index not found in database
						uDict.ret = E_OBJ_NOT_FOUND;
						return uDict.ret;
					}
					break;
					
					
//				case 0x05:
//				case 0x07:
//				case 0x09:
//				case 0x0B:
//					_pTmpDBase = (rom unsigned char *)_db_pdo_tx_map;
//					_tDBaseLen = sizeof(_db_pdo_tx_map)/sizeof(DICT_OBJECT_TEMPLATE);
//					break;
				
				default:
					// Index not found in database
					uDict.ret = E_OBJ_NOT_FOUND;
					return uDict.ret;
				}
			break;

		default:
			// Index not found in database
			uDict.ret = E_OBJ_NOT_FOUND;
			return uDict.ret;
			break;
		}

	
	// Adjust the status
	uDict.ret = E_OBJ_NOT_FOUND;
	
	// Copy the index and sub-index to local memory
	*(_DATA4 *)(&_uDictTemp[0]) = *(rom _DATA4 *)_pTmpDBase;

	// Scan the database and load the pointer
	while(_tDBaseLen) {	

		// Match the index 
		if((_uDictTemp[1] == _tMplex.index.bytes.B1.byte) &&
			(_uDictTemp[0] == _tMplex.index.bytes.B0.byte))	{
			// Adjust the status
			uDict.ret = E_SUBINDEX_NOT_FOUND;
													
			// If the sub index matches then return success code
			if((_uDictTemp[2] == _tMplex.sindex.byte) ||	(_uDictTemp[3] & FSUB_BIT))	{
				// Copy control information
		    	*((DICT_OBJECT_TEMPLATE *)(&(uDictObj->index))) = *((rom DICT_OBJECT_TEMPLATE *)(_pTmpDBase));
		    				    	
		    	// If functionally defined sub-index, copy sub-index from request
		    	if(_uDictTemp[3] & FSUB_BIT) 
						uDictObj->subindex = _tMplex.sindex.byte;
						
				// If function specific then call the app function for 
				// read/write, mapping, and length info
				if(_uDictTemp[3] & FDEF_BIT) {	//if (uDict.obj->ctl & FDEF_BIT)
					uDict.ret = E_SUCCESS;
					uDict.cmd = DICT_OBJ_INFO;
					uDictObj->p.pFunc();	
					return uDict.ret;
					}
				else {
					uDict.ret = E_SUCCESS;
					return uDict.ret;
					}
				}
			}

		// Adjust the pointer
		_pTmpDBase += sizeof(DICT_OBJECT_TEMPLATE);
		
		_tDBaseLen--;
		
		// Copy the index and sub-index to local memory
		*(_DATA4 *)(&(_uDictTemp[0])) = *(rom _DATA4 *)_pTmpDBase;
		}

	return uDict.ret;
	}



