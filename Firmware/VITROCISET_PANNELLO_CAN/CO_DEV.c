/*****************************************************************************
 *
 * Microchip CANopen Stack (Device Info)
 *
 *****************************************************************************
 * FileName:        CO_DEV.C
 * Dependencies:    
 * Processor:       PIC24F with CAN
 * Compiler:       	C30 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * This file contains many of the standard objects defined by CANopen.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.1.17		PIC24E version
 * 
 *****************************************************************************/


#include	"CO_TYPES24.H"
#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include	"CO_main24.H"
#endif



// CANopen object 0x1000
//rom unsigned long rCO_DevType = 			0x8934AL; boh?
//I/O Functionality:       
//16th Bit: Digital input
//17th Bit: Digital output
//18th Bit: Analogue input
//19th Bit: Analogue output



// CANopen object 0x1008
// CANopen object 0x1018
// CANopen object 0x1009
// CANopen object 0x100A
const rom unsigned char rCO_DevIdentityIndx =            4;
const rom unsigned long rCO_DevVendorID =                 0x115fL;
#ifdef IS_BOOTLOADER
const rom unsigned long rCO_DevType =        0x191L;
const rom char rCO_DevName[27] =             "K-tronic CANopen Bootloader";
const rom unsigned char rCO_DevHardwareVer[] =      "V1.0";
const rom unsigned char rCO_DevSoftwareVer[] =        "V1.0";
const rom unsigned long rCO_DevProductCode =          0x00000001L;
const rom unsigned long rCO_DevRevNo =                0x00000001L;
#else
const rom unsigned long rCO_DevType =        0xF0191L;
const rom char rCO_DevName[26] =             "K-tronic VitroCiset Device";
const rom unsigned char rCO_DevHardwareVer[] =      "V1.0";
const rom unsigned char rCO_DevSoftwareVer[] =        "V1.3";
const rom unsigned long rCO_DevProductCode =          0x00000002L;
const rom unsigned long rCO_DevRevNo =                0x00000001L;
//const rom unsigned long rCO_DevSerialNo =           0x00000000L; v. configParms
#endif

 





// CANopen object 0x1001
unsigned char uCO_DevErrReg;
// 1003
unsigned long uCO_DevErrReg2[8];		// pag. 88 del doc grande

// CANopen object 0x1002
unsigned long uCO_DevManufacturerStatReg;

//1011
const unsigned char rCO_parmIndx1=1,rCO_parmIndx2=2,rCO_parmIndx3=3,rCO_parmIndx4=4,rCO_parmIndx5=5,
	rCO_parmIndx7=7,rCO_parmIndx8=8;

