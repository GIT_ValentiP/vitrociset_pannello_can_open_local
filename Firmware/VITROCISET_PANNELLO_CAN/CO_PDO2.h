#include "co_pdo.h"




#ifdef USE_PDO_2
void CO_COMM_PDO2_Create(WORD,WORD);
void CO_COMM_PDO2_Open(BYTE, BYTE);
void CO_COMM_PDO2_Close(void);
void CO_COMM_RPDO2_RXEvent(unsigned char);
void CO_COMM_RPDO2_TXEvent(BYTE);
void CO_COMM_TPDO2_RXEvent(unsigned char);
void CO_COMM_TPDO2_TXEvent(BYTE);

void CO_COMM_RPDO2_COBIDAccessEvent(void);
void CO_COMM_RPDO2_TypeAccessEvent(void);

void CO_COMM_TPDO2_COBIDAccessEvent(void);
void CO_COMM_TPDO2_TypeAccessEvent(void);
void CO_COMM_TPDO2_ITimeAccessEvent(void);
void CO_COMM_TPDO2_ETimeAccessEvent(void);

void CO_PDO2LSTimerEvent(void);
void CO_RPDO2TXFinEvent(void);
void CO_TPDO2TXFinEvent(void);

#endif




