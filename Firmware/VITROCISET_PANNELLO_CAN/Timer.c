/*****************************************************************************
 *
 * Timer 
 *
 *****************************************************************************
 * FileName:        Timer.c
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.10.02 or higher
 * Linker:          MPLINK 03.20.01 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *

 *
 * This is a simple timer function used within the DeviceNet Stack for
 * demonstration.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			04/28/03	...	
 * Cinzia Greggio 	25.1.17	
 * 
 *****************************************************************************/



#include	<p24exxxx.H>			// 
#include	<timer.H>			// 
#include	"mytimer.H"			// 



#define	TIMER_PERIOD_MS		8					// Period in milliseconds
#define	TIMER_FOSC			70000000L			// Frequency of the processor clock
#define	TIMER_PRESCALE		3					// The desired prescale


#if 	TIMER_PRESCALE == 0
	#define	TIMER_PRESCALE_VALUE	1
#elif 	TIMER_PRESCALE == 1
	#define	TIMER_PRESCALE_VALUE	8
#elif	TIMER_PRESCALE == 2
	#define	TIMER_PRESCALE_VALUE	64
#elif	TIMER_PRESCALE == 3
	#define	TIMER_PRESCALE_VALUE	256
#else
	#define	TIMER_PRESCALE_VALUE	1
	#define	TIMER_PRESCALE	8
#endif



void TimerInit(void) {	



	OpenTimer3(T3_ON & T3_IDLE_CON & T3_GATE_OFF & T3_PS_1_64 & T3_SOURCE_INT,
		1100  /* ~1mS, 4.7.17 */ );		//(il timer va a 16uSec su GB, 4 su GU)


}



unsigned char TimerIsOverflowEvent(void) {

//	unsigned int temp;

	if(IFS0bits.T3IF) {
//		temp = 65536L - ((TIMER_PERIOD_MS * TIMER_FOSC) / 1000L / 4 / TIMER_PRESCALE_VALUE);
//		TMR3 = 0;
		
		IFS0bits.T3IF = 0;
		return 1;
		}
	return 0;



	}

