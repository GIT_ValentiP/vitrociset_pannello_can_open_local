/*****************************************************************************
 *
 * Microchip CANopen Stack (Network Management and Communications)
 *
 *****************************************************************************
 * FileName:        CO_NMT.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 *
 * This is the network management object.
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio 	24.3.17		PIC24E
 * 
 *****************************************************************************/



// Global definitions

#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services
#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include	"CO_main24.H"
#endif

#include	"CO_COMM.H"				// Object


// Event functions for reset requests
void CO_NMTStateChangeEvent(void);
void CO_NMTResetEvent(void);
void CO_NMTAppResetRequest(void);

// Handle for NMT
unsigned char _hNMT;



void CO_COMM_NMT_Open(void) {	

	// Open a receive message endpoint in the driver
	_hNMT=CANOpenMessage((COMM_MSGGRP_NETCTL) | COMM_NETCTL_NMT, 0x00L);
	
	// Enable NMT
	if(_hNMT) 
		COMM_NETCTL_NMT_EN = 1;
	}



void CO_COMM_NMT_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_hNMT);
	COMM_NETCTL_NMT_EN = 0;
	}


void CO_COMM_NMT_RXEvent(unsigned char b) {	

// funzia 27.3.17	LATDbits.LATD9^=1;

	// If the length of the data is 2 then continue
	if(mCANGetDataLen(b) == 2)	{
		// Check the received Node ID or broadcast
		if((mCANGetDataByte1(b) == configParms.nodeID) || (!mCANGetDataByte1(b))) {
			// OPP, STOP, PRE
			//  x    x    0		Not started, will never enter this function
			//  0    0    1		Pre-operational
			//	0    1    1		Stopped from pre-operational
			//  1    0    1		Operational
			//  1    1    1		Stopped from operational
			
			// Decode the network request and execute
			switch(mCANGetDataByte0(b))	{
				case 1:		// Start Node
					COMM_STATE_OPER = 1;
					COMM_STATE_STOP = 0;
					CO_NMTStateChangeEvent();  // Notify the app 
					break;
				case 2:		// Stop Node
					COMM_STATE_STOP = 1;
					CO_NMTStateChangeEvent();  // Notify the app 
					break;
				case 128:	// Pre-operational
					COMM_STATE_OPER = 0;
					COMM_STATE_STOP = 0;					
					CO_NMTStateChangeEvent();  // Notify the app 
					break;
					
				case 129:		// Reset node
					CO_NMTResetEvent();		// Notify the app
					break;
							
				case 130:		// Reset communications	
					CO_NMTAppResetRequest(); 	// Notify the app
					
					// Reset Communications
					CO_COMMResetEventManager();
					break;
					
				}	// Unknown requests, ignore
			}
		}
	}


