#ifndef __CODEFS24_H_INCLUDED
#define __CODEFS24_H_INCLUDED




#define CAN_BITRATE0_BRGCON1	0x005B		// 250Kb @140MHz
#define CAN_BITRATE0_BRGCON2	0x02D2

#define	CAN_BITRATE1			1
#define CAN_BITRATE1_BRGCON1	0x0041
#define CAN_BITRATE1_BRGCON2	0x07BA

#define	CAN_BITRATE2			1
#define CAN_BITRATE2_BRGCON1	0x0041
#define CAN_BITRATE2_BRGCON2	0x07BA

#define	CAN_BITRATE3			1
#define CAN_BITRATE3_BRGCON1	0x0041
#define CAN_BITRATE3_BRGCON2	0x07BA

#define	CAN_BITRATE4			1
#define CAN_BITRATE4_BRGCON1	0x005B			// testato 22.3.17
#define CAN_BITRATE4_BRGCON2	0x02D2

#define	CAN_BITRATE5			1
#define CAN_BITRATE5_BRGCON1	0x0041
#define CAN_BITRATE5_BRGCON2	0x07BA

#define	CAN_BITRATE6			1
#define CAN_BITRATE6_BRGCON1	0x0041
#define CAN_BITRATE6_BRGCON2	0x07BA

#define	CAN_BITRATE7			1
#define CAN_BITRATE7_BRGCON1	0x0041
#define CAN_BITRATE7_BRGCON2	0x07BA

#define	CAN_BITRATE8			1
#define CAN_BITRATE8_BRGCON1	0x0041
#define CAN_BITRATE8_BRGCON2	0x07BA

#define BASE_BUFFER_RX 4

#ifdef IS_BOOTLOADER
#else
#define USE_PDO_1	 1				// tasti led locali
#endif


#ifdef USE_PDO_1
#define _PDO_ADD_1 1
#else
#define _PDO_ADD_1 0
#endif
#ifdef USE_PDO_2
#define _PDO_ADD_2 1
#else
#define _PDO_ADD_2 0
#endif
#ifdef USE_PDO_3
#define _PDO_ADD_3 1
#else
#define _PDO_ADD_3 0
#endif


#define CO_NUM_OF_PDO	 (_PDO_ADD_1+_PDO_ADD_2+_PDO_ADD_3)




//#define CAN_MAX_RCV_ENDP	7		// almeno 5... NMT, SYNC, SDO, PDO1 PDO2 + PDO3&4; 
//  NO 6! con NMTE; saranno 7 con TIME
#define CAN_MAX_RCV_ENDP	(4+(CO_NUM_OF_PDO*2))

#define	CO_SPEED_UP_CODE	0

#define CO_TICK_PERIOD		1		// ossia 1ms

#define	CO_SDO1_MAX_RX_BUF		100			// 80 x lcd...
#define CO_SDO1_MAX_SEG_TIME	500		// Time in milliseconds 


#endif

