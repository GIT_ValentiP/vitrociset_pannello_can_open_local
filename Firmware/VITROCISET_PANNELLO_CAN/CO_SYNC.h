/*****************************************************************************
 *
 * Microchip CANopen Stack (SYNC Object)
 *
 *****************************************************************************
 * FileName:        CO_SYNC.H
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * CinziaG 					23.3.2017		PIC24E 
 *****************************************************************************/



#ifndef	__CO_SYNC_H
#define	__CO_SYNC_H


// Object 1005h, this is also defined functionally
// This is also initialized by the app at startup since it could be 
// initialized from non-volitile memory.
extern UNSIGNED32 _uSYNC_COBID;

void CO_COMM_SYNC_Open(void);

void CO_COMM_SYNC_Close(void);

void CO_COMM_SYNC_RXEvent(unsigned char);

void CO_COMM_SYNC_COBIDAccessEvent(void);

#define	mSYNC_SetCOBID(SYNC_COB)	_uSYNC_COBID.word = SYNC_COB;

#define mSYNC_GetCOBID()			SYNC_COB


#endif	//__CO_SYNC_H


