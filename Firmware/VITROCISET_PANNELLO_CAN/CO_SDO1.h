/*****************************************************************************
 *
 * Microchip CANopen Stack (The Default SDO)
 *
 *****************************************************************************
 * FileName:        CO_SDO1.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		20.7.17		rewrite, less idiotic :)
 * 
 *****************************************************************************/



#ifndef	__CO_SDO1_H
#define	__CO_SDO1_H


union _SDO_CTL {
	unsigned char byte;
	struct _SDO_INITIATE_CTL	{
		unsigned s:1;
		unsigned e:1;
		unsigned n:2;
		unsigned x:1;
		unsigned cmd:3;	
		} __attribute__ ((packed)) ictl;
	struct _SDO_REGULAR_CTL	{
		unsigned c:1;
		unsigned n:3;
		unsigned t:1;
		unsigned cmd:3;
		} __attribute__ ((packed)) rctl;
	struct _SDO_RESPONSE {
		unsigned x:4;
		unsigned t:1;
		unsigned cmd:3;
		} __attribute__ ((packed)) rsp;
	};

union _SDO_STATE {
	unsigned int word;
	struct _SDO_STATE_BITS	{
//LSB first!
		unsigned :3;
		unsigned ntime:1;
		unsigned tog:1;
		unsigned start:1;
		unsigned dnld:1;	
		unsigned abrt:1;
		unsigned sizeindicated:1;		// GC per block, 6.7.17
		unsigned crc:1;				// idem
		unsigned upld2:1;			// idem
		unsigned dnld2:1;			// idem
		} __attribute__ ((packed)) bits;
	};


extern const rom unsigned char _uSDO1COMMIndx;

void CO_COMM_SDO1_CS_COBIDAccessEvent(void);

void CO_COMM_SDO1_SC_COBIDAccessEvent(void);

void CO_COMM_SDO1_Open(void);

void CO_COMM_SDO1_Close(void);

void CO_COMM_SDO1_LSTimerEvent(void);

void CO_COMM_SDO1_TXEvent(BYTE);

void CO_COMM_SDO1_RXEvent(unsigned char);

void CO_COMM_SDO1_TXFinEvent();


// per bootloader...
void CO_COMM_SDO1_BootloaderCommand(void);
void CO_COMM_SDO1_BootloaderData(void);
void CO_COMM_SDO1_BootloaderReadVersion(void);
void CO_COMM_SDO1_BootloaderReadDevice(void);
void CO_COMM_SDO1_BootloaderKey(void);
void CO_COMM_SerialNumberKey(void);
void CO_COMM_SerialNumber(void);

#endif	//__CO_SDO1_H



