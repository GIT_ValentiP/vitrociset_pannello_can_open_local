/*****************************************************************************
 *
 * Microchip CANopen Stack (Process Data Objects)
 *
 *****************************************************************************
 * FileName:        CO_PDO.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Greggio Dario		30.10.17		PIC24E version
 * 
 *****************************************************************************/


#ifndef	__CO_PDO_H
#define	__CO_PDO_H

#define	RTR_DIS	bytes.B1.bits.b2
#define STD_DIS	bytes.B1.bits.b3
#define PDO_DIS	bytes.B1.bits.b4

typedef struct __PDO_BUFFERS {
	struct __RPDO_BUF	{
		unsigned char len;
		unsigned char *buf;
		} RPDO;
	struct __TPDO_BUF	{
		unsigned char len;
		unsigned char *buf;
		} TPDO;
	} _PDOBUF;
// OKKIO a packed!! nel caso


extern UNSIGNED32		uRPDO1Comm;		// RPD1 communication setting
extern UNSIGNED32		uTPDO1Comm;		// TPD1 communication setting
extern _PDOBUF 			_uPDO1;
extern unsigned char 	_uRPDO1Handles,_uTPDO1Handles;

#ifdef USE_PDO_2
extern UNSIGNED32		uRPDO2Comm;		// RPD2 communication setting
extern UNSIGNED32		uTPDO2Comm;		// TPD2 communication setting
extern _PDOBUF 			_uPDO2;
extern unsigned char 	_uRPDO2Handles,_uTPDO2Handles;
#endif

#ifdef USE_PDO_3
extern UNSIGNED32		uRPDO3Comm;		// RPD3 communication setting
extern UNSIGNED32		uTPDO3Comm;		// TPD3 communication setting
extern _PDOBUF 			_uPDO3;
extern unsigned char 	_uRPDO3Handles,_uTPDO3Handles;
#endif

#ifdef USE_PDO_4
extern UNSIGNED32		uRPDO4Comm;		// RPD4 communication setting
extern UNSIGNED32		uTPDO4Comm;		// TPD4 communication setting
extern _PDOBUF 			_uPDO4;
extern unsigned char 	_uPDO4Handles;
#endif

#ifdef USE_PDO_5
extern UNSIGNED32		uRPDOComm;		// RPD5 communication setting
extern UNSIGNED32		uTPDOComm;		// TPD5 communication setting
extern _PDOBUF 			_uPDO5;
extern unsigned char 	_uPDO5Handles;
#endif

#ifdef USE_PDO_6
extern UNSIGNED32		uRPDO6Comm;		// RPD6 communication setting
extern UNSIGNED32		uTPDO6Comm;		// TPD6 communication setting
extern _PDOBUF 			_uPDO6;
extern unsigned char 	_uPDO6Handles;
#endif

//extern unsigned char uDemoSyncSet[2];				// Internal TPDO type control
extern unsigned char uDemoSyncCount[2];			// Counter for synchronous types

void CO_COMM_PDO1_Create(WORD,WORD);

void CO_COMM_PDO1_Open(BYTE, BYTE);

void CO_COMM_PDO1_Close(void);

void CO_COMM_RPDO1_RXEvent(unsigned char);
void CO_COMM_TPDO1_RXEvent(unsigned char);

void CO_COMM_RPDO1_TXEvent(BYTE);
void CO_COMM_TPDO1_TXEvent(BYTE);


#define	mRPDOOpen(PDOn)				CO_COMM_PDO##PDOn##_Open(1,1)

#define mRPDOIsOpen(PDOn)			COMM_RPDO_##PDOn##_EN

#define mRPDOClose(PDOn)			CO_COMM_PDO##PDOn##_Close()

#define	mRPDOIsGetRdy(PDOn)			(COMM_RPDO_##PDOn##_RF)

#define	mRPDORead(PDOn)				COMM_RPDO_##PDOn##_RF = 0

#define	mTPDORead(PDOn)				COMM_TPDO_##PDOn##_RF = 0

#define mRPDOSetCOB(PDOn, rpdoCOB)	uRPDO##PDOn##Comm.word = rpdoCOB

#define mRPDOGetCOB(PDOn)			uRPDO##PDOn##Comm.word

#define	mRPDOGetLen(PDOn)			(_uPDO##PDOn##.RPDO.len)

#define	mRPDOGetRTR(PDOn)			(_uPDO##PDOn##.RPDO.len & 0x40)

#define	mRPDOGetRxPtr(PDOn)			(_uPDO##PDOn.RPDO.buf)

#define	mRPDOSetRxPtr(PDOn, pRXBUF)	_uPDO##PDOn.RPDO.buf = pRXBUF

#define mTPDOOpen(PDOn)				COMM_TPDO_##PDOn##_EN = 1

#define mTPDOIsOpen(PDOn)			COMM_TPDO_##PDOn##_EN

#define mTPDOClose(PDOn)			COMM_TPDO_##PDOn##_EN = 0

#define	mTPDOIsPutRdy(PDOn)			(!COMM_TPDO_##PDOn##_TF)

#define	mTPDOWritten(PDOn)			COMM_TPDO_##PDOn##_TF = 1

#define	mRPDOWritten(PDOn)			COMM_RPDO_##PDOn##_TF = 1

#define mTPDOSetCOB(PDOn, tpdoCOB)	uTPDO##PDOn##Comm.word = tpdoCOB

#define mTPDOGetCOB(PDOn)			uTPDO##PDOn##Comm.word

#define	mTPDOSetLen(PDOn, length)	_uPDO##PDOn.TPDO.len = length

#define	mTPDOSetRTR(PDOn)			_uPDO##PDOn.TPDO.len |= 0x40

#define	mTPDOGetTxPtr(PDOn)			(_uPDO##PDOn.TPDO.buf)

#define	mTPDOSetTxPtr(PDOn, PTXBUF)	_uPDO##PDOn.TPDO.buf = PTXBUF



#endif	//__CO_PDO_H

