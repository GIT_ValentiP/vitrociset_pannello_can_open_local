#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=CO_COMM.c CO_DEV.c CO_dict.c CO_MAIN.c CO_NMT.c CO_NMTE.c CO_PDO1.c CO_PDO2.c CO_PDO3.c CO_PDO.c CO_SDO1.c CO_SYNC.c CO_TOOLS.c DemoObj.c exttst.c main.c Timer.c CO_CANDRV24.c CO_MEMIO.c interrupt.c swi2c.c co_dict_cust.c co_emcy.c CO_TIME.c lcd_text.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/CO_COMM.o ${OBJECTDIR}/CO_DEV.o ${OBJECTDIR}/CO_dict.o ${OBJECTDIR}/CO_MAIN.o ${OBJECTDIR}/CO_NMT.o ${OBJECTDIR}/CO_NMTE.o ${OBJECTDIR}/CO_PDO1.o ${OBJECTDIR}/CO_PDO2.o ${OBJECTDIR}/CO_PDO3.o ${OBJECTDIR}/CO_PDO.o ${OBJECTDIR}/CO_SDO1.o ${OBJECTDIR}/CO_SYNC.o ${OBJECTDIR}/CO_TOOLS.o ${OBJECTDIR}/DemoObj.o ${OBJECTDIR}/exttst.o ${OBJECTDIR}/main.o ${OBJECTDIR}/Timer.o ${OBJECTDIR}/CO_CANDRV24.o ${OBJECTDIR}/CO_MEMIO.o ${OBJECTDIR}/interrupt.o ${OBJECTDIR}/swi2c.o ${OBJECTDIR}/co_dict_cust.o ${OBJECTDIR}/co_emcy.o ${OBJECTDIR}/CO_TIME.o ${OBJECTDIR}/lcd_text.o
POSSIBLE_DEPFILES=${OBJECTDIR}/CO_COMM.o.d ${OBJECTDIR}/CO_DEV.o.d ${OBJECTDIR}/CO_dict.o.d ${OBJECTDIR}/CO_MAIN.o.d ${OBJECTDIR}/CO_NMT.o.d ${OBJECTDIR}/CO_NMTE.o.d ${OBJECTDIR}/CO_PDO1.o.d ${OBJECTDIR}/CO_PDO2.o.d ${OBJECTDIR}/CO_PDO3.o.d ${OBJECTDIR}/CO_PDO.o.d ${OBJECTDIR}/CO_SDO1.o.d ${OBJECTDIR}/CO_SYNC.o.d ${OBJECTDIR}/CO_TOOLS.o.d ${OBJECTDIR}/DemoObj.o.d ${OBJECTDIR}/exttst.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/Timer.o.d ${OBJECTDIR}/CO_CANDRV24.o.d ${OBJECTDIR}/CO_MEMIO.o.d ${OBJECTDIR}/interrupt.o.d ${OBJECTDIR}/swi2c.o.d ${OBJECTDIR}/co_dict_cust.o.d ${OBJECTDIR}/co_emcy.o.d ${OBJECTDIR}/CO_TIME.o.d ${OBJECTDIR}/lcd_text.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/CO_COMM.o ${OBJECTDIR}/CO_DEV.o ${OBJECTDIR}/CO_dict.o ${OBJECTDIR}/CO_MAIN.o ${OBJECTDIR}/CO_NMT.o ${OBJECTDIR}/CO_NMTE.o ${OBJECTDIR}/CO_PDO1.o ${OBJECTDIR}/CO_PDO2.o ${OBJECTDIR}/CO_PDO3.o ${OBJECTDIR}/CO_PDO.o ${OBJECTDIR}/CO_SDO1.o ${OBJECTDIR}/CO_SYNC.o ${OBJECTDIR}/CO_TOOLS.o ${OBJECTDIR}/DemoObj.o ${OBJECTDIR}/exttst.o ${OBJECTDIR}/main.o ${OBJECTDIR}/Timer.o ${OBJECTDIR}/CO_CANDRV24.o ${OBJECTDIR}/CO_MEMIO.o ${OBJECTDIR}/interrupt.o ${OBJECTDIR}/swi2c.o ${OBJECTDIR}/co_dict_cust.o ${OBJECTDIR}/co_emcy.o ${OBJECTDIR}/CO_TIME.o ${OBJECTDIR}/lcd_text.o

# Source Files
SOURCEFILES=CO_COMM.c CO_DEV.c CO_dict.c CO_MAIN.c CO_NMT.c CO_NMTE.c CO_PDO1.c CO_PDO2.c CO_PDO3.c CO_PDO.c CO_SDO1.c CO_SYNC.c CO_TOOLS.c DemoObj.c exttst.c main.c Timer.c CO_CANDRV24.c CO_MEMIO.c interrupt.c swi2c.c co_dict_cust.c co_emcy.c CO_TIME.c lcd_text.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24EP512GU810
MP_LINKER_FILE_OPTION=,-Tp24EP512GU810.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/CO_COMM.o: CO_COMM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_COMM.o.d 
	@${RM} ${OBJECTDIR}/CO_COMM.o.ok ${OBJECTDIR}/CO_COMM.o.err 
	@${RM} ${OBJECTDIR}/CO_COMM.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_COMM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_COMM.o.d" -o ${OBJECTDIR}/CO_COMM.o CO_COMM.c    
	
${OBJECTDIR}/CO_DEV.o: CO_DEV.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_DEV.o.d 
	@${RM} ${OBJECTDIR}/CO_DEV.o.ok ${OBJECTDIR}/CO_DEV.o.err 
	@${RM} ${OBJECTDIR}/CO_DEV.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_DEV.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_DEV.o.d" -o ${OBJECTDIR}/CO_DEV.o CO_DEV.c    
	
${OBJECTDIR}/CO_dict.o: CO_dict.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_dict.o.d 
	@${RM} ${OBJECTDIR}/CO_dict.o.ok ${OBJECTDIR}/CO_dict.o.err 
	@${RM} ${OBJECTDIR}/CO_dict.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_dict.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_dict.o.d" -o ${OBJECTDIR}/CO_dict.o CO_dict.c    
	
${OBJECTDIR}/CO_MAIN.o: CO_MAIN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_MAIN.o.d 
	@${RM} ${OBJECTDIR}/CO_MAIN.o.ok ${OBJECTDIR}/CO_MAIN.o.err 
	@${RM} ${OBJECTDIR}/CO_MAIN.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_MAIN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_MAIN.o.d" -o ${OBJECTDIR}/CO_MAIN.o CO_MAIN.c    
	
${OBJECTDIR}/CO_NMT.o: CO_NMT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_NMT.o.d 
	@${RM} ${OBJECTDIR}/CO_NMT.o.ok ${OBJECTDIR}/CO_NMT.o.err 
	@${RM} ${OBJECTDIR}/CO_NMT.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_NMT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_NMT.o.d" -o ${OBJECTDIR}/CO_NMT.o CO_NMT.c    
	
${OBJECTDIR}/CO_NMTE.o: CO_NMTE.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_NMTE.o.d 
	@${RM} ${OBJECTDIR}/CO_NMTE.o.ok ${OBJECTDIR}/CO_NMTE.o.err 
	@${RM} ${OBJECTDIR}/CO_NMTE.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_NMTE.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_NMTE.o.d" -o ${OBJECTDIR}/CO_NMTE.o CO_NMTE.c    
	
${OBJECTDIR}/CO_PDO1.o: CO_PDO1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO1.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO1.o.ok ${OBJECTDIR}/CO_PDO1.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO1.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO1.o.d" -o ${OBJECTDIR}/CO_PDO1.o CO_PDO1.c    
	
${OBJECTDIR}/CO_PDO2.o: CO_PDO2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO2.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO2.o.ok ${OBJECTDIR}/CO_PDO2.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO2.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO2.o.d" -o ${OBJECTDIR}/CO_PDO2.o CO_PDO2.c    
	
${OBJECTDIR}/CO_PDO3.o: CO_PDO3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO3.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO3.o.ok ${OBJECTDIR}/CO_PDO3.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO3.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO3.o.d" -o ${OBJECTDIR}/CO_PDO3.o CO_PDO3.c    
	
${OBJECTDIR}/CO_PDO.o: CO_PDO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO.o.ok ${OBJECTDIR}/CO_PDO.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO.o.d" -o ${OBJECTDIR}/CO_PDO.o CO_PDO.c    
	
${OBJECTDIR}/CO_SDO1.o: CO_SDO1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_SDO1.o.d 
	@${RM} ${OBJECTDIR}/CO_SDO1.o.ok ${OBJECTDIR}/CO_SDO1.o.err 
	@${RM} ${OBJECTDIR}/CO_SDO1.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_SDO1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_SDO1.o.d" -o ${OBJECTDIR}/CO_SDO1.o CO_SDO1.c    
	
${OBJECTDIR}/CO_SYNC.o: CO_SYNC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_SYNC.o.d 
	@${RM} ${OBJECTDIR}/CO_SYNC.o.ok ${OBJECTDIR}/CO_SYNC.o.err 
	@${RM} ${OBJECTDIR}/CO_SYNC.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_SYNC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_SYNC.o.d" -o ${OBJECTDIR}/CO_SYNC.o CO_SYNC.c    
	
${OBJECTDIR}/CO_TOOLS.o: CO_TOOLS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o.d 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o.ok ${OBJECTDIR}/CO_TOOLS.o.err 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_TOOLS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_TOOLS.o.d" -o ${OBJECTDIR}/CO_TOOLS.o CO_TOOLS.c    
	
${OBJECTDIR}/DemoObj.o: DemoObj.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DemoObj.o.d 
	@${RM} ${OBJECTDIR}/DemoObj.o.ok ${OBJECTDIR}/DemoObj.o.err 
	@${RM} ${OBJECTDIR}/DemoObj.o 
	@${FIXDEPS} "${OBJECTDIR}/DemoObj.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/DemoObj.o.d" -o ${OBJECTDIR}/DemoObj.o DemoObj.c    
	
${OBJECTDIR}/exttst.o: exttst.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/exttst.o.d 
	@${RM} ${OBJECTDIR}/exttst.o.ok ${OBJECTDIR}/exttst.o.err 
	@${RM} ${OBJECTDIR}/exttst.o 
	@${FIXDEPS} "${OBJECTDIR}/exttst.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/exttst.o.d" -o ${OBJECTDIR}/exttst.o exttst.c    
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o.ok ${OBJECTDIR}/main.o.err 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    
	
${OBJECTDIR}/Timer.o: Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Timer.o.d 
	@${RM} ${OBJECTDIR}/Timer.o.ok ${OBJECTDIR}/Timer.o.err 
	@${RM} ${OBJECTDIR}/Timer.o 
	@${FIXDEPS} "${OBJECTDIR}/Timer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/Timer.o.d" -o ${OBJECTDIR}/Timer.o Timer.c    
	
${OBJECTDIR}/CO_CANDRV24.o: CO_CANDRV24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o.d 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o.ok ${OBJECTDIR}/CO_CANDRV24.o.err 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_CANDRV24.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_CANDRV24.o.d" -o ${OBJECTDIR}/CO_CANDRV24.o CO_CANDRV24.c    
	
${OBJECTDIR}/CO_MEMIO.o: CO_MEMIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o.d 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o.ok ${OBJECTDIR}/CO_MEMIO.o.err 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_MEMIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_MEMIO.o.d" -o ${OBJECTDIR}/CO_MEMIO.o CO_MEMIO.c    
	
${OBJECTDIR}/interrupt.o: interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupt.o.d 
	@${RM} ${OBJECTDIR}/interrupt.o.ok ${OBJECTDIR}/interrupt.o.err 
	@${RM} ${OBJECTDIR}/interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/interrupt.o.d" -o ${OBJECTDIR}/interrupt.o interrupt.c    
	
${OBJECTDIR}/swi2c.o: swi2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/swi2c.o.d 
	@${RM} ${OBJECTDIR}/swi2c.o.ok ${OBJECTDIR}/swi2c.o.err 
	@${RM} ${OBJECTDIR}/swi2c.o 
	@${FIXDEPS} "${OBJECTDIR}/swi2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/swi2c.o.d" -o ${OBJECTDIR}/swi2c.o swi2c.c    
	
${OBJECTDIR}/co_dict_cust.o: co_dict_cust.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/co_dict_cust.o.d 
	@${RM} ${OBJECTDIR}/co_dict_cust.o.ok ${OBJECTDIR}/co_dict_cust.o.err 
	@${RM} ${OBJECTDIR}/co_dict_cust.o 
	@${FIXDEPS} "${OBJECTDIR}/co_dict_cust.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/co_dict_cust.o.d" -o ${OBJECTDIR}/co_dict_cust.o co_dict_cust.c    
	
${OBJECTDIR}/co_emcy.o: co_emcy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/co_emcy.o.d 
	@${RM} ${OBJECTDIR}/co_emcy.o.ok ${OBJECTDIR}/co_emcy.o.err 
	@${RM} ${OBJECTDIR}/co_emcy.o 
	@${FIXDEPS} "${OBJECTDIR}/co_emcy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/co_emcy.o.d" -o ${OBJECTDIR}/co_emcy.o co_emcy.c    
	
${OBJECTDIR}/CO_TIME.o: CO_TIME.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_TIME.o.d 
	@${RM} ${OBJECTDIR}/CO_TIME.o.ok ${OBJECTDIR}/CO_TIME.o.err 
	@${RM} ${OBJECTDIR}/CO_TIME.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_TIME.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_TIME.o.d" -o ${OBJECTDIR}/CO_TIME.o CO_TIME.c    
	
${OBJECTDIR}/lcd_text.o: lcd_text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/lcd_text.o.d 
	@${RM} ${OBJECTDIR}/lcd_text.o.ok ${OBJECTDIR}/lcd_text.o.err 
	@${RM} ${OBJECTDIR}/lcd_text.o 
	@${FIXDEPS} "${OBJECTDIR}/lcd_text.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG  -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/lcd_text.o.d" -o ${OBJECTDIR}/lcd_text.o lcd_text.c    
	
else
${OBJECTDIR}/CO_COMM.o: CO_COMM.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_COMM.o.d 
	@${RM} ${OBJECTDIR}/CO_COMM.o.ok ${OBJECTDIR}/CO_COMM.o.err 
	@${RM} ${OBJECTDIR}/CO_COMM.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_COMM.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_COMM.o.d" -o ${OBJECTDIR}/CO_COMM.o CO_COMM.c    
	
${OBJECTDIR}/CO_DEV.o: CO_DEV.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_DEV.o.d 
	@${RM} ${OBJECTDIR}/CO_DEV.o.ok ${OBJECTDIR}/CO_DEV.o.err 
	@${RM} ${OBJECTDIR}/CO_DEV.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_DEV.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_DEV.o.d" -o ${OBJECTDIR}/CO_DEV.o CO_DEV.c    
	
${OBJECTDIR}/CO_dict.o: CO_dict.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_dict.o.d 
	@${RM} ${OBJECTDIR}/CO_dict.o.ok ${OBJECTDIR}/CO_dict.o.err 
	@${RM} ${OBJECTDIR}/CO_dict.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_dict.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_dict.o.d" -o ${OBJECTDIR}/CO_dict.o CO_dict.c    
	
${OBJECTDIR}/CO_MAIN.o: CO_MAIN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_MAIN.o.d 
	@${RM} ${OBJECTDIR}/CO_MAIN.o.ok ${OBJECTDIR}/CO_MAIN.o.err 
	@${RM} ${OBJECTDIR}/CO_MAIN.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_MAIN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_MAIN.o.d" -o ${OBJECTDIR}/CO_MAIN.o CO_MAIN.c    
	
${OBJECTDIR}/CO_NMT.o: CO_NMT.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_NMT.o.d 
	@${RM} ${OBJECTDIR}/CO_NMT.o.ok ${OBJECTDIR}/CO_NMT.o.err 
	@${RM} ${OBJECTDIR}/CO_NMT.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_NMT.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_NMT.o.d" -o ${OBJECTDIR}/CO_NMT.o CO_NMT.c    
	
${OBJECTDIR}/CO_NMTE.o: CO_NMTE.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_NMTE.o.d 
	@${RM} ${OBJECTDIR}/CO_NMTE.o.ok ${OBJECTDIR}/CO_NMTE.o.err 
	@${RM} ${OBJECTDIR}/CO_NMTE.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_NMTE.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_NMTE.o.d" -o ${OBJECTDIR}/CO_NMTE.o CO_NMTE.c    
	
${OBJECTDIR}/CO_PDO1.o: CO_PDO1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO1.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO1.o.ok ${OBJECTDIR}/CO_PDO1.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO1.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO1.o.d" -o ${OBJECTDIR}/CO_PDO1.o CO_PDO1.c    
	
${OBJECTDIR}/CO_PDO2.o: CO_PDO2.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO2.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO2.o.ok ${OBJECTDIR}/CO_PDO2.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO2.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO2.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO2.o.d" -o ${OBJECTDIR}/CO_PDO2.o CO_PDO2.c    
	
${OBJECTDIR}/CO_PDO3.o: CO_PDO3.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO3.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO3.o.ok ${OBJECTDIR}/CO_PDO3.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO3.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO3.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO3.o.d" -o ${OBJECTDIR}/CO_PDO3.o CO_PDO3.c    
	
${OBJECTDIR}/CO_PDO.o: CO_PDO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_PDO.o.d 
	@${RM} ${OBJECTDIR}/CO_PDO.o.ok ${OBJECTDIR}/CO_PDO.o.err 
	@${RM} ${OBJECTDIR}/CO_PDO.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_PDO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_PDO.o.d" -o ${OBJECTDIR}/CO_PDO.o CO_PDO.c    
	
${OBJECTDIR}/CO_SDO1.o: CO_SDO1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_SDO1.o.d 
	@${RM} ${OBJECTDIR}/CO_SDO1.o.ok ${OBJECTDIR}/CO_SDO1.o.err 
	@${RM} ${OBJECTDIR}/CO_SDO1.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_SDO1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_SDO1.o.d" -o ${OBJECTDIR}/CO_SDO1.o CO_SDO1.c    
	
${OBJECTDIR}/CO_SYNC.o: CO_SYNC.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_SYNC.o.d 
	@${RM} ${OBJECTDIR}/CO_SYNC.o.ok ${OBJECTDIR}/CO_SYNC.o.err 
	@${RM} ${OBJECTDIR}/CO_SYNC.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_SYNC.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_SYNC.o.d" -o ${OBJECTDIR}/CO_SYNC.o CO_SYNC.c    
	
${OBJECTDIR}/CO_TOOLS.o: CO_TOOLS.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o.d 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o.ok ${OBJECTDIR}/CO_TOOLS.o.err 
	@${RM} ${OBJECTDIR}/CO_TOOLS.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_TOOLS.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_TOOLS.o.d" -o ${OBJECTDIR}/CO_TOOLS.o CO_TOOLS.c    
	
${OBJECTDIR}/DemoObj.o: DemoObj.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/DemoObj.o.d 
	@${RM} ${OBJECTDIR}/DemoObj.o.ok ${OBJECTDIR}/DemoObj.o.err 
	@${RM} ${OBJECTDIR}/DemoObj.o 
	@${FIXDEPS} "${OBJECTDIR}/DemoObj.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/DemoObj.o.d" -o ${OBJECTDIR}/DemoObj.o DemoObj.c    
	
${OBJECTDIR}/exttst.o: exttst.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/exttst.o.d 
	@${RM} ${OBJECTDIR}/exttst.o.ok ${OBJECTDIR}/exttst.o.err 
	@${RM} ${OBJECTDIR}/exttst.o 
	@${FIXDEPS} "${OBJECTDIR}/exttst.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/exttst.o.d" -o ${OBJECTDIR}/exttst.o exttst.c    
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o.ok ${OBJECTDIR}/main.o.err 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.c    
	
${OBJECTDIR}/Timer.o: Timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/Timer.o.d 
	@${RM} ${OBJECTDIR}/Timer.o.ok ${OBJECTDIR}/Timer.o.err 
	@${RM} ${OBJECTDIR}/Timer.o 
	@${FIXDEPS} "${OBJECTDIR}/Timer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/Timer.o.d" -o ${OBJECTDIR}/Timer.o Timer.c    
	
${OBJECTDIR}/CO_CANDRV24.o: CO_CANDRV24.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o.d 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o.ok ${OBJECTDIR}/CO_CANDRV24.o.err 
	@${RM} ${OBJECTDIR}/CO_CANDRV24.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_CANDRV24.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_CANDRV24.o.d" -o ${OBJECTDIR}/CO_CANDRV24.o CO_CANDRV24.c    
	
${OBJECTDIR}/CO_MEMIO.o: CO_MEMIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o.d 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o.ok ${OBJECTDIR}/CO_MEMIO.o.err 
	@${RM} ${OBJECTDIR}/CO_MEMIO.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_MEMIO.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_MEMIO.o.d" -o ${OBJECTDIR}/CO_MEMIO.o CO_MEMIO.c    
	
${OBJECTDIR}/interrupt.o: interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/interrupt.o.d 
	@${RM} ${OBJECTDIR}/interrupt.o.ok ${OBJECTDIR}/interrupt.o.err 
	@${RM} ${OBJECTDIR}/interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/interrupt.o.d" -o ${OBJECTDIR}/interrupt.o interrupt.c    
	
${OBJECTDIR}/swi2c.o: swi2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/swi2c.o.d 
	@${RM} ${OBJECTDIR}/swi2c.o.ok ${OBJECTDIR}/swi2c.o.err 
	@${RM} ${OBJECTDIR}/swi2c.o 
	@${FIXDEPS} "${OBJECTDIR}/swi2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/swi2c.o.d" -o ${OBJECTDIR}/swi2c.o swi2c.c    
	
${OBJECTDIR}/co_dict_cust.o: co_dict_cust.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/co_dict_cust.o.d 
	@${RM} ${OBJECTDIR}/co_dict_cust.o.ok ${OBJECTDIR}/co_dict_cust.o.err 
	@${RM} ${OBJECTDIR}/co_dict_cust.o 
	@${FIXDEPS} "${OBJECTDIR}/co_dict_cust.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/co_dict_cust.o.d" -o ${OBJECTDIR}/co_dict_cust.o co_dict_cust.c    
	
${OBJECTDIR}/co_emcy.o: co_emcy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/co_emcy.o.d 
	@${RM} ${OBJECTDIR}/co_emcy.o.ok ${OBJECTDIR}/co_emcy.o.err 
	@${RM} ${OBJECTDIR}/co_emcy.o 
	@${FIXDEPS} "${OBJECTDIR}/co_emcy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/co_emcy.o.d" -o ${OBJECTDIR}/co_emcy.o co_emcy.c    
	
${OBJECTDIR}/CO_TIME.o: CO_TIME.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/CO_TIME.o.d 
	@${RM} ${OBJECTDIR}/CO_TIME.o.ok ${OBJECTDIR}/CO_TIME.o.err 
	@${RM} ${OBJECTDIR}/CO_TIME.o 
	@${FIXDEPS} "${OBJECTDIR}/CO_TIME.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/CO_TIME.o.d" -o ${OBJECTDIR}/CO_TIME.o CO_TIME.c    
	
${OBJECTDIR}/lcd_text.o: lcd_text.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/lcd_text.o.d 
	@${RM} ${OBJECTDIR}/lcd_text.o.ok ${OBJECTDIR}/lcd_text.o.err 
	@${RM} ${OBJECTDIR}/lcd_text.o 
	@${FIXDEPS} "${OBJECTDIR}/lcd_text.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c ${MP_CC} $(MP_EXTRA_CC_PRE)  -g -omf=elf -x c -c -mcpu=$(MP_PROCESSOR_OPTION) -Wall -I"C:/Program Files (x86)/Microchip/mplabc30/v3.31/support/PIC24E/h" -I"." -O1 -MMD -MF "${OBJECTDIR}/lcd_text.o.d" -o ${OBJECTDIR}/lcd_text.o lcd_text.c    
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -D__DEBUG  -o dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,-L".",-Map="${DISTDIR}/Vitrociset_Pannello_Can.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1
else
dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -omf=elf -mcpu=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}         -Wl,--defsym=__MPLAB_BUILD=1,-L".",-Map="${DISTDIR}/Vitrociset_Pannello_Can.${IMAGE_TYPE}.map",--report-mem$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION)
	${MP_CC_DIR}\\pic30-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/VITROCISET_PANNELLO_CAN.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -omf=elf
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
