
/*****************************************************************************
 *
 * Microchip CANopen Stack (Network Management Error Communications Handler)
 *
 *****************************************************************************
 * FileName:        CO_NMTE.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		7.7.17		PIC24GU & restructured Dictionary
 * 									20.7.17		rewrite, less idiotic :)
 *****************************************************************************/



#ifndef	__CO_NMTE_H
#define	__CO_NMTE_H


#define NMTE_BOOT_SERVICE	_uNMTEState.bits.b0
#define NMTE_TMR_LOCAL_EN	_uNMTEState.bits.b1
#define NMTE_NODE_GUARD_EN	_uNMTEState.bits.b2


extern DWORD HeartBeatConsumers[1];

void CO_COMM_NMTE_HeartBeatAccessEvent(void);

void CO_COMM_NMTE_GuardTimeAccessEvent(void);

void CO_COMM_NMTE_LifeFactorAccessEvent(void);

void CO_COMM_NMTE_Open(void);

void CO_COMM_EMCY_TXEvent(BYTE);

void CO_COMM_NMTE_Close(void);

void CO_COMM_NMTE_RXEvent(unsigned char);

void CO_COMM_NMTE_TXEvent(BYTE);

void CO_COMM_NMTE_LSTimerEvent(void);

void CO_COMM_NMTE_TXFinEvent(void);


extern UNSIGNED8 _uNMTEState;
extern UNSIGNED8 _uNMTELocalState;
extern UNSIGNED16 _uNMTEHeartBeat;
extern UNSIGNED16 _uNMTEGuardTime;
extern UNSIGNED8 _uNMTELifeFactor;

#define mNMTE_SetHeartBeat(HeartBeat)	_uNMTEHeartBeat.word = HeartBeat;

#define mNMTE_GetHeartBeat()			_uNMTEHeartBeat.word

#define mNMTE_SetGuardTime(GuardTime)	_uNMTEGuardTime.word = GuardTime;

#define mNMTE_GetGuardTime()			_uNMTEGuardTime.word

#define mNMTE_SetLifeFactor(LifeFactor)	_uNMTELifeFactor.byte = LifeFactor;

#define mNMTE_GetLifeFactor()			_uNMTELifeFactor.byte


#endif	//__CO_NMTE_H
