/*****************************************************************************
 *
 * Microchip CANopen Stack (Demonstration Object)
 *
 *****************************************************************************
 * FileName:        DEMOOBJ.H
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		22.02.17	PIC24 version
 * 
 *****************************************************************************/


#include "co_main24.h"

// These are mapping constants for TPDOs
// starting at 0x1A00 in the dictionary
extern rom unsigned long uTPDO1Map[2];
extern rom unsigned long uRPDO1Map[2];
extern rom unsigned long uTPDO2Map[1];		// togliere
#ifdef NUM_BYTE_LED_I2C 
extern rom unsigned long uRPDO2Map[NUM_BYTE_LED_I2C];
#endif
#if defined(NUM_ANA)
extern rom unsigned long uTPDO3Map[NUM_ANA];		// MAX_ANA; facciam cos�...
extern rom unsigned long uRPDO3Map[NUM_ANA];		// MAX_PWM
#elif defined(NUM_PWM)
extern rom unsigned long uTPDO3Map[NUM_PWM];		// MAX_ANA
extern rom unsigned long uRPDO3Map[NUM_PWM];		// MAX_PWM
#endif
extern rom unsigned long uTPDO4Map;
extern rom unsigned long uRPDO4Map;
extern rom unsigned long uPDO1Dummy;

//extern unsigned int uIOinFilter[];				// 0x6003 filter
//extern unsigned int uIOinPolarity[];				// 0x6002 polarity
//extern unsigned int uIOinIntChange[];			// 0x6006 interrupt on change
//extern unsigned int uIOinIntRise[];				// 0x6007 interrupt on positive edge
//extern unsigned int uIOinIntFall[];				// 0x6008 interrupt on negative edge
//extern unsigned int uIOinIntEnable[];			// 0x6005 enable interrupts, SONO DEI FLAG, change ecc!
extern unsigned int uIOinDigiInputs[];			// 0x6000 digital inputs

//extern unsigned int uIOinDigiInOld;				// 

extern unsigned char uLocalXmtBuffer[8];			// Local buffer for TPDO1
extern unsigned char uLocalRcvBuffer[8];			// local buffer fot RPDO1
extern unsigned char uLocalXmtBuffer2[8];			// Local buffer for TPDO2
extern unsigned char uLocalRcvBuffer2[8];			// local buffer fot RPDO2
#ifdef USE_PDO_3
extern unsigned char uLocalXmtBuffer3[8];			// Local buffer for TPDO3
extern unsigned char uLocalRcvBuffer3[8];			// local buffer fot RPDO3
#endif

extern rom unsigned char uDemoTPDO1Len,uDemoTPDO2Len;

extern rom unsigned char rMaxIndexPuls;

extern unsigned int statoLed;
extern unsigned char statoLedI2C[4];
extern unsigned char statoDisplay[28];
extern unsigned char statoLCD[3][4*20];
#ifdef USA_PWM
extern unsigned int statoPWM[];
#endif
void _setTerminatorsMode(void);
// bah no #if defined(NUM_BYTE_TASTI_I2C) || defined(NUM_BYTE_LED_I2C)
extern unsigned char ErroreI2C;
//#endif
 

void DemoProcessEvents(void);
void DemoInit(void);
void DemoInitHW(void);




