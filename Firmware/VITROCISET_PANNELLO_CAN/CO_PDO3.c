

// The communications conducted in this object incorporates 
// one transmit and one receive endpoint


// Global definitions

#include  "co_main24.h"
#include	"CO_TYPES24.H"
#include	"CO_DICT.H"				
#include	"CO_COMM.H"				// Communications
#include	"CO_PDO.H"				// PDO functions
#include	"CO_PDO3.H"				// PDO functions


rom unsigned long uRPDOMap3 = 0x02292312L;

#ifdef USE_PDO_3
UNSIGNED32		uRPDO3Comm;		// RPD3 communication setting
UNSIGNED32		uTPDO3Comm;		// TPD3 communication setting
_PDOBUF 		_uPDO3;
unsigned char 	_uRPDO3Handles,_uTPDO3Handles;
#endif

// ------------------------------------ usati per PWM e enalogici



#ifdef USE_PDO_3
void CO_COMM_PDO3_Create(WORD tSID,WORD rSID) {
	// Convert to MCHP
	// Store the COB
	mTPDOSetCOB(3,Canopen2Microchip((tSID << 7) | configParms.nodeID));

	// Convert to MCHP
	// Store the COB
	mRPDOSetCOB(3,Canopen2Microchip((rSID << 7) | configParms.nodeID));
	}

void CO_COMM_PDO3_Open(BYTE enableRX, BYTE enableTX) {

	// Call the driver and request to open a receive endpoint
	_uRPDO3Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_RPDO_3, uRPDO3Comm.word);
	if(_uRPDO3Handles) {
		if(enableRX)
			COMM_RPDO_3_EN = 1;	
		}

	_uTPDO3Handles=CANOpenMessage((COMM_MSGGRP_PDO) | COMM_TPDO_3, uTPDO3Comm.word);
	if(_uTPDO3Handles) {
		if(enableTX)	
			COMM_TPDO_3_EN=1;		// [was: perch� dio porco /2]
		}
	}

void CO_COMM_PDO3_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_uRPDO3Handles);
	CANCloseMessage(_uTPDO3Handles);
	COMM_RPDO_3_EN = 0;	
	COMM_TPDO_3_EN=0;	
	}


void CO_COMM_TPDO3_TypeAccessEvent(void) {
	unsigned char tempType;
	
	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the 
			// structure with legth, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// Write the Type to the buffer
// NON CI SONO!			*(uDict.obj->pReqBuf) = configParms.uSyncSet[2];
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			tempType = *(uDict.obj->pReqBuf);
			if((tempType >= 0) && (tempType <= 240)) {
				// Set the new type and resync
// NON CI SONO!				uDemoSyncCount[2] = configParms.uSyncSet[2] = tempType;
				}
			else {
				if((tempType == 254) || (tempType == 255))	{
// NON CI SONO!					configParms.uSyncSet[2] = tempType;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			
			break;
		}	
	}


// Process access events to the inhibit time
void CO_COMM_TPDO3_ITimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}


// Process access events to the event timer
void CO_COMM_TPDO3_ETimeAccessEvent(void) {

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information about the object
			// The application should use this to load the structure with length, access, and mapping.
			break;

		case DICT_OBJ_READ: 	// Read the object
			// The application should use this to load the buffer with the requested data.
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// The application should use this to write the object with the specified data.
			break;
		}	
	}


void CO_COMM_RPDO3_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(mRPDOGetCOB(2));
//USARE _CO_COB_MCHP2CANopen_SID(mRPDOGetCOB(2))

			myCob=Microchip2Canopen(mRPDOGetCOB(3));

			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS) {
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mRPDOGetCOB(3)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mRPDOIsOpen(3)) {
						mRPDOClose(3);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(3))).PDO_DIS = 1;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mRPDOGetCOB(3)) & 0xFFFFEFFFL))	{
					// but only open if the PDO endpoint was closed
					if(!mRPDOIsOpen(3)) {
						mRPDOOpen(3);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mRPDOGetCOB(3))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			break;
		case DICT_OBJ_INFO: 	// Read the object
		break;
		}	
	}

// Process access events to the COB ID
void CO_COMM_TPDO3_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(mTPDOGetCOB(3));
//USARE _CO_COB_MCHP2CANopen_SID(mTPDOGetCOB(3))

			myCob=Microchip2Canopen(mTPDOGetCOB(3));
			
			// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) =myCob;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));
//USARE _CO_COB_CANopen2MCHP_SID(*(unsigned long *)(uDict.obj->pReqBuf))

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));
			
			// If the request is to stop the PDO
			if((*(UNSIGNED32 *)(&myCob)).PDO_DIS)	{
				// And if the COB received matches the stored COB and type then close
				if(!((myCob ^ mTPDOGetCOB(3)) & 0xFFFFEFFFL)) {
					// but only close if the PDO endpoint was open
					if(mTPDOIsOpen(3)) {
						mTPDOClose(3);
						}
		
					// Indicate to the local object that this PDO is disabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(3))).PDO_DIS = 1;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}

			// Else if the TPDO is not open then start the TPDO
			else	{
				// And if the COB received matches the stored COB and type then open
				if(!((myCob ^ mTPDOGetCOB(3)) & 0xFFFFEFFFL)) {
					// but only open if the PDO endpoint was closed
					if(!mTPDOIsOpen(3)) {
						mTPDOOpen(3);
						}
						
					// Indicate to the local object that this PDO is enabled
					(*(UNSIGNED32 *)(&mTPDOGetCOB(3))).PDO_DIS = 0;
					}
				else {
					mCO_DictSetRet(E_PARAM_RANGE);
					} //error
				}
			break;
		case DICT_OBJ_INFO: 	// Read the object
			break;
		}	
	}

void CO_COMM_RPDO3_RXEvent(unsigned char b) {
	WORD n21,n22;

	mRPDORead(3);

	*((_DATA8 *)(_uPDO3.RPDO.buf)) = *((__eds__ _DATA8 *)(mCANGetPtrRxData(b)));

	_uPDO3.RPDO.len = mCANGetDataLen(b);


	if(_uPDO3.RPDO.len == 4) {		// 4 byte anche se 0..100 ...
	//pwm
		n21=MAKEWORD(uLocalRcvBuffer3[0],uLocalRcvBuffer3[1]);
		n22=MAKEWORD(uLocalRcvBuffer3[2],uLocalRcvBuffer3[3]);
		GestPWM(n21,n22);
		}

	}

void CO_COMM_TPDO3_RXEvent(unsigned char b) {

	mTPDORead(3);


	if(mCANIsGetRTR(b))	{
		mTPDOWritten(3);
		}

	}

void CO_COMM_RPDO3_TXEvent(BYTE b) {

	}

void CO_COMM_TPDO3_TXEvent(BYTE b) {

	// Call the driver, load data to transmit
	*((__eds__ _DATA8 *)(mCANGetPtrTxData(b))) = *((_DATA8 *)(_uPDO3.TPDO.buf));

	mCANPutDataLen(b,_uPDO3.TPDO.len);

	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(b)) = uTPDO3Comm.word;

	}


void CO_PDO3LSTimerEvent(void) {		// ma CHI LI USA?? !
	
	}

void CO_RPDO3TXFinEvent(void) {
	
	}

void CO_TPDO3TXFinEvent(void) {
	
	}


#endif

