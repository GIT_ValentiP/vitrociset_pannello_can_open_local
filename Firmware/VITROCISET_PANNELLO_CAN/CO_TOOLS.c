/*****************************************************************************
 *
 * Microchip CANopen Stack (COB Conversion Tools)
 *
 *****************************************************************************
 * FileName:        CO_TOOLS.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * GC 							spring 2017
 *****************************************************************************/




#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include  "co_main24.h"
#endif
#include	"CO_TYPES24.H"






typedef struct _CO_CID {
	union _CO_L	{
		unsigned char byte;
	} l;
	union _CO_H	{
		unsigned char byte;
	} h;
	union _CO_UL {
		unsigned char byte;
	} ul;
	union _CO_UH {
		unsigned char byte;
		struct _CO_UH_BITS {
			unsigned 	:5;
			unsigned 	id:1;
			unsigned	b0:1;
			unsigned 	b1:1;
			} bits;
		} uh;
	} __attribute__ ((packed)) CO_CID;



//unsigned long uMCHP_COB;
//unsigned long uCO_COB;

UNSIGNED32		_uCOB_ID_in;
UNSIGNED32		_uCOB_ID_out;



#if defined(__18CXX)
/*********************************************************************
 * Function:        void _CO_COB_CANopen2MCHP(void)
 *
 * PreCondition:    _uCOB_ID_in must be loaded.
 *
 * Input:       	none
 *                  
 * Output:         	none  
 *
 * Side Effects:    none
 *
 * Overview:        This function converts the CANopen COB format to
 *					the MCHP PIC18 format.
 *
 * Note:			B3       B2       B1       B0					
 *					-----------------------------------
 * CANopen format	28 - 24  23 - 16  15 - 8   7 - 0
 *    				nmtxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
 * MCHP format		
		word 0:
				CiTRBnSID = 0bxxx1 0010 0011 1100
				IDE = 0b0
				SRR = 0b0
				SID<10:0>= 0b100 1000 1111 
		word 1:
				CiTRBnEID = 0bxxxx 0000 0000 0000
				EID<17:6> = 0b0000 0000 0000 
		word 2:
				CiTRBnDLC = 0b0000 0000 xxx0 1111
				EID<5:0> = 0b000000
				RTR = 0b0
				RB1 = 0b0
				RB0 = 0b0
				DLC = 0b1111 in effetti il max � 8!
 *
 * CANopen format	                  10 - 8   7 - 0
 *    				nmt----- -------- -----xxx xxxxxxxx
 * MCHP format		                  3 - 0    10 - 3
		word 0:
				CiTRBnSID = 0bxxx1 0010 0011 1100
				IDE = 0b0
				SRR = 0b0
				SID<10:0>= 0b100 1000 1111 
 *
 * n = option bit 1
 * m = option bit 2
 * t = ID type (standard = 0, extended = 1)
 ********************************************************************/
void _CO_COB_CANopen2MCHP(void) {

	// If this is an extended ID
	if(_uCOB_ID_in.bytes.B3.byte & 0x20)	{
		// Shift bits 28 - 16
		*(unsigned int *)(&_uCOB_ID_out.bytes.B0.byte) = (*(unsigned int *)(&_uCOB_ID_in.bytes.B2.byte)) << 3;
		
		// Temporarily store SIDH
		_uCOB_ID_out.bytes.B3.byte = _uCOB_ID_out.bytes.B1.byte;
		
		// Generate SIDL
		_uCOB_ID_out.bytes.B1.byte = ((_uCOB_ID_out.bytes.B0.byte & 0xE0) | (_uCOB_ID_in.bytes.B2.byte & 0x03)) | 0x08;
		
		// Copy SIDH
		_uCOB_ID_out.bytes.B0.byte = _uCOB_ID_out.bytes.B3.byte;
		
		// Copy EIDL and EIDH
		_uCOB_ID_out.bytes.B3.byte = _uCOB_ID_in.bytes.B0.byte;
		_uCOB_ID_out.bytes.B2.byte = _uCOB_ID_in.bytes.B1.byte;
		}
	// Else 11-bit ID
	else {
		// Convert the ID
		_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word) << 5);
		_uCOB_ID_out.bytes.B2.byte = _uCOB_ID_out.bytes.B0.byte;
		_uCOB_ID_out.bytes.B0.byte = _uCOB_ID_out.bytes.B1.byte;
		_uCOB_ID_out.bytes.B1.byte = _uCOB_ID_out.bytes.B2.byte & 0xE0;
		_uCOB_ID_out.bytes.B2.byte = 0;
		} 

	// Set the option bits
	if(_uCOB_ID_in.bytes.B3.bits.b7) 
		_uCOB_ID_out.bytes.B1.bits.b4 = 1;
	if(_uCOB_ID_in.bytes.B3.bits.b6) 
		_uCOB_ID_out.bytes.B1.bits.b2 = 1;

	}

#else
/*********************************************************************
 * Function:        void _CO_COB_CANopen2MCHP(void)
 *
 * PreCondition:    _uCOB_ID_in must be loaded.
 *
 * Input:       	none
 *                  
 * Output:         	none  
 *
 * Side Effects:    none
 *
 * Overview:        This function converts the CANopen COB format to
 *					the MCHP PIC24E format.
 *
 * Note:          	B3       B2       B1       B0					
 *					-----------------------------------
 * CANopen format	28 - 24  23 - 16  15 - 8   7 - 0
 *    				nmtxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
 * MCHP format		7 - 0    15 - 8   20 - 16  28 - 21
 *					xxxxxxxx xxxxxxxx xxxntmxx xxxxxxxx
 *
 * CANopen format	                  10 - 8   7 - 0
 *    				nmt----- -------- -----xxx xxxxxxxx
 * MCHP format		                  3 - 0    10 - 3
 *					-------- -------- xxxntm-- xxxxxxxx
 *
 * n = option bit 1
 * m = option bit 2
 * t = ID type (standard = 0, extended = 1)
 ********************************************************************/
void _CO_COB_CANopen2MCHP(void) {

	// If this is an extended ID
	if(_uCOB_ID_in.bytes.B3.byte & 0x20)	{

		_uCOB_ID_out.word = ((unsigned int)((_uCOB_ID_in.word & 0b11111111111)) << 2);
		_uCOB_ID_out.word |= ((unsigned int)(((_uCOB_ID_in.word >> (11+6)) & 0b111111111111)));
// VA NELLA WORD DOPO! FINIRE		_uCOB_ID_out.word = ((unsigned int)(((_uCOB_ID_in.word >> (11)) & 0b111111)));
		// Set the option bits
		if(_uCOB_ID_in.bytes.B3.bits.b7) 
			_uCOB_ID_out.bytes.B0.bits.b1 = 1;
		if(_uCOB_ID_in.bytes.B3.bits.b6) 
			_uCOB_ID_out.bytes.B0.bits.b0 = 1;

		}
	// Else 11-bit ID
	else {
		// Convert the ID
		_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word) << 2);
		// Set the option bits
		if(_uCOB_ID_in.bytes.B3.bits.b7) 
			_uCOB_ID_out.bytes.B0.bits.b1 = 1;
		if(_uCOB_ID_in.bytes.B3.bits.b6) 
			_uCOB_ID_out.bytes.B0.bits.b0 = 1;
		} 


	}

#endif


WORD _CO_COB_CANopen2MCHP_SID(WORD cIn) {		// perch� porcodio non puoi scrivere codice cos� di merda!
	WORD cOut;

	// Else 11-bit ID
	// Convert the ID
	cOut = cIn << 2;
	// Set the option bits
	if(cIn & 0b1000000000000000) 
		cOut |= 0b00000010;
	if(cIn & 0b0100000000000000) 
		cOut |= 0b00000001;

	return cOut;

	}

DWORD _CO_COB_CANopen2MCHP_EID(DWORD cIn) {		// idem
	DWORD cOut;

	cOut = cIn & 0b11111111111 << 2;
	cOut |= (cIn >> (11+6)) & 0b111111111111;
// VA NELLA WORD DOPO! FINIRE		_uCOB_ID_out.word = ((unsigned int)(((_uCOB_ID_in.word >> (11)) & 0b111111)));
	// Set the option bits
	if(cIn & 0b1000000000000000) 
		cOut |= 0b00000010;
	if(cIn & 0b0100000000000000) 
		cOut |= 0b00000001;

	return cOut;
	}




/*********************************************************************
 * Function:        void _CO_COB_CANopen2MCHP(void)
 *
 * PreCondition:    _uCOB_ID_in must be loaded.
 *
 * Input:       	none
 *                  
 * Output:         	none  
 *
 * Side Effects:    none
 *
 * Overview:        This function converts the CANopen COB format to
 *					the MCHP format.
 *
 * Note:			B3       B2       B1       B0					
 *					-----------------------------------
 * CANopen format	28 - 24  23 - 16  15 - 8   7 - 0
 *    				nmtxxxxx xxxxxxxx xxxxxxxx xxxxxxxx
 * MCHP format		
		word 0:
				CiTRBnSID = 0bxxx1 0010 0011 1100
				IDE = 0b0
				SRR = 0b0
				SID<10:0>= 0b100 1000 1111 
		word 1:
				CiTRBnEID = 0bxxxx 0000 0000 0000
				EID<17:6> = 0b0000 0000 0000 
		word 2:
				CiTRBnDLC = 0b0000 0000 xxx0 1111
				EID<5:0> = 0b000000
				RTR = 0b0
				RB1 = 0b0
				RB0 = 0b0
				DLC = 0b1111 in effetti il max � 8!
 *
 * CANopen format	                  10 - 8   7 - 0
 *    				nmt----- -------- -----xxx xxxxxxxx
 * MCHP format		                  3 - 0    10 - 3
		word 0:
				CiTRBnSID = 0bxxx1 0010 0011 1100
				IDE = 0b0
				SRR = 0b0
				SID<10:0>= 0b100 1000 1111 
 *
 * n = option bit 1
 * m = option bit 2
 * t = ID type (standard = 0, extended = 1)
 ********************************************************************/

void _CO_COB_MCHP2CANopen(void) {

	// If this is an extended ID
	if(0 /*_uCOB_ID_in.bytes.B1.byte & 0x08  IL FLAG NON E' QUA!! ma in un registro direi...*/ )	{
				
		_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word & 0b11111111111) >> 2);
		
		_uCOB_ID_out.word |= ((unsigned int)((_uCOB_ID_in.word & 0b111111111111) << 17));

// e, come sopra, ci sono gli altri 6 bit da qualche parte...

		// Set the option bits
		if(_uCOB_ID_in.bytes.B0.bits.b0) 
			_uCOB_ID_out.bytes.B3.bits.b6 = 1;
		if(_uCOB_ID_in.bytes.B0.bits.b1) 
			_uCOB_ID_out.bytes.B3.bits.b7 = 1;

		}
	// Else 11-bit ID
	else {
		_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word >> 2) & 0b11111111111);
		
		// Set the option bits
		if(_uCOB_ID_in.bytes.B0.bits.b0) 
			_uCOB_ID_out.bytes.B3.bits.b6 = 1;
		if(_uCOB_ID_in.bytes.B0.bits.b1) 
			_uCOB_ID_out.bytes.B3.bits.b7 = 1;

		}

	}



WORD _CO_COB_MCHP2CANopen_SID(WORD cIn) {			// pi� idem!
	WORD cOut;

	// 11-bit ID
	_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word % 0b11111111111) >> 2);
		
	// Set the option bits
	if(_uCOB_ID_in.bytes.B0.bits.b0) 
		_uCOB_ID_out.bytes.B3.bits.b6 = 1;
	if(_uCOB_ID_in.bytes.B0.bits.b1) 
		_uCOB_ID_out.bytes.B3.bits.b7 = 1;

	return cOut;
	}

DWORD _CO_COB_MCHP2CANopen_EID(DWORD cIn) {
	DWORD cOut;

	// this is an extended ID
				
	_uCOB_ID_out.word = ((unsigned int)(_uCOB_ID_in.word & 0b11111111111) >> 2);
	
	_uCOB_ID_out.word |= ((unsigned int)((_uCOB_ID_in.word & 0b111111111111) << 17));

// e, come sopra, ci sono gli altri 6 bit da qualche parte...

	// Set the option bits
	if(_uCOB_ID_in.bytes.B0.bits.b0) 
		_uCOB_ID_out.bytes.B3.bits.b6 = 1;
	if(_uCOB_ID_in.bytes.B0.bits.b1) 
		_uCOB_ID_out.bytes.B3.bits.b7 = 1;

	return cOut;
	}

WORD Canopen2Microchip(WORD cob) {

	_uCOB_ID_in.word = cob; 
	_CO_COB_CANopen2MCHP();
	return _uCOB_ID_out.word;
	}

WORD Microchip2Canopen(WORD cob) {

	_uCOB_ID_in.word = cob; 
	_CO_COB_MCHP2CANopen();
	return _uCOB_ID_out.word;
	}



