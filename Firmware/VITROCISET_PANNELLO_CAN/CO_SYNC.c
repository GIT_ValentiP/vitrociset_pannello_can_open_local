/*****************************************************************************
 *
 * Microchip CANopen Stack (SYNC Object)
 *
 *****************************************************************************
 * FileName:        CO_SYNC.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * CinziaG 					24.3.2017		PIC24E 
 * 
 *****************************************************************************/


									// Global definitions

#ifdef IS_BOOTLOADER
#include  "bootloader\co_main24.h"
#else
#include  "co_main24.h"
#endif
#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services

#include	"CO_COMM.H"				// Object
#include	"CO_DICT.H"				// Dictionary Object Services
#include	"CO_ABERR.H"			// Abort types
#include	"CO_TOOLS.H"

#include	"CO_SYNC.H"



// External communications event
void CO_COMMSyncEvent(void);

// Object 1005h, this is also defined functionally
// This is also initialized by the app at startup since it could be 
// initialized from non-volitile memory.
UNSIGNED32 _uSYNC_COBID;

// Local handle to the receive endpoint. Links to filtering in the driver.
unsigned char _hSYNC;


void CO_COMM_SYNC_Open(void) {	

	// Call the driver and request to open a receive endpoint
	_hSYNC=CANOpenMessage(COMM_MSGGRP_NETCTL | COMM_NETCTL_SYNC, _uSYNC_COBID.word);

	// Enable SYNC
	if(_hSYNC) 
		COMM_NETCTL_SYNC_EN = 1;
	}


void _CO_COMM_SYNC_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_hSYNC);
	COMM_NETCTL_SYNC_EN = 0;
	}


void CO_COMM_SYNC_RXEvent(unsigned char b) {

	// If the length of the data is 0 then continue
	if(!mCANGetDataLen(b)) {
		// Notify the app that a SYNC has been received
		CO_COMMSyncEvent();	
		}
	}


void CO_COMM_SYNC_COBIDAccessEvent(void) {
	WORD myCob;

	switch(mCO_DictGetCmd())	{
		case DICT_OBJ_INFO:		// Get information not required for this object
			break;

		case DICT_OBJ_READ: 	// Read the object
			// Translate MCHP COB to CANopen COB
//			mTOOLS_MCHP2CO(_uSYNC_COBID.word);
// USARE _CO_COB_MCHP2CANopen_SID(_uSYNC_COBID.word)

			myCob=Microchip2Canopen(_uSYNC_COBID.word);

		// Return the COBID
			*(unsigned long *)(uDict.obj->pReqBuf) = mTOOLS_GetCOBID();
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			// Translate the COB to MCHP format
//			mTOOLS_CO2MCHP(*(unsigned long *)(uDict.obj->pReqBuf));

			myCob=Canopen2Microchip(*(unsigned long *)(uDict.obj->pReqBuf));

			// Check for good COBID not necessary
			
			// Insure the COB indicates receive only
			if((*(UNSIGNED32 *)(&myCob)).bytes.B1.bits.b2 == 0) {

				// Close the SYNC object
				_CO_COMM_SYNC_Close();

				// Copy the output to the local COB ID
				_uSYNC_COBID.word = myCob;

				// Start the SYNC object
				CO_COMM_SYNC_Open();
				}
			
			// Else return error code
			else {
				mCO_DictSetRet(E_PARAM_RANGE);
				}
				
			break;
		}	
	}



