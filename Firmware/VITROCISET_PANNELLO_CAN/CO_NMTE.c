
/*****************************************************************************
 *
 * Microchip CANopen Stack (Network Management Error Communications Handler)
 *
 *****************************************************************************
 * FileName:        CO_NMTE.C
 * Dependencies:    
 * Processor:       PIC18F with CAN
 * Compiler:       	C18 02.30.00 or higher
 * Linker:          MPLINK 03.70.00 or higher
 * Company:         Microchip Technology Incorporated
 *
 * Software License Agreement
 *
 * The software supplied herewith by Microchip Technology Incorporated
 * (the "Company") is intended and supplied to you, the Company's
 * customer, for use solely and exclusively with products manufactured
 * by the Company. 
 *
 * The software is owned by the Company and/or its supplier, and is 
 * protected under applicable copyright laws. All rights are reserved. 
 * Any use in violation of the foregoing restrictions may subject the 
 * user to criminal sanctions under applicable laws, as well as to 
 * civil liability for the breach of the terms and conditions of this 
 * license.
 *
 * 
 * 
 *
 *
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Ross Fosler			11/13/03	...	
 * Cinzia Greggio		25.1.17	  PIC24E version
 * 
 *****************************************************************************/


// Heartbeat or node-guard


#include	"CO_TYPES24.H"
#include	"CO_CANDRV24.H"			// Driver services

#include	"CO_COMM.H"				// Object
#include	"CO_DICT.H"				// Dictionary Object Services
#include	"CO_NMTE.H"				// Network Management Error services
#include	"CO_TOOLS.H"			// COB ID tools



UNSIGNED32 		_uNMTE_COBID;


long 		_uNMTETimer;

UNSIGNED8		_uNMTEState;
UNSIGNED8		_uNMTELocalState;

UNSIGNED16	_uNMTEHeartBeat;
UNSIGNED16	_uNMTEGuardTime;
UNSIGNED8		_uNMTELifeFactor;

DWORD HeartBeatConsumers[1];

unsigned char 	_hNMTE;


void CO_NMTENodeGuardErrEvent(void);


void handleHrtLifeGuard(void) {

	// If the heartbeat is greater than 0 then enable heartbeat
	if(_uNMTEHeartBeat.word) {
		NMTE_TMR_LOCAL_EN = 1;
		_uNMTETimer = _uNMTEHeartBeat.word;
		}
	else {
		// else if the node guard and life is greater than 0 then enable node guard
		if(_uNMTEGuardTime.word && _uNMTELifeFactor.byte) {
			_uNMTELocalState.byte = 0x80;
			NMTE_NODE_GUARD_EN = 1;
			NMTE_TMR_LOCAL_EN = 1;
			_uNMTETimer = _uNMTEGuardTime.word * _uNMTELifeFactor.byte;
			}
		}
	}


void CO_COMM_NMTE_HeartBeatAccessEvent(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(UNSIGNED16 *)(uDict.obj->pReqBuf) = _uNMTEHeartBeat;
// era una prova per bug compilatore/EDS 9.17			uDict.obj->pReqBuf[1] = HIBYTE(_uNMTEGuardTime.word);
//			uDict.obj->pReqBuf[0] = LOBYTE(_uNMTEHeartBeat.word);
//			uDict.obj->pReqBuf[1] = HIBYTE(_uNMTEHeartBeat.word);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			_uNMTEHeartBeat = *(UNSIGNED16 *)(uDict.obj->pReqBuf);
//			_uNMTEHeartBeat.word = MAKEWORD(uDict.obj->pReqBuf[0],uDict.obj->pReqBuf[1]);

			// Reset the state
			_uNMTEState.byte = 0;

			handleHrtLifeGuard();
			break;
		}	
	}



void CO_COMM_NMTE_GuardTimeAccessEvent(void) {

	switch(uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(UNSIGNED16 *)(uDict.obj->pReqBuf) = _uNMTEGuardTime;
//			uDict.obj->pReqBuf[0] = LOBYTE(_uNMTEGuardTime.word);
// era una prova per bug compilatore/EDS 9.17			uDict.obj->pReqBuf[1] = HIBYTE(_uNMTEGuardTime.word);
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			_uNMTEGuardTime = *(UNSIGNED16 *)(uDict.obj->pReqBuf);
//			_uNMTEGuardTime.word = MAKEWORD(uDict.obj->pReqBuf[0],uDict.obj->pReqBuf[1]);

			// Reset the state
			_uNMTEState.byte = 0;

			handleHrtLifeGuard();
			break;
		}	
	}



void CO_COMM_NMTE_LifeFactorAccessEvent(void) {

	switch (uDict.cmd)	{
		case DICT_OBJ_INFO:		// Get information about the object
			break;

		case DICT_OBJ_READ: 	// Read the object
			*(UNSIGNED8 *)(uDict.obj->pReqBuf) = _uNMTELifeFactor;
			break;

		case DICT_OBJ_WRITE: 	// Write the object
			_uNMTELifeFactor = *(UNSIGNED8 *)(uDict.obj->pReqBuf);

			// Reset the state
			_uNMTEState.byte = 0;

			handleHrtLifeGuard();
			break;
		}	
	}


void CO_COMM_NMTE_Open(void) {

	// Set the COB ID and convert it to MCHP
//	_uNMTE_COBID.word = 0x700L | _uCO_nodeID.byte;
//	mTOOLS_CO2MCHP(_uNMTE_COBID.word);
//	_uNMTE_COBID.word = mTOOLS_GetCOBID();
//USARE _CO_COB_CANopen2MCHP_SID(0x700L | _uCO_nodeID.byte)

	_uNMTE_COBID.word=Canopen2Microchip(0x700L | configParms.nodeID);
		
	_uNMTEState.byte = 0;

	// If the heartbeat is greater than 0 then enable heartbeat
	if(_uNMTEHeartBeat.word) {
//		NMTE_TMR_LOCAL_EN = 1;
		COMM_NETCTL_NMTE_EN = 1;
//		_uNMTETimer = _uNMTEHeartBeat.word;
		}
	else {

		// else if the node guard and life is greater than 0 then enable node guard
		if(_uNMTEGuardTime.word && _uNMTELifeFactor.byte) {
			// Call the driver and request to open a receive endpoint
			_hNMTE=CANOpenMessage((COMM_MSGGRP_NETCTL) | COMM_NETCTL_NMTE, _uNMTE_COBID.word);
			if(_hNMTE)	{ 
				COMM_NETCTL_NMTE_EN = 1;
			
				// Reset the toggle memory
//				_uNMTELocalState.byte = 0x80;
	
//				NMTE_NODE_GUARD_EN = 1;
//				NMTE_TMR_LOCAL_EN = 1;
//				_uNMTETimer = _uNMTEGuardTime.word * _uNMTELifeFactor.byte;
				}
			}
		}

	handleHrtLifeGuard();
	}


void _CO_COMM_NMTE_Close(void) {

	// Call the driver, request to close the receive endpoint
	CANCloseMessage(_hNMTE);
	COMM_NETCTL_NMTE_EN = 0;
	}



void CO_COMM_NMTE_RXEvent(unsigned char b) {

	// If node guard is used and an RTR with 1 byte is received
	if(NMTE_NODE_GUARD_EN)	{
		if(mCANIsGetRTR(b))	{
// id=000 rtr=1 len=1 data=0

			if(mCANGetDataLen(b) == 0 /* era 1 ma matteo dice 0, 11.10.17*/) {
				// Queue a request to send data
				COMM_NETCTL_NMTE_TF = 1;
 
				// Reset the timer
				_uNMTETimer = _uNMTEGuardTime.word * _uNMTELifeFactor.byte;

// v. anche HeartBeatConsumers

				}
			}
		}
	}



void CO_COMM_NMTE_TXEvent(BYTE w) {

		// Set the COB
	*(__eds__ unsigned long *)(mCANGetPtrTxCOB(w)) = _uNMTE_COBID.word;
//		*mCANGetPtrTxCOB() = _uNMTE_COBID.word;
//		*(mCANGetPtrTxCOB()+1) = _uNMTE_COBID.word >> 16;

	// Set the length
	mCANPutDataLen(w,1);
		
	// Send the state or the boot
	if(!NMTE_BOOT_SERVICE) {
		// Toggle and strip old state (also known as bootup state)
		_uNMTELocalState.byte &= 0x80;
		if(NMTE_NODE_GUARD_EN) {
			_uNMTELocalState.bits.b7 ^= 1;
			}
		else {
			_uNMTELocalState.bits.b7 = 0;
			}

		// Set the data
		if(COMM_STATE_STOP) 
			_uNMTELocalState.byte |= 4;
		else {
			if(COMM_STATE_OPER) 
				_uNMTELocalState.byte |= 5;
			else {
				if(COMM_STATE_PREOP) 
					_uNMTELocalState.byte |= 127;
				}
			}

		// Load the data
		mCANPutDataByte0(w,_uNMTELocalState.byte);
		}
	else {
		NMTE_BOOT_SERVICE = 0;
		mCANPutDataByte0(w,0);
		}

	}



void CO_COMM_NMTE_LSTimerEvent(void) {

	// Process only if the local timer is enabled
	if(NMTE_TMR_LOCAL_EN) {
		// Adjust the time
		_uNMTETimer -= CO_TICK_PERIOD;
			
		// If the timer is zero then
		//if (_uNMTETimer == 0)
		if(_uNMTETimer <= 0)	{
			// If the heartbeat protocol is enabled
			if(!NMTE_NODE_GUARD_EN) {
				// Reset the timer
				_uNMTETimer += _uNMTEHeartBeat.word;
						
				// Queue the endpoint to send a heartbeat	
				COMM_NETCTL_NMTE_TF = 1;
				}

			// Else the node guard protocol is enabled
			else {
				// Reset the timer
				_uNMTETimer = _uNMTEGuardTime.word * _uNMTELifeFactor.byte;

				// Notify the application of an error
				CO_NMTENodeGuardErrEvent();
				}
			}
		}
	}

void CO_COMM_NMTE_TXFinEvent(void) {

	}


